import React from 'react';

const FacebookIcon: React.FC = () => (
    <svg
        version="1.0"
        xmlns="http://www.w3.org/2000/svg"
        width="512.000000pt"
        height="512.000000pt"
        viewBox="0 0 512.000000 512.000000"
        preserveAspectRatio="xMidYMid meet">
        <g
            transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
            fill="#000000"
            stroke="none">
            <path
                d="M2320 4440 l0 -40 -80 0 -80 0 0 -40 0 -40 -80 0 -80 0 0 -40 0 -40
-40 0 -40 0 0 -80 0 -80 -40 0 -40 0 0 -440 0 -440 -80 0 -80 0 0 -40 0 -40
-40 0 -40 0 0 -40 0 -40 -40 0 -40 0 0 -160 0 -160 40 0 40 0 0 -40 0 -40 40
0 40 0 0 -40 0 -40 80 0 80 0 0 -880 0 -880 40 0 40 0 0 -40 0 -40 40 0 40 0
0 -40 0 -40 160 0 160 0 0 40 0 40 40 0 40 0 0 40 0 40 40 0 40 0 0 880 0 880
360 0 360 0 0 40 0 40 40 0 40 0 0 80 0 80 40 0 40 0 0 80 0 80 40 0 40 0 0
120 0 120 -480 0 -480 0 0 200 0 200 40 0 40 0 0 80 0 80 80 0 80 0 0 40 0 40
400 0 400 0 0 40 0 40 40 0 40 0 0 80 0 80 40 0 40 0 0 80 0 80 40 0 40 0 0
120 0 120 -720 0 -720 0 0 -40z"
            />
        </g>
    </svg>
);

export default FacebookIcon;

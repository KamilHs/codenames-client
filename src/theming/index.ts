import {
    unstable_createMuiStrictModeTheme as createMuiTheme,
    createStyles,
    makeStyles,
    responsiveFontSizes,
    ThemeOptions
} from '@material-ui/core';

class Theming {
    private createTheme = (): ThemeOptions => ({
        palette: {
            text: {
                primary: '#2A0046',
                secondary: '#FFEA21'
            },
            primary: {
                main: '#2A0046',
                contrastText: '#fff'
            },
            secondary: {
                main: '#FFEA21',
                contrastText: '#fff'
            }
        },
        shape: {
            borderRadius: 8
        },
        typography: {
            fontFamily: [
                'Roboto',
                'sans-serif',
                '"Press Start 2P"',
                'cursive',
                'Ticketing',
                'sans-serif'
            ].join(','),
            fontSize: 16
        }
    });
    public getTheme = () => {
        const themeOptions = this.createTheme();
        const theme = createMuiTheme(themeOptions);

        return responsiveFontSizes(theme);
    };
}

const useStyles = makeStyles(theme =>
    createStyles({
        '@global': {
            '.container': {
                [theme.breakpoints.up('xs')]: {
                    maxWidth: '540px'
                },
                [theme.breakpoints.up('sm')]: {
                    maxWidth: '720px'
                },
                [theme.breakpoints.up('md')]: {
                    maxWidth: '960px'
                },
                [theme.breakpoints.up('lg')]: {
                    maxWidth: '1170px'
                },
                margin: '0 auto',
                padding: '0 15px',
                width: '100%'
            },
            '.container_flex': {
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex'
            },
            '.container_100vh': {
                minHeight: '100vh',
                height: '100vh'
            },
            '.h-full': {
                height: '100%'
            }
        }
    })
);

export const GlobalStyles = () => {
    useStyles();

    return null;
};

export const themeService = new Theming();

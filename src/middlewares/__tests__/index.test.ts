import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import thunk, { ThunkDispatch } from 'redux-thunk';

import { AllActionsType } from 'store/const';
import { localStorageActions } from 'store/global/local-storage.actions';
import { RootState } from 'store';

import { localStorageMiddleware } from '../localStorageMiddleware';

describe('localStorageMiddleware', () => {
    describe('actions', () => {
        const data = { name: 'name', item: 'item' };

        beforeAll(() => localStorage.clear());
        afterAll(() => localStorage.clear());

        type DispatchExts = ThunkDispatch<RootState, undefined, AllActionsType>;

        const middlewares = [
            thunk,
            routerMiddleware(createBrowserHistory()),
            localStorageMiddleware
        ];
        const mockStore = configureMockStore<RootState, DispatchExts>(
            middlewares
        );
        const storeApi: MockStoreEnhanced<
            RootState,
            DispatchExts
        > = mockStore();

        test('SET_LOCAL_STORAGE_ITEM', () => {
            storeApi.dispatch(
                localStorageActions.setLocalStorageItem(data.name, data.item)
            );
            expect(localStorage.getItem(data.name)).toBeTruthy();
        });

        test('UNSET_LOCAL_STORAGE_ITEM', () => {
            storeApi.dispatch(
                localStorageActions.unsetLocalStorageItem(data.name)
            );
            expect(localStorage.getItem(data.name)).not.toBeTruthy();
        });
    });
});

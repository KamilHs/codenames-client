import { DeepPartial, Middleware } from 'redux';
import get from 'lodash/get';
import { push } from 'connected-react-router';

import { AuthActionsType, LOGOUT } from 'modules/Auth/redux/const';
import { AUTH_ROUTES } from 'modules/Auth/routes/const';
import { initialState } from 'modules/Auth/redux/reducer';
import {
    LocalStorageActionsType,
    SET_LOCAL_STORAGE_ITEM,
    UNSET_LOCAL_STORAGE_ITEM
} from 'store/global/local-storage.const';
import { RootState } from 'store';

export const localStorageMiddleware: Middleware<
    Record<string, unknown>,
    RootState
> = store => next => (action: LocalStorageActionsType | AuthActionsType) => {
    switch (action.type) {
        case SET_LOCAL_STORAGE_ITEM:
            localStorage.setItem(
                get(action, ['payload', 'name']),
                get(action, ['payload', 'item'])
            );
            break;
        case UNSET_LOCAL_STORAGE_ITEM:
            localStorage.removeItem(action.payload);
            break;
        case LOGOUT:
            localStorage.clear();
            store.dispatch(push(AUTH_ROUTES.login));
            next(action);
            break;
        default:
            next(action);
    }
};

export const loadFromLocalStorage = (): DeepPartial<any> => {
    const state: DeepPartial<RootState> = {
        auth: {
            ...initialState
        }
    };

    const storageUser = localStorage.getItem('user');
    const storageToken = localStorage.getItem('token');

    if (state && state.auth && storageUser && storageToken) {
        state.auth.user = JSON.parse(storageUser);
        state.auth.isAuthenticated = true;
        state.auth.token = JSON.parse(storageToken);
    }

    return state;
};

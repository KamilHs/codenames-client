import { FetchStatus } from '../../../store/const';

import {
    ProfileActionsType,
    IProfileState,
    SET_PROFILE_STATUS,
    SET_AVATARS,
    SET_PROFILE_DATA
} from './const';

const initialState: IProfileState = {
    avatars: [],
    status: FetchStatus.none,
    profileData: null
};

const reducer = (
    state: IProfileState = initialState,
    action: ProfileActionsType
): IProfileState => {
    switch (action.type) {
        case SET_PROFILE_STATUS:
            return { ...state, status: action.payload };
        case SET_AVATARS:
            return { ...state, avatars: action.payload };
        case SET_PROFILE_DATA:
            return { ...state, profileData: action.payload };
        default:
            return state;
    }
};

export default reducer;

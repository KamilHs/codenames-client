import { Action } from 'redux';
import { push } from 'connected-react-router';
import { ThunkAction } from 'redux-thunk';

import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { FetchStatus } from '../../../store/const';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import { profileApi } from '../../../utils/api';

import {
    IEditableData,
    IProfileData,
    ProfileActionsType,
    SET_AVATARS,
    SET_PROFILE_DATA,
    SET_PROFILE_STATUS
} from './const';
import { handleError } from 'utils/redux';
import authActions from 'modules/Auth/redux/actions';
import { popupActions } from 'store/global/popup.actions';

const profileActions = {
    setProfileData: (data: IProfileData | null): ProfileActionsType => ({
        type: SET_PROFILE_DATA,
        payload: data
    }),
    setProfileStatus: (status: FetchStatus): ProfileActionsType => ({
        type: SET_PROFILE_STATUS,
        payload: status
    }),
    setAvatars: (avatars: string[]): ProfileActionsType => ({
        type: SET_AVATARS,
        payload: avatars
    }),
    getProfile: (
        token: string,
        userId: number
    ): ThunkAction<void, unknown, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(profileActions.setProfileStatus(FetchStatus.loading));
            const {
                data: { data: result }
            } = await profileApi.getProfile(token, userId);

            if (result && result.profile) {
                dispatch(profileActions.setProfileStatus(FetchStatus.success));
                dispatch(profileActions.setProfileData(result.profile));
            } else {
                dispatch(push(PROFILE_ROUTES.profile));
                dispatch(profileActions.setProfileStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(push(DASHBOARD_ROUTES.main));
            dispatch(profileActions.setProfileStatus(FetchStatus.failure));
        }
    },
    setProfile: (
        token: string,
        data: IEditableData
    ): ThunkAction<void, unknown, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(profileActions.setProfileStatus(FetchStatus.loading));
            const {
                data: { data: result }
            } = await profileApi.setProfile(token, data);

            if (result && result.success) {
                dispatch(
                    authActions.setLoginData({
                        user: result.user,
                        token: result.token,
                        isAuthenticated: !!result.user && !!result.token
                    })
                );
                dispatch(
                    popupActions.addSuccessPopup('Updated profile successfully')
                );
                dispatch(profileActions.setProfileStatus(FetchStatus.success));
            } else {
                dispatch(profileActions.setProfileStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(profileActions.setProfileStatus(FetchStatus.failure));
        }
    },
    getAvatars: (
        token: string
    ): ThunkAction<void, unknown, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(profileActions.setProfileStatus(FetchStatus.loading));
            const {
                data: { data: result }
            } = await profileApi.getAvatars(token);

            if (result && result.avatars) {
                dispatch(profileActions.setProfileStatus(FetchStatus.success));
                dispatch(profileActions.setAvatars(result.avatars));
            } else {
                dispatch(profileActions.setProfileStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(profileActions.setProfileStatus(FetchStatus.failure));
        }
    }
};

export default profileActions;

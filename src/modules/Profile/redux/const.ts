import { FetchStatus } from '../../../store/const';

export const SET_AVATARS = 'SET_AVATARS';
export const SET_PROFILE_STATUS = 'SET_PROFILE_STATUS';
export const SET_PROFILE_DATA = 'SET_PROFILE_DATA';

export interface IEditableData {
    username: string;
    description: string;
    avatar: string;
}

export interface IStatsData {
    games: number;
    wonGames: number;
    guesses: number;
    guessedWords: number;
}

export interface IProfileData extends IEditableData {
    userId: number;
    stats: IStatsData;
}

interface ISetAvatars {
    type: typeof SET_AVATARS;
    payload: string[];
}

interface ISetProfileData {
    type: typeof SET_PROFILE_DATA;
    payload: IProfileData | null;
}

interface ISetProfileStatus {
    type: typeof SET_PROFILE_STATUS;
    payload: FetchStatus;
}

export type ProfileActionsType =
    | ISetAvatars
    | ISetProfileData
    | ISetProfileStatus;

export interface IProfileState {
    avatars: string[];
    status: FetchStatus;
    profileData: IProfileData | null;
}

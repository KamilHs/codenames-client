import React from 'react';
import { Route, Switch } from 'react-router';

import Profile from '../components/index';

import { PROFILE_ROUTES } from './const';

export const ProfileRoutes: React.FC = () => (
    <Switch>
        <Route
            path={[PROFILE_ROUTES.profile, PROFILE_ROUTES.foreignProfile]}
            component={Profile}
            exact
        />
    </Switch>
);

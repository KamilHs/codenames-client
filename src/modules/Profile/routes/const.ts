export const PROFILE_PREFIX = '/profile';

export const PROFILE_ROUTES = {
    profile: `${PROFILE_PREFIX}/`,
    foreignProfile: `${PROFILE_PREFIX}/:userId`
};

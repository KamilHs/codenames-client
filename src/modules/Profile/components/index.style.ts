import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    section: {
        padding: theme.spacing(5),
        border: `1px solid ${theme.palette.secondary.main}`
    },
    statText: {
        fontFamily: '"Press Start 2P"',
        color: theme.palette.secondary.light,
        marginRight: theme.spacing(5)
    },

    statNumber: {
        fontFamily: '"Press Start 2P"',
        color: theme.palette.secondary.light
    },
    secondaryText: {
        fontFamily: 'Ticketing',
        color: theme.palette.secondary.dark,
        fontSize: '24px'
    },
    sectionTitle: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(3),
        lineHeight: '48px',
        textTransform: 'uppercase'
    },
    statContainer: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'space-between'
    },
    listItem: {
        position: 'relative'
    },
    title: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(3),
        lineHeight: '48px',
        textTransform: 'uppercase'
    },
    form: {
        padding: theme.spacing(5),
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: `${theme.shape.borderRadius * 3}px`
    },
    formField: {
        width: '100%',
        padding: theme.spacing(1),
        marginBottom: theme.spacing(3)
    },
    formButton: {
        marginBottom: theme.spacing(2),
        fontFamily: '"Press Start 2P"',
        borderRadius: `${theme.shape.borderRadius * 3}px`
    },
    deleteButton: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(2),
        fontSize: '10px',
        marginLeft: '20px',
        borderRadius: theme.shape.borderRadius * 3,
        color: '#c60000'
    },
    passwordButton: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(2),
        fontSize: '10px',
        marginLeft: '20px',
        borderRadius: theme.shape.borderRadius * 3
    },
    linearProgress: {
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.primary.dark
    },
    avatarContainer: {
        width: '100%',
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        overflowY: 'auto'
    },
    sliderButton: {
        cursor: 'pointer',
        fontFamily: '"Press Start 2P"',
        textAlign: 'center'
    },
    avatar: {
        width: '140px',
        height: '140px',
        userSelect: 'none',
        overflow: 'hidden'
    },
    deleteModal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    deleteModalInner: {
        background: theme.palette.primary.light,
        padding: theme.spacing(4),
        borderRadius: theme.shape.borderRadius
    },
    deleteConfirmInput: {
        width: '100%',
        '& label': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& input': {
            color: theme.palette.secondary.main,
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        }
    }
}));

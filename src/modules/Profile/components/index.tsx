import React from 'react';
import clsx from 'clsx';
import {
    Box,
    Grid,
    Typography,
    LinearProgress,
    List,
    ListItem,
    CircularProgress,
    Modal,
    TextField
} from '@material-ui/core';
import { FormikProps, Formik, Form, Field } from 'formik';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { AUTH_ROUTES } from 'modules/Auth/routes/const';
import {
    AuthenticationField,
    AuthenticationTextField
} from 'components/TextFields';
import Button from 'components/Buttons';
import { FetchStatus } from 'store/const';
import { Header } from 'components/Layout';
import { RootState } from 'store';
import { IStatsData } from '../redux/const';
import profileActions from '../redux/actions';
import { RegistrationTypeEnum } from 'modules/Auth/redux/const';
import { ValidationType } from 'utils/valdation/const';
import { validate } from 'utils/valdation';

import { IValues, IUseParams, statsLabels, DELETE_CONFIRMATION } from './const';
import useStyles from './index.style';
import authActions from 'modules/Auth/redux/actions';

const Profile: React.FC = () => {
    const classes = useStyles();
    const formikRef = React.useRef<FormikProps<IValues>>(null);

    const [currentAvatar, setCurrentAvatar] = React.useState<number>(0);
    const [edit, setEdit] = React.useState<boolean>(false);
    const [deleteModalOpen, setDeleteModalOpen] = React.useState<boolean>(
        false
    );
    const [deleteConfirmation, setDeleteConfirmation] = React.useState<string>(
        ''
    );

    const { user, token, avatars, profileData, status } = useSelector(
        (state: RootState) => ({
            user: state.auth.user,
            token: state.auth.token,
            ...state.profile
        })
    );
    const { userId } = useParams<IUseParams>();
    const dispatch = useDispatch();

    const isOwn = React.useMemo<boolean>(
        () => !userId || +userId === user?.id,
        [userId, user]
    );

    React.useEffect(() => {
        if (!user || !token || profileData) return;
        dispatch(profileActions.getProfile(token, userId ? +userId : user.id));
    }, [user, token, userId, profileData, dispatch]);

    React.useEffect(() => {
        if (!token || !isOwn || avatars.length > 0) return;
        dispatch(profileActions.getAvatars(token));
    }, [token, isOwn, avatars, dispatch]);

    React.useEffect(() => {
        if (!profileData || avatars.length === 0) return;
        setCurrentAvatar(avatars.indexOf(profileData.avatar));
    }, [profileData, avatars]);

    const handleAvatarChange = React.useCallback(
        (icon: number) => {
            if (avatars.length === 0 || !isOwn) return;
            if (icon === avatars.length) return setCurrentAvatar(0);
            if (icon < 0) return setCurrentAvatar(avatars.length - 1);
            setCurrentAvatar(icon);
        },
        [avatars, isOwn]
    );

    const handleSubmit = React.useCallback(
        (data: IValues) => {
            if (!token) return;
            dispatch(
                profileActions.setProfile(token, {
                    ...data,
                    avatar: avatars[currentAvatar]
                })
            );
            setEdit(false);
        },
        [token, avatars, currentAvatar, dispatch]
    );

    const initialValues = React.useMemo<IValues>(
        () => ({
            username: profileData?.username || '',
            description: profileData?.description || ''
        }),
        [profileData]
    );

    const deleteAccount = React.useCallback(() => {
        if (deleteConfirmation !== DELETE_CONFIRMATION || !token) return;
        dispatch(authActions.deleteAccount(token));
    }, [deleteConfirmation, token, dispatch]);

    React.useEffect(() => {
        if (!formikRef.current) return;
        formikRef.current.setSubmitting(status === FetchStatus.loading);
    }, [status]);

    if (!profileData) return null;

    const { stats, avatar } = profileData;

    if (!stats) return null;

    return (
        <>
            <Box className="background_main">
                <Box className="container container_100vh container_flex">
                    <Header />
                    <Box className={classes.form}>
                        <Typography
                            color="secondary"
                            align="center"
                            variant="h4"
                            className={classes.title}>
                            Profile
                        </Typography>
                        <Grid
                            container
                            direction="row"
                            justify="space-between"
                            wrap="nowrap"
                            alignItems="center"
                            spacing={1}>
                            <Grid item xs={6}>
                                <Formik
                                    innerRef={formikRef}
                                    initialValues={initialValues}
                                    validate={values =>
                                        validate(
                                            ValidationType.profileEdit,
                                            values
                                        )
                                    }
                                    onSubmit={(values, { setSubmitting }) => {
                                        setSubmitting(false);
                                        handleSubmit(values);
                                    }}>
                                    {({ submitForm, isSubmitting }) => (
                                        <Form>
                                            <Grid container>
                                                <Grid item xs={6}>
                                                    <Box
                                                        className={
                                                            classes.avatarContainer
                                                        }>
                                                        {edit && (
                                                            <Typography
                                                                color="secondary"
                                                                variant="h3"
                                                                onClick={() =>
                                                                    handleAvatarChange(
                                                                        currentAvatar -
                                                                            1
                                                                    )
                                                                }
                                                                className={clsx(
                                                                    classes.sliderButton
                                                                )}>
                                                                &lt;
                                                            </Typography>
                                                        )}
                                                        <img
                                                            src={
                                                                avatars[
                                                                    currentAvatar
                                                                ] ?? avatar
                                                            }
                                                            className={
                                                                classes.avatar
                                                            }
                                                            draggable={false}
                                                            alt="avatar"
                                                        />
                                                        {edit && (
                                                            <Typography
                                                                color="secondary"
                                                                variant="h3"
                                                                onClick={() =>
                                                                    handleAvatarChange(
                                                                        currentAvatar +
                                                                            1
                                                                    )
                                                                }
                                                                className={clsx(
                                                                    classes.sliderButton
                                                                )}>
                                                                &gt;
                                                            </Typography>
                                                        )}
                                                    </Box>
                                                    {!edit && isOwn && (
                                                        <Box
                                                            marginTop={3}
                                                            display="flex"
                                                            justifyContent="center">
                                                            <Button
                                                                fontFamily='"Press Start 2P"'
                                                                variant="contained"
                                                                color="primary"
                                                                onClick={() =>
                                                                    setEdit(
                                                                        true
                                                                    )
                                                                }
                                                                className={
                                                                    classes.formButton
                                                                }>
                                                                Edit profile
                                                            </Button>
                                                        </Box>
                                                    )}
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <Field
                                                        id="standard-basic"
                                                        label="Username"
                                                        name="username"
                                                        component={
                                                            AuthenticationField
                                                        }
                                                        className={
                                                            classes.formField
                                                        }
                                                        disabled={
                                                            !edit || !isOwn
                                                        }
                                                    />
                                                    <Field
                                                        id="standard-basic"
                                                        label="Description"
                                                        name="description"
                                                        component={
                                                            AuthenticationTextField
                                                        }
                                                        className={
                                                            classes.formField
                                                        }
                                                        disabled={
                                                            !edit || !isOwn
                                                        }
                                                    />
                                                </Grid>
                                                {edit && isOwn && (
                                                    <Grid item xs={12}>
                                                        <Button
                                                            fontFamily='"Press Start 2P"'
                                                            variant="contained"
                                                            color="primary"
                                                            disabled={
                                                                isSubmitting
                                                            }
                                                            onClick={submitForm}
                                                            className={
                                                                classes.formButton
                                                            }>
                                                            Submit
                                                        </Button>
                                                        <Button
                                                            fontFamily='"Press Start 2P"'
                                                            variant="contained"
                                                            color="secondary"
                                                            className={
                                                                classes.deleteButton
                                                            }
                                                            onClick={() =>
                                                                setDeleteModalOpen(
                                                                    true
                                                                )
                                                            }>
                                                            Delete Account
                                                        </Button>
                                                        {user?.registrationType ===
                                                            RegistrationTypeEnum.local && (
                                                            <Link
                                                                to={
                                                                    AUTH_ROUTES.updatePassword
                                                                }>
                                                                <Button
                                                                    fontFamily='"Press Start 2P"'
                                                                    variant="contained"
                                                                    color="secondary"
                                                                    className={
                                                                        classes.passwordButton
                                                                    }>
                                                                    Change
                                                                    Password
                                                                </Button>
                                                            </Link>
                                                        )}
                                                    </Grid>
                                                )}
                                                {isSubmitting && (
                                                    <LinearProgress
                                                        color="secondary"
                                                        className={
                                                            classes.linearProgress
                                                        }
                                                    />
                                                )}
                                            </Grid>
                                        </Form>
                                    )}
                                </Formik>
                            </Grid>
                            <Grid item xs={6}>
                                <Box className={classes.section}>
                                    <Typography
                                        color="secondary"
                                        align="center"
                                        variant="h4"
                                        className={classes.title}>
                                        stats
                                    </Typography>
                                    <List>
                                        {Object.entries(stats).map(
                                            ([key, value]) => (
                                                <ListItem
                                                    key={key}
                                                    className={
                                                        classes.statContainer
                                                    }
                                                    disableGutters={true}>
                                                    <Typography
                                                        variant="h6"
                                                        className={
                                                            classes.statText
                                                        }>
                                                        {
                                                            statsLabels[
                                                                key as keyof IStatsData
                                                            ]
                                                        }
                                                        :
                                                    </Typography>

                                                    <Typography
                                                        variant="h6"
                                                        className={
                                                            classes.statNumber
                                                        }>
                                                        {value}
                                                    </Typography>
                                                </ListItem>
                                            )
                                        )}
                                    </List>
                                </Box>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Box>
            {status === FetchStatus.loading && (
                <Box
                    top="0"
                    left="0"
                    width="100%"
                    height="100%"
                    position="fixed"
                    display="flex"
                    justifyContent="center"
                    alignItems="center">
                    <CircularProgress color="secondary" />
                </Box>
            )}
            <Modal
                open={deleteModalOpen}
                className={classes.deleteModal}
                onClose={() => setDeleteModalOpen(false)}>
                <Box
                    className={classes.deleteModalInner}
                    display="flex"
                    flexDirection="column">
                    <Typography
                        style={{
                            fontFamily: 'Ticketing'
                        }}
                        variant="body1"
                        color="secondary">
                        Write{' '}
                        <span style={{ color: 'red' }}>
                            "{DELETE_CONFIRMATION}"
                        </span>{' '}
                        to delete the account
                    </Typography>
                    <Box marginTop={4}>
                        <TextField
                            className={classes.deleteConfirmInput}
                            value={deleteConfirmation}
                            onChange={e =>
                                setDeleteConfirmation(
                                    e.target.value.trimStart()
                                )
                            }
                            label="Write here"
                        />
                    </Box>
                    <Box marginTop={4} display="flex" justifyContent="center">
                        <Button
                            variant="contained"
                            fontFamily='"Press Start 2P"'
                            className={classes.deleteButton}
                            color="secondary"
                            disabled={
                                deleteConfirmation !== DELETE_CONFIRMATION
                            }
                            onClick={deleteAccount}>
                            Delete Account
                        </Button>
                    </Box>
                </Box>
            </Modal>
        </>
    );
};

export default Profile;

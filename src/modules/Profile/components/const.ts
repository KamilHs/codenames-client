import { IStatsData } from 'modules/Profile/redux/const';

export interface IValues {
    username: string;
    description: string;
}

export interface IUseParams {
    userId: string;
}

export const statsLabels: Record<keyof IStatsData, string> = {
    games: 'Games',
    wonGames: 'Won games',
    guesses: 'Guesses',
    guessedWords: 'Guessed'
};

export const DELETE_CONFIRMATION = 'I want to delete my account';

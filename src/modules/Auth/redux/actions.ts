/* eslint-disable @typescript-eslint/no-magic-numbers */
import { Action } from 'redux';
import { push } from 'connected-react-router';
import { ThunkAction } from 'redux-thunk';

import { AUTH_ROUTES } from 'modules/Auth/routes/const';
import { authApi } from 'utils/api';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { FetchStatus } from 'store/const';
import { localStorageActions } from 'store/global/local-storage.actions';
import { popupActions } from 'store/global/popup.actions';
import { RootState } from 'store';

import {
    AuthActionsType,
    ILogin,
    IRegister,
    IUser,
    LOGOUT,
    SET_AUTH_STATUS,
    SET_IS_AUTHENTICATED,
    SET_TOKEN,
    SET_USER
} from './const';
import { handleError } from 'utils/redux';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';

const authActions = {
    setIsAuthenticated: (isAuthenticated: boolean): AuthActionsType => ({
        type: SET_IS_AUTHENTICATED,
        payload: isAuthenticated
    }),
    setFetchStatus: (status: FetchStatus): AuthActionsType => ({
        type: SET_AUTH_STATUS,
        payload: status
    }),
    setUser: (user: IUser | null): AuthActionsType => ({
        type: SET_USER,
        payload: user
    }),
    setToken: (token: string | null): AuthActionsType => ({
        type: SET_TOKEN,
        payload: token
    }),
    logout: (): AuthActionsType => ({
        type: LOGOUT
    }),
    setLoginData: (payload: {
        user: IUser | null;
        token: string | null;
        isAuthenticated: boolean;
    }): ThunkAction<void, unknown, unknown, Action<any>> => async dispatch => {
        const { user, token, isAuthenticated } = payload;

        dispatch(localStorageActions.setLocalStorageItem('token', token));
        dispatch(localStorageActions.setLocalStorageItem('user', user));
        dispatch(authActions.setUser(user));
        dispatch(authActions.setIsAuthenticated(isAuthenticated));
        dispatch(authActions.setToken(token));
    },
    verifyEmail: (
        verificationId: string,
        token: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            const {
                data: { data: response }
            } = await authApi.verifyEmail(verificationId, token);

            if (response && response.success) {
                dispatch(
                    popupActions.addSuccessPopup('Email verified successfully')
                );
                dispatch(push(DASHBOARD_ROUTES.main));
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    createPasswordReset: (
        email: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            const {
                data: { data: response }
            } = await authApi.createPasswordReset(email);

            if (response && response.success) {
                dispatch(
                    popupActions.addSuccessPopup(
                        'Check your email for instructions'
                    )
                );
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    confirmPasswordReset: (
        password: string,
        resetId: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            const {
                data: { data: response }
            } = await authApi.confirmPasswordReset(password, resetId);

            if (response && response.success) {
                dispatch(push(AUTH_ROUTES.login));
                dispatch(
                    popupActions.addSuccessPopup('Password reset successfully')
                );
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    getUser: (
        token: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            const {
                data: { data: result }
            } = await authApi.getUser({ token });

            if (result && result.user) {
                return dispatch(
                    authActions.setLoginData({
                        user: result.user,
                        token,
                        isAuthenticated: true
                    })
                );
            }

            dispatch(authActions.logout());
            dispatch(popupActions.addErrorPopup('Invalid login'));
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.logout());
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    register: (
        registerData: IRegister
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(authActions.setFetchStatus(FetchStatus.loading));
            const {
                data: { data: response }
            } = await authApi.register(registerData);

            if (response && response?.success) {
                dispatch(authActions.setFetchStatus(FetchStatus.success));
                dispatch(push(AUTH_ROUTES.login));
                dispatch(
                    popupActions.addSuccessPopup('Account created successfully')
                );
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
                dispatch(authActions.setFetchStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    login: (
        loginData: ILogin
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(authActions.setFetchStatus(FetchStatus.loading));
            const {
                data: { data: response }
            } = await authApi.login(loginData);

            if (response && response.user) {
                dispatch(authActions.setFetchStatus(FetchStatus.success));
                dispatch(authActions.setLoginData(response));

                dispatch(push(DASHBOARD_ROUTES.main));

                dispatch(popupActions.addSuccessPopup('Logged successfully'));
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
                dispatch(authActions.setFetchStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    updatePassword: (
        token: string,
        password: string,
        updatedPassword: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(authActions.setFetchStatus(FetchStatus.loading));
            const {
                data: { data: response }
            } = await authApi.updatePassword(token, password, updatedPassword);

            if (response && response.success) {
                dispatch(authActions.setFetchStatus(FetchStatus.success));

                dispatch(authActions.getUser(token));
                dispatch(push(PROFILE_ROUTES.profile));
                dispatch(
                    popupActions.addSuccessPopup(
                        'Password Updated Successfully'
                    )
                );
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
                dispatch(authActions.setFetchStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    },
    deleteAccount: (
        token: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(authActions.setFetchStatus(FetchStatus.loading));
            const {
                data: { data: response }
            } = await authApi.deleteAccount(token);

            if (response && response.success) {
                dispatch(authActions.setFetchStatus(FetchStatus.success));

                dispatch(authActions.logout());
                dispatch(push(AUTH_ROUTES.login));
                dispatch(
                    popupActions.addSuccessPopup('Account Deleted Successfully')
                );
            } else {
                dispatch(popupActions.addErrorPopup('Something went wrong'));
                dispatch(authActions.setFetchStatus(FetchStatus.failure));
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(authActions.setFetchStatus(FetchStatus.failure));
        }
    }
};

export default authActions;

import axios from 'axios';
import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import thunk, { ThunkDispatch } from 'redux-thunk';

import { localStorageMiddleware } from 'middlewares/localStorageMiddleware';

const mockedAxios = axios as jest.Mocked<typeof axios>;

import authActions from '../actions';
import {
    AuthActionsType,
    IAuthState,
    ILogin,
    IRegister,
    IUser,
    LOGOUT,
    RegistrationTypeEnum,
    SET_AUTH_STATUS,
    SET_IS_AUTHENTICATED,
    SET_TOKEN,
    SET_USER
} from '../const';
import { RootState } from 'store';
import { AllActionsType, FetchStatus } from 'store/const';
import { ILoginResponse, IRegisterResponse } from 'utils/axios/const';
import reducer from 'modules/Auth/redux/reducer';

describe('Auth module redux', () => {
    describe('action creators', () => {
        test('setIsAuthenticated', () => {
            const isAuthenticated = false;
            const expectedAction: AuthActionsType = {
                type: SET_IS_AUTHENTICATED,
                payload: isAuthenticated
            };

            expect(authActions.setIsAuthenticated(isAuthenticated)).toEqual(
                expectedAction
            );
        });

        test('setFetchStatus', () => {
            const fetchStatus = FetchStatus.success;
            const expectedAction: AuthActionsType = {
                type: SET_AUTH_STATUS,
                payload: fetchStatus
            };

            expect(authActions.setFetchStatus(fetchStatus)).toEqual(
                expectedAction
            );
        });

        test('setUser', () => {
            const user: IUser = {
                id: 1,
                username: 'Username',
                email: 'exampl@gmail.com',
                avatar: 'avatar.png',
                description: 'Hey',
                isOnline: false,
                isVerified: false,
                registrationType: RegistrationTypeEnum.local
            };
            const expectedAction: AuthActionsType = {
                type: SET_USER,
                payload: user
            };

            expect(authActions.setUser(user)).toEqual(expectedAction);
        });

        test('logout', () => {
            const expectedAction: AuthActionsType = {
                type: LOGOUT
            };

            expect(authActions.logout()).toEqual(expectedAction);
        });

        test('setToken', () => {
            const token = 'token';
            const expectedAction: AuthActionsType = {
                type: SET_TOKEN,
                payload: token
            };

            expect(authActions.setToken(token)).toEqual(expectedAction);
        });
    });

    describe('async action creators', () => {
        type DispatchExts = ThunkDispatch<RootState, undefined, AllActionsType>;

        const middlewares = [
            thunk,
            routerMiddleware(createBrowserHistory()),
            localStorageMiddleware
        ];
        const mockStore = configureMockStore<RootState, DispatchExts>(
            middlewares
        );
        let storeApi: MockStoreEnhanced<RootState, DispatchExts>;

        beforeEach(() => {
            storeApi = mockStore();
        });

        describe('register', () => {
            const mockData: IRegister = {
                email: '',
                username: '',
                password: ''
            };

            test('success', async () => {
                const mockResponse: IRegisterResponse = {
                    data: { success: true, message: 'message' },
                    error: false
                };
                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.success
                    }
                ];

                mockedAxios.post.mockResolvedValueOnce({ data: mockResponse });

                await storeApi.dispatch(authActions.register(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });

            test('invalid', async () => {
                const mockResponse: IRegisterResponse = {
                    data: false,
                    error: {
                        message: 'message',
                        status: 422
                    }
                };
                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.failure
                    }
                ];

                mockedAxios.post.mockResolvedValueOnce({ data: mockResponse });

                await storeApi.dispatch(authActions.register(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });

            test('error', async () => {
                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.failure
                    }
                ];

                mockedAxios.post.mockRejectedValueOnce(null);

                await storeApi.dispatch(authActions.register(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });
        });

        describe('login', () => {
            const mockData: ILogin = {
                email: 'example@gmail.com',
                password: 'secret'
            };

            test('success', async () => {
                const mockResponse: ILoginResponse = {
                    data: {
                        isAuthenticated: true,
                        token: 'string',
                        user: {
                            id: 123,
                            email: mockData.email,
                            username: 'Dasd',
                            avatar: 'Dasda',
                            isOnline: false,
                            description: 'dasdF',
                            isVerified: false,
                            registrationType: RegistrationTypeEnum.local
                        }
                    },
                    error: false
                };

                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_USER,
                        payload:
                            (mockResponse.data && mockResponse.data.user) ||
                            null
                    },
                    {
                        type: SET_IS_AUTHENTICATED,
                        payload: true
                    },
                    {
                        type: SET_TOKEN,
                        payload:
                            (mockResponse.data && mockResponse.data.token) ||
                            null
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.success
                    }
                ];

                mockedAxios.post.mockResolvedValueOnce({ data: mockResponse });

                await storeApi.dispatch(authActions.login(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });

            test('invalid', async () => {
                const mockResponse: ILoginResponse = {
                    data: false,
                    error: {
                        message: 'message',
                        status: 422
                    }
                };
                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.failure
                    }
                ];

                mockedAxios.post.mockResolvedValueOnce({ data: mockResponse });

                await storeApi.dispatch(authActions.login(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });

            test('error', async () => {
                const expectedActions: AllActionsType[] = [
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.loading
                    },
                    {
                        type: SET_AUTH_STATUS,
                        payload: FetchStatus.failure
                    }
                ];

                mockedAxios.post.mockRejectedValueOnce(null);

                await storeApi.dispatch(authActions.login(mockData));

                expect(storeApi.getActions()).toEqual(expectedActions);
            });
        });
    });

    describe('reducers', () => {
        const initialState: IAuthState = {
            isAuthenticated: false,
            error: '',
            status: FetchStatus.none,
            user: null,
            token: null
        };

        test('SET_IS_AUTHENTICATED', () =>
            expect(
                reducer(undefined, authActions.setIsAuthenticated(true))
            ).toEqual({ ...initialState, isAuthenticated: true }));

        test('SET_AUTH_STATUS', () =>
            expect(
                reducer(
                    undefined,
                    authActions.setFetchStatus(FetchStatus.failure)
                )
            ).toEqual({ ...initialState, status: FetchStatus.failure }));

        test('SET_USER', () =>
            expect(reducer(undefined, authActions.setUser(null))).toEqual({
                ...initialState,
                user: null
            }));

        test('SET_TOKEN', () =>
            expect(reducer(undefined, authActions.setToken('token'))).toEqual({
                ...initialState,
                token: 'token'
            }));

        test('LOGOUT', () =>
            expect(reducer(undefined, authActions.logout())).toEqual({
                ...initialState,
                token: null,
                user: null,
                isAuthenticated: false
            }));
    });
});

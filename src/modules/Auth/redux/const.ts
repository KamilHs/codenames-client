import { FetchStatus } from 'store/const';

export const SET_AUTH_ERROR = 'SET_AUTH_ERROR';
export const SET_AUTH_STATUS = 'SET_AUTH_STATUS';
export const SET_IS_AUTHENTICATED = 'SET_IS_AUTHENTICATED';
export const SET_USER = 'SET_USER';
export const SET_TOKEN = 'SET_TOKEN';
export const LOGOUT = 'LOGOUT';

export interface ILogin {
    email: string;
    password: string;
}

export interface IRegister extends ILogin {
    username: string;
}

export enum RegistrationTypeEnum {
    local = 'local',
    google = 'google',
    facebook = 'facebook',
    github = 'github'
}

export interface IUser {
    id: number;
    username: string;
    email: string;
    phone?: string;
    avatar: string;
    description: string;
    isOnline: boolean;
    isVerified: boolean;
    registrationType: RegistrationTypeEnum;
}

interface ISetIsAuthenticated {
    type: typeof SET_IS_AUTHENTICATED;
    payload: boolean;
}

interface ISetFetchStatus {
    type: typeof SET_AUTH_STATUS;
    payload: FetchStatus;
}

interface ISetAuthError {
    type: typeof SET_AUTH_ERROR;
    payload: string;
}

interface ISetUser {
    type: typeof SET_USER;
    payload: IUser | null;
}

interface ISetToken {
    type: typeof SET_TOKEN;
    payload: string | null;
}

interface ILogout {
    type: typeof LOGOUT;
}

export type AuthActionsType =
    | ISetIsAuthenticated
    | ISetFetchStatus
    | ISetAuthError
    | ISetUser
    | ISetToken
    | ILogout;

export interface IAuthState {
    isAuthenticated: boolean;
    error: string;
    status: FetchStatus;
    user: IUser | null;
    token: string | null;
}

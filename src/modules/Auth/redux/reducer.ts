import { FetchStatus } from 'store/const';

import {
    AuthActionsType,
    IAuthState,
    LOGOUT,
    SET_AUTH_ERROR,
    SET_AUTH_STATUS,
    SET_IS_AUTHENTICATED,
    SET_TOKEN,
    SET_USER
} from './const';

export const initialState: IAuthState = {
    isAuthenticated: false,
    error: '',
    status: FetchStatus.none,
    user: null,
    token: null
};

const reducer = (
    state: IAuthState = initialState,
    action: AuthActionsType
): IAuthState => {
    switch (action.type) {
        case SET_IS_AUTHENTICATED:
            return { ...state, isAuthenticated: action.payload };
        case SET_AUTH_STATUS:
            if (action.payload !== FetchStatus.failure) {
                return { ...state, status: action.payload, error: '' };
            }

            return { ...state, status: action.payload };
        case SET_AUTH_ERROR:
            return {
                ...state,
                status: FetchStatus.failure,
                error: action.payload
            };
        case SET_USER:
            return {
                ...state,
                user: action.payload
            };
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload
            };
        case LOGOUT: {
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                token: null
            };
        }
        default:
            return state;
    }
};

export default reducer;

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import {
    CreatePasswordReset,
    Login,
    PasswordReset,
    Register,
    VerifyEmail,
    UpdatePassword
} from '../components';
import { RootState } from 'store';

import { AUTH_PREFIX, AUTH_ROUTES } from './const';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';

export const AuthRoutes: React.FC = () => {
    const { isAuthenticated, token, user } = useSelector(
        (state: RootState) => state.auth
    );

    const render = React.useCallback(
        () =>
            isAuthenticated && token && user ? (
                <Switch>
                    <Route
                        path={AUTH_ROUTES.verifyEmail}
                        component={VerifyEmail}
                        exact
                    />
                    <Route
                        path={AUTH_ROUTES.updatePassword}
                        component={UpdatePassword}
                        exact
                    />
                    <Redirect to={DASHBOARD_ROUTES.main} />
                </Switch>
            ) : (
                <Switch>
                    <Route
                        path={[
                            AUTH_PREFIX,
                            AUTH_ROUTES.login,
                            AUTH_ROUTES.loginToken
                        ]}
                        component={Login}
                        exact
                    />
                    <Route
                        path={AUTH_ROUTES.register}
                        component={Register}
                        exact
                    />
                    <Route
                        path={AUTH_ROUTES.passwordReset}
                        component={PasswordReset}
                        exact
                    />
                    <Route
                        path={AUTH_ROUTES.createPasswordReset}
                        component={CreatePasswordReset}
                        exact
                    />
                </Switch>
            ),
        [isAuthenticated, user, token]
    );

    return <Route path={AUTH_PREFIX} render={render} />;
};

export const AUTH_PREFIX = '/auth';

export const AUTH_ROUTES = {
    login: `${AUTH_PREFIX}/login`,
    loginToken: `${AUTH_PREFIX}/login/:token?`,
    register: `${AUTH_PREFIX}/register`,
    passwordReset: `${AUTH_PREFIX}/password-reset/:resetId`,
    createPasswordReset: `${AUTH_PREFIX}/password-reset`,
    verifyEmail: `${AUTH_PREFIX}/verify-email/:verificationId`,
    updatePassword: `${AUTH_PREFIX}/update-password`
};

export enum OAuthLinkType {
    github = 'github',
    google = 'google',
    facebook = 'facebook'
}

export const OAUTH2_ROUTES: Record<OAuthLinkType, string> = {
    [OAuthLinkType.google]: `${process.env.REACT_APP_API_URL}${AUTH_PREFIX}/google`,
    [OAuthLinkType.github]: `${process.env.REACT_APP_API_URL}${AUTH_PREFIX}/github`,
    [OAuthLinkType.facebook]: `${process.env.REACT_APP_API_URL}${AUTH_PREFIX}/facebook`
};

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';

import authActions from 'modules/Auth/redux/actions';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { LocalStorageContext } from 'context/LocalStorageContext';
import { RegistrationTypeEnum } from 'modules/Auth/redux/const';
import { RootState } from 'store';

import { IUseParams } from './const';

const VerifyEmail: React.FC = () => {
    const { verificationId } = useParams<IUseParams>();
    const { checkAuth } = React.useContext(LocalStorageContext);

    const { token, user } = useSelector((state: RootState) => state.auth);
    const dispatch = useDispatch();
    const history = useHistory();

    React.useEffect(checkAuth, [checkAuth]);

    React.useEffect(() => {
        (user?.registrationType !== RegistrationTypeEnum.local ||
            user?.isVerified) &&
            history.push(DASHBOARD_ROUTES.main);
    }, [user, history]);

    React.useEffect(() => {
        token &&
            verificationId &&
            dispatch(authActions.verifyEmail(verificationId, token));
    }, [token, verificationId, dispatch]);

    return null;
};

export default VerifyEmail;

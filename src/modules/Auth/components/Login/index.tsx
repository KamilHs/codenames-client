import React from 'react';
import clsx from 'clsx';
import { Box, Grid, LinearProgress, Typography } from '@material-ui/core';
import { Field, Form, Formik, FormikProps } from 'formik';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { AUTH_ROUTES } from '../../routes/const';
import authActions from 'modules/Auth/redux/actions';
import { AuthenticationField } from '../../../../components/TextFields';
import Button from '../../../../components/Buttons';
import { FetchStatus } from 'store/const';
import { Header } from 'components/Layout';
import OAuthLinks from 'components/Links/OAuth';
import { RootState } from 'store';
import { validate } from '../../../../utils/valdation';
import { ValidationType } from '../../../../utils/valdation/const';
import useModuleStyles from '../index.styles';

import { IUseParams, IValues } from './const';
import useStyles from './index.styles';

const initialValues: IValues = {
    email: '',
    password: ''
};

const Login: React.FC = () => {
    const { token } = useParams<IUseParams>();

    const dispatch = useDispatch();

    React.useEffect(() => {
        if (token) dispatch(authActions.getUser(token));
    }, [token, dispatch]);

    const formikRef = React.useRef<FormikProps<IValues>>(null);
    const classes = { ...useModuleStyles(), ...useStyles() };

    const { status } = useSelector((state: RootState) => state.auth);

    const handleLogin = React.useCallback(
        (values: IValues) => {
            dispatch(authActions.login(values));
        },
        [dispatch]
    );

    React.useEffect(() => {
        if (!formikRef.current) return;
        formikRef.current.setSubmitting(status === FetchStatus.loading);
    }, [status]);

    return (
        <Box className="background_main">
            <Header />
            <Box className="container container_flex container_100vh">
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={6} lg={5}>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="h3"
                                className={classes.title}
                                paragraph>
                                Login
                            </Typography>
                            <Formik
                                innerRef={formikRef}
                                initialValues={initialValues}
                                validate={values =>
                                    validate(ValidationType.login, values)
                                }
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(false);
                                    handleLogin(values);
                                }}>
                                {({ submitForm, isSubmitting }) => (
                                    <Form>
                                        <Field
                                            type="email"
                                            name="email"
                                            label="Email"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="password"
                                            label="Password"
                                            className={clsx(
                                                classes.formField,
                                                classes.passwordField
                                            )}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Box
                                            className={
                                                classes.forgotPasswordContainer
                                            }>
                                            <Link
                                                to={
                                                    AUTH_ROUTES.createPasswordReset
                                                }>
                                                <Typography
                                                    className={
                                                        classes.forgotPassword
                                                    }
                                                    variant="body2">
                                                    Forgot password?
                                                </Typography>
                                            </Link>
                                        </Box>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={submitForm}
                                            className={classes.formButton}>
                                            Login
                                        </Button>
                                        <Link to={AUTH_ROUTES.register}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                className={classes.formButton}>
                                                Register
                                            </Button>
                                        </Link>
                                        <OAuthLinks />
                                        {isSubmitting && (
                                            <LinearProgress
                                                color="secondary"
                                                className={
                                                    classes.linearProgress
                                                }
                                            />
                                        )}
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default Login;

import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    forgotPasswordContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: theme.spacing(3)
    },
    forgotPassword: {
        fontFamily: '"Ticketing"',
        color: theme.palette.secondary.main,
        '&:hover': {
            textDecoration: 'underline'
        }
    },
    passwordField: {
        marginBottom: 0
    }
}));

export interface IValues {
    email: string;
    password: string;
}

export interface IUseParams {
    token?: string;
}

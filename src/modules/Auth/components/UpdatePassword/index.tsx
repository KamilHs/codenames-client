import React from 'react';
import { Box, Grid, LinearProgress, Typography } from '@material-ui/core';
import { Field, Form, Formik, FormikProps } from 'formik';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import authActions from 'modules/Auth/redux/actions';
import { AuthenticationField } from 'components/TextFields';
import Button from 'components/Buttons';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { Header } from 'components/Layout';
import { popupActions } from 'store/global/popup.actions';
import { RegistrationTypeEnum } from 'modules/Auth/redux/const';
import { RootState } from 'store';
import { validate } from 'utils/valdation';
import { ValidationType } from 'utils/valdation/const';

import { IUpdatePasswordValues } from './const';
import useStyles from '../index.styles';

const initialValues: IUpdatePasswordValues = {
    oldPassword: '',
    password: '',
    confirmPassword: ''
};

const PasswordReset: React.FC = () => {
    const { user, token } = useSelector((state: RootState) => state.auth);
    const formikRef = React.useRef<FormikProps<IUpdatePasswordValues>>(null);

    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();

    const handleUpdatePassword = React.useCallback(
        (values: IUpdatePasswordValues) => {
            if (!user || !token) return;
            dispatch(
                authActions.updatePassword(
                    token,
                    values.oldPassword,
                    values.password
                )
            );
        },
        [token, user, dispatch]
    );

    React.useEffect(() => {
        if (!user || user.registrationType !== RegistrationTypeEnum.local) {
            history.push(DASHBOARD_ROUTES.main);
            dispatch(
                popupActions.addErrorPopup(
                    "You can't update password with non-password strategy"
                )
            );
        }
    }, [user, history, dispatch]);

    return (
        <Box className="background_main">
            <Header />
            <Box className="container container_flex container_100vh">
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={6} lg={5}>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="body1"
                                className={classes.title}
                                paragraph>
                                Update password
                            </Typography>
                            <Formik
                                innerRef={formikRef}
                                initialValues={initialValues}
                                validate={values =>
                                    validate(
                                        ValidationType.updatePassword,
                                        values
                                    )
                                }
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(false);
                                    handleUpdatePassword(values);
                                }}>
                                {({ submitForm, isSubmitting }) => (
                                    <Form>
                                        <Field
                                            type="password"
                                            name="oldPassword"
                                            label="Current password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="password"
                                            label="New password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="confirmPassword"
                                            label="Confirm password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={submitForm}
                                            className={classes.formButton}>
                                            Update password
                                        </Button>
                                        <Link to={DASHBOARD_ROUTES.main}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                className={classes.formButton}>
                                                Dashboard
                                            </Button>
                                        </Link>
                                        {isSubmitting && (
                                            <LinearProgress
                                                color="secondary"
                                                className={
                                                    classes.linearProgress
                                                }
                                            />
                                        )}
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default PasswordReset;

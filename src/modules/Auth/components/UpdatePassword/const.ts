export interface IUpdatePasswordValues {
    oldPassword: string;
    password: string;
    confirmPassword: string;
}

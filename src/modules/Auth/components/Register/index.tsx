import React from 'react';
import { Box, Grid, LinearProgress, Typography } from '@material-ui/core';
import { Field, Form, Formik, FormikProps } from 'formik';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { AUTH_ROUTES } from '../../routes/const';
import authActions from 'modules/Auth/redux/actions';
import { AuthenticationField } from '../../../../components/TextFields';
import Button from 'components/Buttons';
import { FetchStatus } from 'store/const';
import { Header } from 'components/Layout';
import { RootState } from 'store';
import { validate } from '../../../../utils/valdation';
import { ValidationType } from '../../../../utils/valdation/const';
import useModuleStyles from '../index.styles';

import { IValues } from './const';

const initialValues: IValues = {
    username: '',
    password: '',
    confirmPassword: '',
    email: ''
};

const Register: React.FC = () => {
    const formikRef = React.useRef<FormikProps<IValues>>(null);
    const classes = useModuleStyles();
    const { status } = useSelector((state: RootState) => state.auth);

    const dispatch = useDispatch();

    const handleRegister = React.useCallback(
        (values: IValues) => {
            dispatch(authActions.register(values));
        },
        [dispatch]
    );

    React.useEffect(() => {
        if (!formikRef.current) return;
        formikRef.current.setSubmitting(status === FetchStatus.loading);
    }, [status]);

    return (
        <Box className="background_main">
            <Header />
            <Box className="container container_flex container_100vh">
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={12} sm={10} md={8} lg={6}>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="h3"
                                className={classes.title}
                                paragraph>
                                Register
                            </Typography>
                            <Formik
                                innerRef={formikRef}
                                initialValues={initialValues}
                                validate={values =>
                                    validate(ValidationType.register, values)
                                }
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(false);
                                    handleRegister(values);
                                }}>
                                {({ submitForm, isSubmitting }) => (
                                    <Form>
                                        <Field
                                            type="email"
                                            name="email"
                                            label="Email"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="text"
                                            name="username"
                                            label="Username"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="password"
                                            label="Password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="confirmPassword"
                                            label="Confirm Password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={submitForm}
                                            className={classes.formButton}>
                                            Register
                                        </Button>
                                        <Link to={AUTH_ROUTES.login}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                className={classes.formButton}>
                                                Login
                                            </Button>
                                        </Link>
                                        {isSubmitting && (
                                            <LinearProgress
                                                color="secondary"
                                                className={
                                                    classes.linearProgress
                                                }
                                            />
                                        )}
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default Register;

export interface IValues {
    username: string;
    password: string;
    confirmPassword: string;
    email: string;
}

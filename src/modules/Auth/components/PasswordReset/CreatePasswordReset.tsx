import React from 'react';
import { Box, Grid, LinearProgress, Typography } from '@material-ui/core';

import { AUTH_ROUTES } from 'modules/Auth/routes/const';
import authActions from 'modules/Auth/redux/actions';
import { AuthenticationField } from 'components/TextFields';
import Button from 'components/Buttons';
import { Field, Form, Formik, FormikProps } from 'formik';
import { Header } from 'components/Layout';
import { Link } from 'react-router-dom';
import { validate } from 'utils/valdation';
import { ValidationType } from 'utils/valdation/const';
import { useDispatch } from 'react-redux';

import useStyles from '../index.styles';

import { ICreatePasswordResetValues } from './const';

const initialValues: ICreatePasswordResetValues = {
    email: ''
};

const CreatePasswordReset: React.FC = () => {
    const classes = useStyles();
    const formikRef = React.useRef<FormikProps<ICreatePasswordResetValues>>(
        null
    );

    const dispatch = useDispatch();

    const handleCreateReset = React.useCallback(
        (values: ICreatePasswordResetValues) => {
            dispatch(authActions.createPasswordReset(values.email));
        },
        [dispatch]
    );

    return (
        <Box className="background_main">
            <Header />
            <Box className="container container_flex container_100vh">
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={6} lg={5}>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="body1"
                                className={classes.title}
                                paragraph>
                                Enter your email
                            </Typography>
                            <Formik
                                innerRef={formikRef}
                                initialValues={initialValues}
                                validate={values =>
                                    validate(
                                        ValidationType.createPasswordReset,
                                        values
                                    )
                                }
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(false);
                                    handleCreateReset(values);
                                }}>
                                {({ submitForm, isSubmitting }) => (
                                    <Form>
                                        <Field
                                            type="email"
                                            name="email"
                                            label="Email"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={submitForm}
                                            className={classes.formButton}>
                                            Send Instructions
                                        </Button>
                                        <Link to={AUTH_ROUTES.login}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                className={classes.formButton}>
                                                Login
                                            </Button>
                                        </Link>
                                        {isSubmitting && (
                                            <LinearProgress
                                                color="secondary"
                                                className={
                                                    classes.linearProgress
                                                }
                                            />
                                        )}
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default CreatePasswordReset;

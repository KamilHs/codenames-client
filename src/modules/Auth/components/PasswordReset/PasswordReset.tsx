import React from 'react';
import { AuthenticationField } from 'components/TextFields';
import { Box, Grid, LinearProgress, Typography } from '@material-ui/core';
import { Field, Form, Formik, FormikProps } from 'formik';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import authActions from 'modules/Auth/redux/actions';
import { AUTH_ROUTES } from 'modules/Auth/routes/const';
import Button from 'components/Buttons';
import { Header } from 'components/Layout';
import { validate } from 'utils/valdation';
import { ValidationType } from 'utils/valdation/const';

import useStyles from '../index.styles';

import { IPasswordResetUseParams, IPasswordResetValues } from './const';
import { authApi } from 'utils/api';

const initialValues: IPasswordResetValues = {
    password: '',
    confirmPassword: ''
};

const PasswordReset: React.FC = () => {
    const [isValid, setIsValid] = React.useState<boolean>(false);
    const formikRef = React.useRef<FormikProps<IPasswordResetValues>>(null);

    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();
    const { resetId } = useParams<IPasswordResetUseParams>();

    const handleCreateReset = React.useCallback(
        (values: IPasswordResetValues) => {
            if (!isValid) return;
            dispatch(
                authActions.confirmPasswordReset(values.password, resetId)
            );
        },
        [resetId, isValid, dispatch]
    );

    React.useEffect(() => {
        const getPasswordReset = async () => {
            const {
                data: { data: response }
            } = await authApi.getPasswordReset(resetId);

            response && response.success
                ? setIsValid(true)
                : history.push(AUTH_ROUTES.login);
        };

        getPasswordReset();
    }, [history, resetId]);

    return (
        <Box className="background_main">
            <Header />
            <Box className="container container_flex container_100vh">
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={6} lg={5}>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="body1"
                                className={classes.title}
                                paragraph>
                                Reset Password
                            </Typography>
                            <Formik
                                innerRef={formikRef}
                                initialValues={initialValues}
                                validate={values =>
                                    validate(
                                        ValidationType.passwordReset,
                                        values
                                    )
                                }
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(false);
                                    handleCreateReset(values);
                                }}>
                                {({ submitForm, isSubmitting }) => (
                                    <Form>
                                        <Field
                                            type="password"
                                            name="password"
                                            label="New password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Field
                                            type="password"
                                            name="confirmPassword"
                                            label="Confirm password"
                                            className={classes.formField}
                                            component={AuthenticationField}
                                            autoComplete="new-password"
                                        />
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={isSubmitting}
                                            onClick={submitForm}
                                            className={classes.formButton}>
                                            Reset password
                                        </Button>
                                        <Link to={AUTH_ROUTES.login}>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                className={classes.formButton}>
                                                Login
                                            </Button>
                                        </Link>
                                        {isSubmitting && (
                                            <LinearProgress
                                                color="secondary"
                                                className={
                                                    classes.linearProgress
                                                }
                                            />
                                        )}
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default PasswordReset;

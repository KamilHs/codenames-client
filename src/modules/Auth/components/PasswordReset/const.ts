export interface ICreatePasswordResetValues {
    email: string;
}

export interface IPasswordResetValues {
    password: string;
    confirmPassword: string;
}

export interface IPasswordResetUseParams {
    resetId: string;
}

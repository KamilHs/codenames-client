import { FetchStatus } from 'store/const';

import {
    DashboardActionsType,
    IDashboardState,
    SET_DASHBOARD_STATUS,
    SET_FRIENDS,
    SET_MESSAGES,
    SET_NOTIFICATIONS,
    SET_OPENED_CHAT,
    SET_SEARCHED_USER,
    SET_SOCKET_CONNECTED
} from './const';

export const initialState: IDashboardState = {
    status: FetchStatus.none,
    connected: false,
    friends: [],
    openedChat: null,
    notifications: [],
    searchedUser: null,
    messages: []
};

const reducer = (
    state: IDashboardState = initialState,
    action: DashboardActionsType
) => {
    switch (action.type) {
        case SET_SEARCHED_USER:
            return { ...state, searchedUser: action.payload };
        case SET_NOTIFICATIONS:
            return { ...state, notifications: action.payload };
        case SET_DASHBOARD_STATUS:
            return { ...state, status: action.payload };
        case SET_SOCKET_CONNECTED:
            return { ...state, connected: action.payload };
        case SET_FRIENDS:
            return { ...state, friends: action.payload };
        case SET_OPENED_CHAT:
            return { ...state, openedChat: action.payload };
        case SET_MESSAGES:
            return { ...state, messages: action.payload };
        default:
            return state;
    }
};

export default reducer;

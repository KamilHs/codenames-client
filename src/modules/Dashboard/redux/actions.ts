import { Action } from 'redux';
import { push } from 'connected-react-router';
import { ThunkAction } from 'redux-thunk';

import { dashboardApi } from 'utils/api';
import { FetchStatus } from 'store/const';
import { handleError } from 'utils/redux';
import { popupActions } from 'store/global/popup.actions';
import { ROOM_PREFIX } from 'modules/Rooms/routes/const';
import { RootState } from 'store';

import {
    DashboardActionsType,
    IFriend,
    IMessage,
    INotification,
    ISearchedUser,
    SET_DASHBOARD_STATUS,
    SET_FRIENDS,
    SET_MESSAGES,
    SET_NOTIFICATIONS,
    SET_OPENED_CHAT,
    SET_SEARCHED_USER,
    SET_SOCKET_CONNECTED
} from './const';

const dashboardActions = {
    setMessages: (messages: IMessage[]): DashboardActionsType => ({
        type: SET_MESSAGES,
        payload: messages
    }),
    setSearchedUser: (
        searchedUser: ISearchedUser | null
    ): DashboardActionsType => ({
        type: SET_SEARCHED_USER,
        payload: searchedUser
    }),
    setNotifications: (
        notifications: INotification[]
    ): DashboardActionsType => ({
        type: SET_NOTIFICATIONS,
        payload: notifications
    }),
    setFriends: (friends: IFriend[]): DashboardActionsType => ({
        type: SET_FRIENDS,
        payload: friends
    }),
    setOpenedChat: (chatId: number | null): DashboardActionsType => ({
        type: SET_OPENED_CHAT,
        payload: chatId
    }),
    setSocketConnected: (connected: boolean): DashboardActionsType => ({
        type: SET_SOCKET_CONNECTED,
        payload: connected
    }),
    setDashboardStatus: (status: FetchStatus): DashboardActionsType => ({
        type: SET_DASHBOARD_STATUS,
        payload: status
    }),
    createRoom: (
        token: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(dashboardActions.setDashboardStatus(FetchStatus.loading));
            const {
                data: { data: result }
            } = await dashboardApi.createRoom(token);

            if (result && result.success) {
                dispatch(
                    dashboardActions.setDashboardStatus(FetchStatus.success)
                );
                dispatch(
                    popupActions.addSuccessPopup('Game created successfully')
                );
                dispatch(push(`${ROOM_PREFIX}/${result.slug}`));
            } else {
                dispatch(
                    dashboardActions.setDashboardStatus(FetchStatus.failure)
                );
            }
        } catch (error) {
            handleError(error, dispatch);
            dispatch(dashboardActions.setDashboardStatus(FetchStatus.failure));
        }
    },
    getActiveGame: (
        token: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(dashboardActions.setDashboardStatus(FetchStatus.loading));
            const {
                data: { data: result }
            } = await dashboardApi.getActiveRoom(token);

            if (result && result.success) {
                dispatch(
                    popupActions.addSuccessPopup('Returned to active game')
                );
                dispatch(push(`${ROOM_PREFIX}/${result.slug}`));
            } else {
                dispatch(
                    dashboardActions.setDashboardStatus(FetchStatus.failure)
                );
            }
        } catch {
            dispatch(dashboardActions.setDashboardStatus(FetchStatus.failure));
        }
    }
};

export default dashboardActions;

import { FetchStatus } from 'store/const';

export const SET_DASHBOARD_STATUS = 'SET_DASHBOARD_STATUS';
export const SET_SOCKET_CONNECTED = 'SET_SOCKET_CONNECTED';
export const SET_FRIENDS = 'SET_FRIENDS';
export const SET_OPENED_CHAT = 'SET_OPENED_CHAT';
export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';
export const SET_SEARCHED_USER = 'SET_SEARCHED_USER';
export const SET_MESSAGES = 'SET_MESSAGES';

export enum NotificationTypeEnum {
    friendRequestSent = 'friendRequestSent',
    friendRequestAccepted = 'friendRequestAccepted',
    joinGame = 'joinGame'
}

export enum SearchedUserState {
    blocked = 'blocked',
    neutral = 'neutral'
}

export interface ISearchedUser {
    id: number;
    username: string;
    avatar: string;
    state: SearchedUserState;
}

export interface INotification {
    id: number;
    userId: number;
    username: string;
    type: NotificationTypeEnum;
    slug?: string;
    friendId?: number;
}

export interface IFriend {
    avatar: string;
    username: string;
    slug?: string;
    chatId: number;
    userId: number;
    friendId: number;
    isOnline: boolean;
}

export interface IMessage {
    authorId: number;
    content: string;
    date: Date;
}

interface ISetSearchedUser {
    type: typeof SET_SEARCHED_USER;
    payload: ISearchedUser | null;
}
interface ISetDashboardStatus {
    type: typeof SET_DASHBOARD_STATUS;
    payload: FetchStatus;
}

interface ISetSocketConnected {
    type: typeof SET_SOCKET_CONNECTED;
    payload: boolean;
}

interface ISetFriends {
    type: typeof SET_FRIENDS;
    payload: IFriend[];
}

interface ISetOpenedChat {
    type: typeof SET_OPENED_CHAT;
    payload: number | null;
}

interface ISetNotifications {
    type: typeof SET_NOTIFICATIONS;
    payload: INotification[];
}

interface ISetMessages {
    type: typeof SET_MESSAGES;
    payload: IMessage[];
}

export type DashboardActionsType =
    | ISetDashboardStatus
    | ISetSocketConnected
    | ISetFriends
    | ISetOpenedChat
    | ISetNotifications
    | ISetSearchedUser
    | ISetMessages;

export interface IDashboardState {
    status: FetchStatus;
    connected: boolean;
    friends: IFriend[];
    openedChat: number | null;
    notifications: INotification[];
    searchedUser: ISearchedUser | null;
    messages: IMessage[];
}

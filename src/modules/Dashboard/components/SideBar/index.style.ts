import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    drawerContent: {
        display: 'flex',
        flexDirection: 'column',
        width: '350px',
        height: '100%'
    },
    friendList: {
        flexGrow: 2,
        overflowY: 'auto'
    },
    drawer: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.secondary.main,
        boxShadow: `-2px -4px 13px 1px ${theme.palette.secondary.main}`
    },
    heading: {
        textAlign: 'center',
        fontFamily: '"Press Start 2P"',
        fontSize: '27px'
    },
    inputBase: {
        fontFamily: 'Ticketing',
        flexGrow: 2,
        color: theme.palette.secondary.main
    },
    typography: {
        fontFamily: 'Ticketing',
        marginLeft: theme.spacing(2)
    },
    divider: {
        background: theme.palette.secondary.main
    },
    trigger: {
        position: 'fixed',
        zIndex: 2,
        top: theme.spacing(3),
        right: theme.spacing(5)
    },
    listItem: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    menuItem: {
        fontFamily: 'Ticketing',
        color: theme.palette.primary.main
    },
    menuPaper: {
        backgroundColor: theme.palette.secondary.main,
        borderRadius: `${theme.shape.borderRadius * 0.5}px`
    }
}));

import React from 'react';
import {
    Box,
    Typography,
    Avatar,
    IconButton,
    ListItem,
    MenuItem,
    Menu
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { IFriend } from 'modules/Dashboard/redux/const';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import StyledBadge from '../../../../components/Badge/StyledBadge';

import useStyles from './index.style';

type FriendProps = IFriend & {
    handleOpenChat: (chatId: number) => void;
    handleRemoveFriend: (friendId: number) => void;
    handleBlock: (userId: number) => void;
};

const Friend: React.FC<FriendProps> = ({
    avatar,
    userId,
    username,
    chatId,
    friendId,
    isOnline,
    handleOpenChat,
    handleRemoveFriend,
    handleBlock
}) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const handleMenuClick = React.useCallback(
        (event: React.MouseEvent<HTMLElement>) =>
            setAnchorEl(event.currentTarget),
        []
    );

    const classes = useStyles();

    return (
        <ListItem key={friendId} className={classes.listItem}>
            <Box display="flex" alignItems="center">
                {isOnline ? (
                    <StyledBadge
                        overlap="circle"
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right'
                        }}
                        variant="dot">
                        <Avatar src={avatar} alt={username} />
                    </StyledBadge>
                ) : (
                    <Avatar src={avatar} alt={username} />
                )}
                <Typography variant="body1" className={classes.typography}>
                    {username}
                </Typography>
            </Box>
            <Box>
                <IconButton onClick={handleMenuClick}>
                    <MoreVertIcon color="secondary" />
                </IconButton>
                <Menu
                    anchorEl={anchorEl}
                    open={open}
                    classes={{ paper: classes.menuPaper }}
                    onClose={() => setAnchorEl(null)}
                    keepMounted>
                    <MenuItem button>
                        <Link
                            to={`${PROFILE_ROUTES.profile}${userId}`}
                            className={classes.menuItem}>
                            Profile
                        </Link>
                    </MenuItem>
                    <MenuItem
                        className={classes.menuItem}
                        onClick={() => handleOpenChat(chatId)}
                        button>
                        Chat
                    </MenuItem>
                    <MenuItem
                        className={classes.menuItem}
                        onClick={() => handleRemoveFriend(friendId)}
                        button>
                        Remove
                    </MenuItem>
                    <MenuItem
                        className={classes.menuItem}
                        onClick={() => handleBlock(userId)}
                        button>
                        Block
                    </MenuItem>
                </Menu>
            </Box>
        </ListItem>
    );
};

export default Friend;

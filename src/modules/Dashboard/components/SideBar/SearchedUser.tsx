import React from 'react';
import {
    Box,
    Typography,
    Avatar,
    IconButton,
    MenuItem,
    Menu
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useSelector } from 'react-redux';

import { dashboardSocketActions } from 'sockets/dashboard/actions';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import { RootState } from 'store';
import { SearchedUserState } from 'modules/Dashboard/redux/const';
import { SocketContext } from 'context/SocketContext';

import useStyles from './index.style';

const SearchedUser: React.FC = () => {
    const classes = useStyles();
    const { socket } = React.useContext(SocketContext);
    const { searchedUser } = useSelector((state: RootState) => state.dashboard);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const handleMenuClick = React.useCallback(
        (event: React.MouseEvent<HTMLElement>) =>
            setAnchorEl(event.currentTarget),
        []
    );

    const handleAddFriend = React.useCallback(() => {
        if (
            !socket ||
            !searchedUser ||
            searchedUser.state === SearchedUserState.blocked
        )
            return;
        setAnchorEl(null);
        dashboardSocketActions.requestFriend(socket, searchedUser.id);
    }, [socket, searchedUser]);

    const handleBlock = React.useCallback(() => {
        if (
            !socket ||
            !searchedUser ||
            searchedUser.state === SearchedUserState.blocked
        )
            return;
        dashboardSocketActions.blockUser(socket, searchedUser.id);
    }, [socket, searchedUser]);

    const handleUnblock = React.useCallback(() => {
        if (
            !socket ||
            !searchedUser ||
            searchedUser.state === SearchedUserState.neutral
        )
            return;
        dashboardSocketActions.unblockUser(socket, searchedUser.id);
    }, [socket, searchedUser]);

    if (!searchedUser) return null;

    const { id, username, avatar, state } = searchedUser;

    return (
        <Box
            key={id}
            display="flex"
            justifyContent="space-between"
            padding="16px">
            <Box display="flex" alignItems="center">
                <Avatar src={avatar} alt={username} />
                <Typography variant="body1" className={classes.typography}>
                    {username}
                </Typography>
            </Box>
            <Box>
                <IconButton onClick={handleMenuClick}>
                    <MoreVertIcon color="secondary" />
                </IconButton>
                <Menu
                    anchorEl={anchorEl}
                    open={open}
                    classes={{ paper: classes.menuPaper }}
                    onClose={() => setAnchorEl(null)}
                    keepMounted>
                    <MenuItem button>
                        <Link
                            to={`${PROFILE_ROUTES.profile}${id}`}
                            className={classes.menuItem}>
                            Profile
                        </Link>
                    </MenuItem>
                    {state === SearchedUserState.neutral ? (
                        [
                            <MenuItem
                                key="add-friend"
                                className={classes.menuItem}
                                onClick={handleAddFriend}
                                button>
                                Add to Friends
                            </MenuItem>,
                            <MenuItem
                                key="block"
                                className={classes.menuItem}
                                onClick={handleBlock}
                                button>
                                Block
                            </MenuItem>
                        ]
                    ) : (
                        <MenuItem
                            className={classes.menuItem}
                            onClick={handleUnblock}
                            button>
                            Unblock
                        </MenuItem>
                    )}
                </Menu>
            </Box>
        </Box>
    );
};

export default SearchedUser;

import React from 'react';
import {
    Box,
    Button,
    Typography,
    Divider,
    IconButton,
    InputBase,
    List,
    ListItem,
    SwipeableDrawer
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from 'store';
import dashboardActions from 'modules/Dashboard/redux/actions';
import { dashboardSocketActions } from 'sockets/dashboard/actions';
import { SocketContext } from 'context/SocketContext';

import Friend from './Friend';
import Neutral from './SearchedUser';
import useStyles from './index.style';

const SwipeableTemporaryDrawer = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { socket } = React.useContext(SocketContext);
    const { friends, searchedUser } = useSelector(
        (state: RootState) => state.dashboard
    );
    const [open, setOpen] = React.useState<boolean>(false);
    const [search, setSearch] = React.useState<string>('');

    const handleOpenChat = React.useCallback(
        (chatId: number) => {
            if (!socket) return;
            dispatch(dashboardActions.setOpenedChat(chatId));
            dashboardSocketActions.joinChat(socket, chatId);
            setOpen(false);
        },
        [socket, dispatch]
    );

    const handleSearch = React.useCallback(() => {
        if (!socket) return;
        dashboardSocketActions.searchUser(socket, search);
    }, [socket, search]);

    const handleClear = React.useCallback(() => {
        dispatch(dashboardActions.setSearchedUser(null));
        setSearch('');
    }, [dispatch]);

    const handleClose = React.useCallback(() => {
        handleClear();
        setOpen(false);
    }, [handleClear]);

    const handleRemoveFriend = React.useCallback(
        (friendId: number) => {
            if (!socket) return;
            dashboardSocketActions.removeFriend(socket, friendId);
        },
        [socket]
    );

    const handleBlock = React.useCallback(
        (userId: number) => {
            if (!socket) return;
            dashboardSocketActions.blockUser(socket, userId);
        },
        [socket]
    );

    return (
        <>
            <Box className={classes.trigger}>
                <Button onClick={() => setOpen(true)}>
                    <MenuIcon color="secondary" />
                </Button>
            </Box>
            <Box>
                <SwipeableDrawer
                    anchor="right"
                    classes={{ paper: classes.drawer }}
                    open={open}
                    onClose={handleClose}
                    onOpen={() => setOpen(true)}
                    disableScrollLock>
                    <Typography className={classes.heading}>Friends</Typography>
                    <Divider classes={{ root: classes.divider }} />
                    <ListItem className={classes.listItem}>
                        <InputBase
                            placeholder="Add friend by email"
                            value={search}
                            onChange={e => setSearch(e.target.value.trim())}
                            className={classes.inputBase}
                        />
                        <Box>
                            <IconButton onClick={handleSearch}>
                                <SearchIcon color="secondary" />
                            </IconButton>
                            {search.length > 0 && (
                                <IconButton onClick={handleClear}>
                                    <CloseIcon color="secondary" />
                                </IconButton>
                            )}
                        </Box>
                    </ListItem>
                    <Box className={classes.drawerContent}>
                        {search.length === 0 && !searchedUser && (
                            <Box className={classes.friendList}>
                                <List>
                                    {friends?.map(friend => (
                                        <Friend
                                            key={friend.friendId}
                                            {...friend}
                                            handleOpenChat={handleOpenChat}
                                            handleRemoveFriend={
                                                handleRemoveFriend
                                            }
                                            handleBlock={handleBlock}
                                        />
                                    ))}
                                </List>
                            </Box>
                        )}
                        {searchedUser && <Neutral />}
                    </Box>
                </SwipeableDrawer>
            </Box>
        </>
    );
};

export default SwipeableTemporaryDrawer;

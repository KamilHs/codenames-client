import React from 'react';
import { Box } from '@material-ui/core';

import Chat from '../components/Chat';
import GameForm from '../components/GameForm';
import Header from '../../../components/Layout/Header/index';
import SideBar from '../components/SideBar';

const Dashboard: React.FC = () => (
    <Box>
        <Header />
        <SideBar />
        <Chat />
        <GameForm />
    </Box>
);

export default Dashboard;

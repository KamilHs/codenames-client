import React from 'react';
import {
    Box,
    Grid,
    LinearProgress,
    TextField,
    Typography
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import Button from 'components/Buttons';
import dashboardActions from 'modules/Dashboard/redux/actions';
import { FetchStatus } from 'store/const';
import { LocalStorageContext } from 'context/LocalStorageContext';
import { ROOM_PREFIX } from 'modules/Rooms/routes/const';
import roomActions from 'modules/Rooms/redux/actions';
import { RootState } from 'store';

import useStyles from './index.styles';

const GameForm: React.FC = () => {
    const classes = useStyles();
    const [slug, setSlug] = React.useState<string>('');
    const { token, status } = useSelector((state: RootState) => state.auth);
    const { checkAuth } = React.useContext(LocalStorageContext);

    const dispatch = useDispatch();
    const history = useHistory();

    const handleCreate = React.useCallback(() => {
        dispatch(dashboardActions.createRoom(token!));
    }, [token, dispatch]);

    const handleJoin = React.useCallback(() => {
        history.push(`${ROOM_PREFIX}/${slug}`);
    }, [history, slug]);

    const handleClick = React.useCallback(() => {
        dispatch(roomActions.clearRoomData());
        slug.length > 0 ? handleJoin() : handleCreate();
    }, [slug, handleJoin, handleCreate, dispatch]);

    React.useEffect(checkAuth, [checkAuth]);

    React.useEffect(() => {
        if (!token) return;
        dispatch(dashboardActions.getActiveGame(token));
    }, [token, dispatch]);

    return (
        <Box className="background_main">
            <Box className="container container_flex container_100vh">
                <Grid spacing={1} justify="center" container>
                    <Grid xs={12} sm={7} md={8} lg={9} item>
                        <Box className={classes.form}>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="h3"
                                className={classes.title}
                                paragraph>
                                {slug.length > 0 ? 'Join Game' : 'Create Game'}
                            </Typography>
                            <Box display="flex" flexDirection="column">
                                <TextField
                                    className={classes.formField}
                                    value={slug}
                                    onChange={({ target }) =>
                                        setSlug(target.value.trim())
                                    }
                                    label="Want to join? Enter slug of the existing game"
                                />

                                <Button
                                    color="primary"
                                    onClick={handleClick}
                                    className={classes.formButton}>
                                    {slug.length > 0
                                        ? 'Join Game'
                                        : 'Create Game'}
                                </Button>

                                {status === FetchStatus.loading && (
                                    <LinearProgress
                                        color="secondary"
                                        className={classes.linearProgress}
                                    />
                                )}
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
};

export default GameForm;

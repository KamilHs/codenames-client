import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    title: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(3),
        lineHeight: '48px',
        textTransform: 'uppercase'
    },
    form: {
        padding: theme.spacing(5),
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: `${theme.shape.borderRadius * 3}px`
    },
    formField: {
        width: '100%',
        padding: theme.spacing(1),
        marginBottom: theme.spacing(3),
        '& label': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& label.Mui-focused:not(.Mui-error)': {
            color: theme.palette.secondary.main
        },
        '& .MuiInput-underline:not(.Mui-error):after': {
            borderBottomColor: theme.palette.secondary.main
        },
        '& p.Mui-error': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& input': {
            color: theme.palette.secondary.main,
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        }
    },
    formButton: {
        display: 'block',
        margin: '20px auto 0',
        padding: `${theme.spacing(2)}px ${theme.spacing(5)}px`,
        marginBottom: theme.spacing(2),
        fontFamily: '"Press Start 2P"'
    },
    linearProgress: {
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.primary.dark
    }
}));

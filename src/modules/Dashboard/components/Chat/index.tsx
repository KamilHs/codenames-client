import React from 'react';
import {
    Avatar,
    Box,
    List,
    ListItem,
    Typography,
    Dialog,
    IconButton
} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import { useDispatch, useSelector } from 'react-redux';

import dashboardActions from 'modules/Dashboard/redux/actions';
import { dashboardSocketActions } from 'sockets/dashboard/actions';
import { IFriend } from 'modules/Dashboard/redux/const';
import { RootState } from 'store';
import StyledBadge from '../../../../components/Badge/StyledBadge';
import { SocketContext } from 'context/SocketContext';

import Message from './Message';
import useStyles from './index.style';

const Chat: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const divRef = React.useRef<HTMLDivElement>(null);
    const { socket } = React.useContext(SocketContext);
    const { openedChat, friends, messages, user } = useSelector(
        (state: RootState) => ({
            ...state.dashboard,
            ...state.auth
        })
    );
    const [message, setMessage] = React.useState<string>('');
    const friend = React.useMemo<IFriend | undefined>(
        () => friends.find(friend => friend.chatId === openedChat),
        [friends, openedChat]
    );

    const handleLeave = React.useCallback(() => {
        if (!socket || openedChat === null) return;
        dispatch(dashboardActions.setOpenedChat(null));
        dispatch(dashboardActions.setMessages([]));
        dashboardSocketActions.leaveChat(socket, openedChat);
    }, [socket, openedChat, dispatch]);

    const sendMessage = React.useCallback(() => {
        const trimmed = message.trim();

        if (trimmed.length === 0 || !socket || openedChat === null) return;

        setMessage('');
        dashboardSocketActions.sendMessage(socket, trimmed, openedChat);
    }, [message, openedChat, socket]);

    React.useLayoutEffect(() => {
        if (!divRef.current) return;
        divRef.current.scrollIntoView();
    }, [messages]);

    return friend ? (
        <Dialog open={openedChat !== null} onClose={handleLeave} fullWidth>
            <Box className={classes.chatHeader}>
                <Box>
                    {friend.isOnline ? (
                        <StyledBadge
                            overlap="circle"
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right'
                            }}
                            variant="dot">
                            <Avatar src={friend.avatar} alt={friend.username} />
                        </StyledBadge>
                    ) : (
                        <Avatar src={friend.avatar} alt={friend.username} />
                    )}
                </Box>
                <Box className={classes.username}>
                    <Typography className={classes.usernameFont}>
                        {friend.username}
                    </Typography>
                </Box>
            </Box>
            <Box className={classes.chat}>
                <List>
                    {messages.map((message, index) => (
                        <ListItem
                            disableGutters
                            key={`${message.content}${message.date}${
                                index * 2
                            }`}>
                            <Message
                                {...message}
                                isOwn={message.authorId === user?.id}
                            />
                        </ListItem>
                    ))}
                </List>
                <div ref={divRef} />
            </Box>
            <form
                onSubmit={e => {
                    e.preventDefault();
                    sendMessage();
                }}>
                <Box className={classes.chatInput}>
                    <Box width="100%">
                        <input
                            className={classes.inputFont}
                            placeholder="Type a message"
                            value={message}
                            onChange={e => setMessage(e.target.value)}
                        />
                    </Box>
                    <Box>
                        <IconButton
                            onClick={sendMessage}
                            disableRipple
                            disableFocusRipple>
                            <SendIcon />
                        </IconButton>
                    </Box>
                </Box>
            </form>
        </Dialog>
    ) : null;
};

export default Chat;

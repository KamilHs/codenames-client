import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    chat: {
        height: '600px',
        color: theme.palette.secondary.main,
        backgroundColor: theme.palette.primary.main,
        overflowY: 'auto'
    },
    messageContainer: {
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-end',
        paddingRight: theme.spacing(1.5)
    },
    messageContainerNotOwn: {
        display: 'flex',
        width: '100%',
        justifyContent: 'flex-start',
        paddingLeft: theme.spacing(1.5)
    },
    message: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.secondary.main,
        maxWidth: '70%',
        border: `solid 1px ${theme.palette.secondary.main}`,
        padding: theme.spacing(0, 0.5, 0, 1),
        borderRadius: theme.spacing(0.5)
    },
    messageNotOwn: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.primary.main,
        maxWidth: '70%',
        padding: theme.spacing(0, 0.5, 0, 1),
        borderRadius: theme.spacing(0.5)
    },
    username: {
        position: 'relative',
        textAlign: 'center',
        marginLeft: theme.spacing(1.5)
    },
    messageFont: {
        fontFamily: 'Ticketing'
    },
    chatHeader: {
        display: 'flex',
        padding: theme.spacing(1.5),
        color: theme.palette.primary.main,
        borderBottom: `solid 2px  ${theme.palette.secondary.main}`
    },
    chatInput: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: theme.spacing(0.5, 1.5),
        borderTop: `solid 2px  ${theme.palette.secondary.main}`
    },
    usernameFont: {
        position: 'absolute',
        top: '-50%',
        transform: 'translateY(100%)',
        fontFamily: '"Press Start 2P"'
    },
    inputFont: {
        width: '100%',
        padding: '10px',
        color: theme.palette.primary.main,
        fontFamily: 'Ticketing',
        fontSize: '24px',
        borderRadius: theme.shape.borderRadius,
        flexGrow: 2
    }
}));

import React from 'react';

import { IMessage } from 'modules/Dashboard/redux/const';

import useStyles from './index.style';
import { Box, Typography } from '@material-ui/core';

type MessageProps = IMessage & { isOwn: boolean };

const Message: React.FC<MessageProps> = ({ content, isOwn }) => {
    const classes = useStyles();

    return (
        <Box
            className={
                isOwn
                    ? classes.messageContainer
                    : classes.messageContainerNotOwn
            }>
            <Box className={isOwn ? classes.message : classes.messageNotOwn}>
                <Typography className={classes.messageFont}>
                    {content}
                </Typography>
            </Box>
        </Box>
    );
};

export default Message;

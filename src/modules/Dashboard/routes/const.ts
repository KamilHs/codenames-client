export const DASHBOARD_PREFIX = '/dashboard';

export const DASHBOARD_ROUTES = {
    main: `${DASHBOARD_PREFIX}/`
};

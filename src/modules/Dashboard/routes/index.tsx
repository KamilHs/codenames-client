import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { AUTH_PREFIX } from 'modules/Auth/routes/const';
import { RootState } from 'store';

import { DASHBOARD_PREFIX, DASHBOARD_ROUTES } from './const';
import Dashboard from '../components/index';

export const DashboardRoutes: React.FC = () => {
    const { isAuthenticated, token, user } = useSelector(
        (state: RootState) => state.auth
    );
    const render = React.useCallback(
        () =>
            !isAuthenticated || !token || !user ? (
                <Redirect to={AUTH_PREFIX} />
            ) : (
                <Switch>
                    <Route
                        path={DASHBOARD_ROUTES.main}
                        component={Dashboard}
                        exact
                    />
                </Switch>
            ),
        [isAuthenticated, token, user]
    );

    return <Route path={DASHBOARD_PREFIX} render={render} />;
};

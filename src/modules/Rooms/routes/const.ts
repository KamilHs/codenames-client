export const ROOM_PREFIX = '/rooms';

export const ROOM_ROUTES = {
    main: `${ROOM_PREFIX}/:slug`
};

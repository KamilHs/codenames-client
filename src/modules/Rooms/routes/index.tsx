import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { AUTH_PREFIX } from 'modules/Auth/routes/const';
import Room from '../components/';
import { RootState } from 'store';

import { ROOM_PREFIX, ROOM_ROUTES } from './const';

export const RoomRoutes: React.FC = () => {
    const { isAuthenticated, user, token } = useSelector(
        (state: RootState) => state.auth
    );
    const render = React.useCallback(
        () =>
            !isAuthenticated || !user || !token ? (
                <Redirect to={AUTH_PREFIX} />
            ) : (
                <Switch>
                    <Route path={ROOM_ROUTES.main} component={Room} exact />
                </Switch>
            ),
        [isAuthenticated, user, token]
    );

    return <Route path={ROOM_PREFIX} render={render} />;
};

import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    lobby: {
        maxWidth: '500px',
        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(2)
        },
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(1)
        },
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: `${theme.shape.borderRadius * 3}px`
    },
    sectionTitle: {
        fontFamily: '"Press Start 2P"',
        textAlign: 'center'
    },
    languages: {
        marginTop: theme.spacing(1),
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap'
    },
    language: {
        cursor: 'pointer',
        width: '40px',
        height: '40px',
        borderRadius: '20px',
        overflow: 'hidden',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: `0 ${theme.spacing(1)}px`
    },
    selectedLanguage: {
        boxShadow: `0 0 10px 4px ${theme.palette.secondary.main}`
    },
    deck: {
        textAlign: 'left',
        color: theme.palette.secondary.main,
        justifyContent: 'space-between',
        '& span': {
            textAlign: 'left',
            fontFamily: 'Ticketing'
        }
    },
    startButton: {
        display: 'block',
        margin: '0 auto',
        marginTop: theme.spacing(2)
    },
    linearProgress: {
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.primary.dark
    }
}));

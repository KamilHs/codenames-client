/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import {
    Box,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    LinearProgress,
    Typography
} from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';

import Button from 'components/Buttons';
import { Deck, Language } from 'modules/Rooms/redux/const';
import { RootState } from 'store';
import { SocketContext } from 'context/SocketContext';

import useStyles from './index.styles';
import { gameSocketActions } from 'sockets/game/actions';
import roomActions from 'modules/Rooms/redux/actions';
import { FetchStatus } from 'store/const';

type LobbyProps = {
    slug: string;
    languages: Language[];
};

const Lobby: React.FC<LobbyProps> = ({ languages, slug }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { socket } = React.useContext(SocketContext);
    const { status } = useSelector((state: RootState) => state.room);

    const [languageId, setLanguageId] = React.useState<number>(-1);
    const currentDecks = React.useMemo<Deck[]>(
        () =>
            languages.find(language => language.id === languageId)?.decks ?? [],
        [languages, languageId]
    );
    const [checkedDeckIds, setCheckedDeckIds] = React.useState<number[]>([]);

    const handleDeckCheck = React.useCallback(
        deckId =>
            setCheckedDeckIds(
                checkedDeckIds.includes(deckId)
                    ? checkedDeckIds.filter(id => id !== deckId)
                    : [...checkedDeckIds, deckId]
            ),

        [checkedDeckIds]
    );

    const handleStartGame = React.useCallback(() => {
        if (!socket || status === FetchStatus.loading) return;
        dispatch(roomActions.setRoomStatus(FetchStatus.loading));
        gameSocketActions.startGame(socket, slug, checkedDeckIds);
    }, [dispatch, status, socket, slug, checkedDeckIds]);

    React.useEffect(() => {
        setCheckedDeckIds([]);
    }, [currentDecks]);

    React.useEffect(() => {
        setLanguageId(languages.length > 0 ? languages[0].id : -1);
    }, [languages]);

    return (
        <Box className={classes.lobby}>
            <Typography
                color="secondary"
                variant="h5"
                className={classes.sectionTitle}>
                Game Settings
            </Typography>
            <Box marginY={3}>
                <Typography
                    color="secondary"
                    variant="body1"
                    className={classes.sectionTitle}>
                    Languages
                </Typography>
                <Box className={classes.languages}>
                    {languages.map(({ id, logo }) => (
                        <Box
                            key={id}
                            className={clsx(
                                classes.language,
                                id === languageId
                                    ? classes.selectedLanguage
                                    : ''
                            )}
                            onClick={() => setLanguageId(id)}>
                            <img
                                style={{
                                    width: '100%',
                                    height: '100%'
                                }}
                                src={logo}
                                alt={logo}
                            />
                        </Box>
                    ))}
                </Box>
            </Box>
            <Box>
                <Typography
                    color="secondary"
                    variant="body1"
                    className={classes.sectionTitle}>
                    Decks
                </Typography>
                <FormGroup>
                    {currentDecks.map(({ id, name, length }) => (
                        <FormControlLabel
                            key={id}
                            label={`${name} (${length})`}
                            className={classes.deck}
                            labelPlacement="start"
                            control={
                                <Checkbox
                                    value={id}
                                    checked={checkedDeckIds.includes(id)}
                                    onChange={() => handleDeckCheck(id)}
                                />
                            }
                        />
                    ))}
                </FormGroup>
            </Box>
            <Button
                className={classes.startButton}
                fontFamily='"Press Start 2P"'
                color={
                    checkedDeckIds.length === 0 ||
                    status === FetchStatus.loading
                        ? 'secondary'
                        : 'primary'
                }
                variant="outlined"
                disabled={
                    checkedDeckIds.length === 0 ||
                    status === FetchStatus.loading
                }
                onClick={handleStartGame}>
                Start game
            </Button>
            {status === FetchStatus.loading && (
                <LinearProgress
                    color="secondary"
                    className={classes.linearProgress}
                />
            )}
        </Box>
    );
};

export default Lobby;

export interface IUseParams {
    slug: string;
}

export const TEAM_MAIN_IMAGE_PREFIX = '/cards/main';

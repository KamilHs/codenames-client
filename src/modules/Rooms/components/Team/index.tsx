import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';

import Button from 'components/Buttons';
import {
    IPlayer,
    ITeam,
    PlayerRoleEnum,
    TeamTypeEnum
} from 'modules/Rooms/redux/const';

import useStyles from './index.styles';
import { useSelector } from 'react-redux';
import { RootState } from 'store';
import { TEAM_MAIN_IMAGE_PREFIX } from 'modules/Rooms/components/const';

type TeamProps = {
    team: ITeam;
    userId: number;
    handleJoin: (role: PlayerRoleEnum) => void;
    handleLeave: () => void;
};

const teamColors: Record<TeamTypeEnum, string> = {
    [TeamTypeEnum.blue]: '#1d62c8',
    [TeamTypeEnum.red]: '#ab2323'
};

const Team: React.FC<TeamProps> = ({
    team,
    userId,
    handleJoin,
    handleLeave
}) => {
    const classes = useStyles();
    const { leftCards } = useSelector((state: RootState) => state.room);

    const userPlayer = React.useMemo<IPlayer | null>(
        () => team.players.find(player => player.userId === userId) ?? null,
        [team, userId]
    );

    const spymaster = React.useMemo<IPlayer | null>(
        () =>
            team.players.find(
                ({ role }) => role === PlayerRoleEnum.spymaster
            ) ?? null,
        [team]
    );

    const operatives = React.useMemo<IPlayer[]>(
        () =>
            team.players.filter(
                ({ role }) => role === PlayerRoleEnum.operative
            ),
        [team]
    );

    const isRed = React.useMemo(() => team.type === TeamTypeEnum.red, [team]);

    return (
        <Box
            style={{ background: teamColors[team.type] }}
            className={classes.team}>
            <Box
                display="flex"
                justifyContent="center"
                flexDirection={isRed ? 'row-reverse' : 'row'}
                alignItems="center">
                <Typography
                    variant="h5"
                    color="secondary"
                    className={clsx(
                        classes.cardsLeft,
                        isRed ? classes.redCardsLeft : classes.blueCardsLeft
                    )}>
                    {leftCards ? leftCards[team.type] : ''}
                </Typography>
                <Box>
                    <img
                        style={{ maxWidth: '100%' }}
                        src={`${TEAM_MAIN_IMAGE_PREFIX}/${team.type}.png`}
                        alt="hero"
                    />
                </Box>
            </Box>
            <Box
                marginY={2}
                display="flex"
                alignItems="center"
                flexDirection="column">
                {spymaster ? (
                    <Typography
                        variant="body2"
                        color="secondary"
                        className={classes.member}>
                        Spymaster: {spymaster.username}
                    </Typography>
                ) : (
                    <Button
                        fontFamily="Ticketing"
                        color="primary"
                        variant="outlined"
                        size="small"
                        onClick={() => handleJoin(PlayerRoleEnum.spymaster)}>
                        Join as spymaster
                    </Button>
                )}
            </Box>
            <Box
                marginY={2}
                display="flex"
                alignItems="center"
                flexDirection="column">
                <Box>
                    {operatives.map(({ id, username }) => (
                        <Box key={id}>
                            <Typography
                                variant="body2"
                                color="secondary"
                                className={classes.member}>
                                {username}
                            </Typography>
                        </Box>
                    ))}
                </Box>
                {(!userPlayer ||
                    userPlayer.role === PlayerRoleEnum.spymaster) && (
                    <Button
                        fontFamily="Ticketing"
                        color="primary"
                        variant="outlined"
                        size="small"
                        onClick={() => handleJoin(PlayerRoleEnum.operative)}>
                        Join as operative
                    </Button>
                )}
            </Box>
            {userPlayer && (
                <Box display="flex" alignItems="center" flexDirection="column">
                    <Button
                        fontFamily="Ticketing"
                        color="primary"
                        variant="outlined"
                        size="small"
                        onClick={handleLeave}>
                        Leave team
                    </Button>
                </Box>
            )}
        </Box>
    );
};

export default Team;

import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    team: {
        overflowY: 'auto',
        padding: theme.spacing(3),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(2)
        },
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(1)
        },
        width: '100%',
        maxHeight: '100%'
    },
    cardsLeft: {
        fontFamily: '"Press Start 2P"'
    },
    member: {
        fontFamily: 'Ticketing'
    },
    redCardsLeft: {
        marginLeft: theme.spacing(1)
    },
    blueCardsLeft: {
        marginRight: theme.spacing(1)
    }
}));

import React from 'react';
import { Backdrop, Box, Grid } from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';

import Button from 'components/Buttons';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { gameSocketActions } from 'sockets/game/actions';
import { GameStatus, PlayerRoleEnum } from '../redux/const';
import { LocalStorageContext } from 'context/LocalStorageContext';
import roomActions from 'modules/Rooms/redux/actions';
import { roomSocketActions } from 'sockets/room/actions';
import { RootState } from 'store';
import { SocketContext } from 'context/SocketContext';

import Board from './Board';
import Gamelogs from './Gamelogs';
import { IUseParams } from './const';
import Lobby from './Lobby';
import Members from './Members';
import Team from './Team';
import useStyles from './index.styles';
import Winner from './Winner';

const Room: React.FC = () => {
    const { slug } = useParams<IUseParams>();
    const { socket } = React.useContext(SocketContext);
    const { checkAuth } = React.useContext(LocalStorageContext);

    const {
        token,
        isAuthenticated,
        game,
        joined,
        connected,
        languages,
        teams,
        user,
        tabDisabled,
        winner
    } = useSelector((state: RootState) => ({
        ...state.auth,
        ...state.room,
        ...state.dashboard
    }));
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();

    const handleJoinTeam = React.useCallback(
        (teamId: number, role: PlayerRoleEnum) => {
            if (tabDisabled || !game || !socket) return;
            roomSocketActions.joinTeam(socket, game.slug, teamId, role);
        },
        [tabDisabled, socket, game]
    );

    const handleLeaveTeam = React.useCallback(() => {
        if (tabDisabled || !game || !socket) return;
        dispatch(roomActions.setPlayerData(null));
        roomSocketActions.leaveTeam(socket, game.slug);
    }, [tabDisabled, game, socket, dispatch]);

    const handleResetTeams = React.useCallback(() => {
        if (tabDisabled || !game || !socket) return;
        roomSocketActions.resetTeams(socket, game.slug);
    }, [tabDisabled, game, socket]);

    const handleLeaveRoom = React.useCallback(() => {
        if (!socket || !game) return;

        dispatch(roomActions.clearRoomData());
        roomSocketActions.leaveRoom(socket, game.slug);
        history.push(DASHBOARD_ROUTES.main);
    }, [history, socket, game, dispatch]);

    React.useEffect(checkAuth, [checkAuth]);

    React.useEffect(() => {
        if (tabDisabled || !token || !isAuthenticated) return;
        const getRoom = async () => {
            dispatch(roomActions.getRoom(token, slug));
        };

        getRoom();
    }, [tabDisabled, token, slug, isAuthenticated, dispatch]);

    React.useEffect(() => {
        if (tabDisabled || !game || !socket || !connected) return;
        if (joined) {
            if (
                !game.isCreator ||
                game.status !== GameStatus.notStarted ||
                languages.length > 0
            )
                return;
            gameSocketActions.getDecksData(socket, game.slug);
        } else {
            roomSocketActions.joinRoom(socket, slug);
            gameSocketActions.getGameData(socket, slug);
            dispatch(roomActions.setJoined(true));
        }
    }, [
        tabDisabled,
        joined,
        slug,
        game,
        socket,
        connected,
        languages,
        dispatch
    ]);

    if (!game || teams.length === 0 || !user) return null;

    return (
        <>
            {game.status === GameStatus.finished && winner && (
                <Winner winner={winner} handleLeave={handleLeaveRoom} />
            )}
            <Backdrop open={tabDisabled} style={{ zIndex: 100 }}>
                <Button
                    color="primary"
                    variant="contained"
                    fontFamily='"Press Start 2P"'
                    onClick={() => window.location.reload()}>
                    Refresh
                </Button>
            </Backdrop>
            <Box className={clsx('background_main', classes.wrapper)}>
                <Members handleLeave={handleLeaveRoom} />
                {game.isCreator && game.status === GameStatus.notStarted && (
                    <Box className={classes.buttonWrapper}>
                        <Button
                            variant="outlined"
                            fontFamily='"Press Start 2P"'
                            color="primary"
                            onClick={handleResetTeams}>
                            Reset
                        </Button>
                    </Box>
                )}
                <Box
                    className="container_100vh"
                    paddingTop={10}
                    paddingBottom={1}
                    paddingX={6}>
                    <Grid
                        justify="space-between"
                        className="h-full"
                        spacing={2}
                        container>
                        <Grid xs={3} md={2} className="h-full" item>
                            <Box height="40%">
                                <Team
                                    team={teams[0]}
                                    userId={user.id}
                                    handleJoin={handleJoinTeam.bind(
                                        null,
                                        teams[0].id
                                    )}
                                    handleLeave={handleLeaveTeam}
                                />
                            </Box>
                        </Grid>
                        <Grid
                            xs={6}
                            md={8}
                            className={classes.contentWrapper}
                            item>
                            <>
                                {game.status === GameStatus.notStarted &&
                                    game.isCreator && (
                                        <Lobby
                                            slug={game.slug}
                                            languages={languages}
                                        />
                                    )}
                                {game.status !== GameStatus.notStarted && (
                                    <Board />
                                )}
                            </>
                        </Grid>
                        <Grid style={{ height: '100%' }} xs={3} md={2} item>
                            <Box
                                display="flex"
                                flexDirection="column"
                                height="100%">
                                <Box height="40%">
                                    <Team
                                        team={teams[1]}
                                        userId={user.id}
                                        handleJoin={handleJoinTeam.bind(
                                            null,
                                            teams[1].id
                                        )}
                                        handleLeave={handleLeaveTeam}
                                    />
                                </Box>
                                {game.status !== GameStatus.notStarted && (
                                    <Box
                                        height="60%"
                                        flexGrow={2}
                                        paddingTop={2}>
                                        <Gamelogs />
                                    </Box>
                                )}
                            </Box>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </>
    );
};

export default Room;

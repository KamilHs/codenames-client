import React from 'react';
import { Box } from '@material-ui/core';

import {
    CardTypeEnum,
    GameLogTypeEnum,
    ICard,
    IGameLog,
    IPlayer,
    ITeam
} from 'modules/Rooms/redux/const';
import { TEAM_NAME } from 'modules/Rooms/components/Board/components/const';

import { CARD_COLORS, GAMELOG_TEXTS, TEAM_COLORS } from './const';

type GamelogProps = {
    gamelog: IGameLog;
    teams: ITeam[];
    cards: ICard[][];
};

const Gamelog: React.FC<GamelogProps> = ({ gamelog, teams, cards }) => {
    const team = React.useMemo<ITeam | null>(
        () =>
            teams.find(team =>
                team.players.find(player => player.id === gamelog.playerId)
            ) ?? null,
        [gamelog, teams]
    );

    const player = React.useMemo<IPlayer | null>(
        () =>
            team
                ? team.players.find(player => player.id === gamelog.playerId) ??
                  null
                : null,
        [team, gamelog]
    );

    const card = React.useMemo<ICard | null>(
        () =>
            gamelog.type !== GameLogTypeEnum.endGuess &&
            gamelog.type !== GameLogTypeEnum.clue &&
            gamelog.type !== GameLogTypeEnum.win
                ? cards.reduce<ICard | null>(
                      (acc, row) =>
                          row.find(card => card.id === gamelog.cardId) ?? acc,
                      null
                  )
                : null,
        [gamelog, cards]
    );

    return (
        <Box
            style={{
                backgroundColor: TEAM_COLORS[gamelog.team].back
            }}
            display="flex"
            flexWrap="wrap"
            width="100%"
            padding={1}>
            {gamelog.type !== GameLogTypeEnum.win ? (
                <>
                    <span
                        style={{
                            fontFamily: 'Ticketing',
                            fontSize: '16px',
                            fontWeight: 'bold',
                            color: TEAM_COLORS[gamelog.team].text
                        }}>
                        {player ? player.username : 'Unknown'}
                    </span>
                    <span
                        style={{
                            fontFamily: 'Ticketing',
                            fontSize: '16px',
                            margin: '0 6px'
                        }}>
                        {GAMELOG_TEXTS[gamelog.type]}
                    </span>
                    {card && (
                        <span
                            style={{
                                fontFamily: 'Ticketing',
                                fontSize: '16px',
                                fontWeight: 'bold',
                                color:
                                    CardTypeEnum.grey === card.type
                                        ? CARD_COLORS[card.type!].text
                                        : CARD_COLORS[card.type!]
                            }}>
                            {card.word}
                        </span>
                    )}
                    {gamelog.type === GameLogTypeEnum.clue && (
                        <span
                            style={{
                                fontFamily: 'Ticketing',
                                fontSize: '16px',
                                fontWeight: 'bold',
                                padding: '1px 4px',
                                color: CARD_COLORS[CardTypeEnum.grey].text,
                                background: 'white'
                            }}>
                            {gamelog.content}
                        </span>
                    )}
                </>
            ) : (
                <span
                    style={{
                        width: '100%',
                        fontFamily: 'Ticketing',
                        fontSize: '16px',
                        fontWeight: 'bold',
                        display: 'block',
                        textAlign: 'center',
                        color: TEAM_COLORS[gamelog.team].text
                    }}>
                    {TEAM_NAME[gamelog.team]} wins
                </span>
            )}
        </Box>
    );
};

export default Gamelog;

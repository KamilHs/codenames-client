import React from 'react';
import { Box, Typography } from '@material-ui/core';

import { RootState } from 'store';
import { useSelector } from 'react-redux';

import Gamelog from './Gamelog';

const Gamelogs: React.FC = () => {
    const divRef = React.useRef<HTMLDivElement>(null);

    const { gamelogs, teams, cards } = useSelector(
        (state: RootState) => state.room
    );

    React.useEffect(() => {
        if (!divRef.current) return;
        divRef.current.scrollIntoView();
    }, [gamelogs.length]);

    return (
        <Box
            paddingY="4"
            style={{
                background: 'white'
            }}
            height="100%"
            display="flex"
            flexDirection="column">
            <Box padding={1}>
                <Typography
                    align="center"
                    style={{
                        fontFamily: '"Press Start 2P"'
                    }}
                    variant="body2">
                    Gamelogs
                </Typography>
            </Box>
            <Box overflow="auto" flexGrow={2}>
                {gamelogs.map(gamelog => (
                    <Gamelog
                        key={gamelog.id}
                        gamelog={gamelog}
                        teams={teams}
                        cards={cards}
                    />
                ))}
                <div ref={divRef} />
            </Box>
        </Box>
    );
};

export default Gamelogs;

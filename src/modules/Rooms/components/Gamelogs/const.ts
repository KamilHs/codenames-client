import {
    CardTypeEnum,
    GameLogTypeEnum,
    TeamTypeEnum
} from 'modules/Rooms/redux/const';

export const TEAM_COLORS: Record<
    TeamTypeEnum,
    { back: string; text: string }
> = {
    [TeamTypeEnum.blue]: {
        back: '#0000fa33',
        text: '#0000fa'
    },
    [TeamTypeEnum.red]: {
        back: '#fa000033',
        text: '#fa0000'
    }
};

export const CARD_COLORS = {
    [CardTypeEnum.black]: '#000000',
    [CardTypeEnum.red]: '#fa0000',
    [CardTypeEnum.blue]: '#0000fa',
    [CardTypeEnum.grey]: {
        text: '#aeaeae',
        back: '#aeaeae33'
    }
};

export const GAMELOG_TEXTS: Record<GameLogTypeEnum, string> = {
    [GameLogTypeEnum.assasinated]: 'taps',
    [GameLogTypeEnum.clue]: 'gives clue',
    [GameLogTypeEnum.guess]: 'taps',
    [GameLogTypeEnum.endGuess]: 'ends guessing',
    [GameLogTypeEnum.win]: ''
};

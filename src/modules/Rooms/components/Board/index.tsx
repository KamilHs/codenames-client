import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { RootState } from 'store';
import { useSelector } from 'react-redux';

import Card from './components/Card';
import Title from 'modules/Rooms/components/Board/components/Title';
import ClueField from 'modules/Rooms/components/Board/components/ClueField';
import { SocketContext } from 'context/SocketContext';
import { gameSocketActions } from 'sockets/game/actions';
import { InGameState, PlayerRoleEnum } from 'modules/Rooms/redux/const';
import Button from 'components/Buttons';

const Board: React.FC = () => {
    const { cards, game, player, clue, assumptions } = useSelector(
        (state: RootState) => state.room
    );
    const { socket } = React.useContext(SocketContext);

    const handleClue = React.useCallback(
        (clue: string) => {
            if (!socket || !game) return;
            if (
                game.currentTeamId !== player?.teamId ||
                player?.role !== PlayerRoleEnum.spymaster
            )
                return;
            gameSocketActions.giveClue(socket, game.slug, clue);
        },
        [socket, game, player]
    );

    const handleGuess = React.useCallback(
        (cardId: number) => {
            if (!socket || !game || !player) return;
            if (
                game.currentTeamId !== player.teamId ||
                player.role !== PlayerRoleEnum.operative
            )
                return;

            gameSocketActions.makeGuess(socket, game.slug, cardId);
        },
        [game, socket, player]
    );

    const handleEndGuess = React.useCallback(() => {
        if (!socket || !game || !player) return;
        if (
            game.currentTeamId !== player.teamId ||
            player.role !== PlayerRoleEnum.operative
        )
            return;
        gameSocketActions.endGuess(socket, game.slug);
    }, [game, socket, player]);

    if (!cards || cards.length === 0) return null;

    const rows = cards.length;
    const cols = cards[rows - 1].length;

    return (
        <Box
            display="flex"
            flexDirection="column"
            justifyContent="space-between"
            height="100%">
            <Title />
            <Box display="flex" flexWrap="wrap">
                {cards.map(row =>
                    row.map(card => (
                        <Box
                            key={card.id}
                            style={{
                                width: `${100 / cols}%`
                            }}>
                            <Card
                                assumptions={
                                    assumptions && assumptions[card.id]
                                        ? assumptions[card.id]
                                        : []
                                }
                                card={card}
                                handleGuess={() => handleGuess(card.id)}
                            />
                        </Box>
                    ))
                )}
            </Box>
            <Box>
                <Box
                    width="100%"
                    display="flex"
                    justifyContent="center"
                    marginBottom={2}>
                    {game?.state === InGameState.guessing && (
                        <Typography
                            style={{ fontFamily: '"Press Start 2P"' }}
                            variant="body1"
                            color="secondary">
                            {clue}
                        </Typography>
                    )}
                </Box>
                {game?.currentTeamId === player?.teamId && (
                    <>
                        {game?.state === InGameState.clue &&
                            player?.role === PlayerRoleEnum.spymaster && (
                                <ClueField handleClue={handleClue} />
                            )}
                        {game?.state === InGameState.guessing &&
                            player?.role === PlayerRoleEnum.operative && (
                                <Box display="flex" justifyContent="center">
                                    <Button
                                        fontFamily='"Press Start 2P"'
                                        variant="outlined"
                                        color="primary"
                                        onClick={handleEndGuess}>
                                        End Guessing
                                    </Button>
                                </Box>
                            )}
                    </>
                )}
            </Box>
        </Box>
    );
};

export default Board;

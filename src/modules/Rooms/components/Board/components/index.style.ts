import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    cardWord: {
        position: 'absolute',
        left: '50%',
        transform: 'translateX(-50%)',
        bottom: '15%',
        color: theme.palette.primary.main,
        fontFamily: 'Ticketing',
        [theme.breakpoints.down('md')]: {
            fontSize: '14px'
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '10px'
        }
    },
    blackCardWord: {
        color: theme.palette.secondary.main
    },
    cardHoverable: {
        cursor: 'pointer'
    },
    boardTitle: {
        textAlign: 'center',
        fontFamily: '"Press Start 2P"'
    },
    redBoardTitle: {
        color: '#AB2323'
    },
    blueBoardTitle: {
        color: '#1D62C8'
    },
    clueField: {
        fontSize: '32px',
        outline: 'none',
        border: `1px solid ${theme.palette.secondary.main}`,
        width: '30vw',
        minWidth: '300px',
        padding: `${theme.spacing(2)}px ${theme.spacing(5)}px`,
        boxShadow: `0 0 10px 0 ${theme.palette.secondary.main}`,
        borderRadius: '999px',
        background: theme.palette.primary.light,
        color: theme.palette.secondary.main,
        fontFamily: '"Press Start 2P"',
        '&::placeholder': {
            color: theme.palette.secondary.main
        }
    },
    clueOptionMenu: {
        '& .MuiMenu-list': {
            background: theme.palette.secondary.main,
            display: 'flex'
        }
    },
    clueOptionItem: {
        borderRight: `1px solid ${theme.palette.primary.main}`
    },
    inlinableBox: {
        backfaceVisibility: 'hidden',
        transition: 'all 0.4s ease',
        transformOrigin: 'top',
        zIndex: 4
    },
    inclinedBox: {
        transform: 'rotateX(55deg)'
    },
    inclineContainer: {
        perspective: '600px',
        perspectiveOrigin: '50% 50%',
        [theme.breakpoints.down('md')]: {
            perspective: '300px'
        }
    }
}));

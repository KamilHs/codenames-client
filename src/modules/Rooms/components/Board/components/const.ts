import {
    CardTypeEnum,
    InGameState,
    TeamTypeEnum
} from 'modules/Rooms/redux/const';

const CLOSED_CARD_BACKGROUNDS_PREFIX = '/cards/closed_cards';

export const CLOSED_CARD_BACKGROUNDS: Record<CardTypeEnum, string> = {
    [CardTypeEnum.black]: `${CLOSED_CARD_BACKGROUNDS_PREFIX}/black.png`,
    [CardTypeEnum.red]: `${CLOSED_CARD_BACKGROUNDS_PREFIX}/red.png`,
    [CardTypeEnum.blue]: `${CLOSED_CARD_BACKGROUNDS_PREFIX}/blue.png`,
    [CardTypeEnum.grey]: `${CLOSED_CARD_BACKGROUNDS_PREFIX}/grey.png`
};

const OPENED_CARD_BACKGROUNDS_PREFIX = '/cards/opened_cards';

export const OPENED_CARD_BACKGROUNDS: Record<CardTypeEnum, string> = {
    [CardTypeEnum.black]: `${OPENED_CARD_BACKGROUNDS_PREFIX}/black.png`,
    [CardTypeEnum.red]: `${OPENED_CARD_BACKGROUNDS_PREFIX}/red.png`,
    [CardTypeEnum.blue]: `${OPENED_CARD_BACKGROUNDS_PREFIX}/blue.png`,
    [CardTypeEnum.grey]: `${OPENED_CARD_BACKGROUNDS_PREFIX}/grey.png`
};

export const TEAM_NAME: Record<TeamTypeEnum, string> = {
    [TeamTypeEnum.red]: 'Team Red',
    [TeamTypeEnum.blue]: 'Team Blue'
};

export const TEAM_COLOR: Record<TeamTypeEnum, string> = {
    [TeamTypeEnum.red]: '#AB2323',
    [TeamTypeEnum.blue]: '#1D62C8'
};

export const GAME_STATE_TITLES: Record<InGameState, string> = {
    [InGameState.clue]: 'spymaster gives clue',
    [InGameState.guessing]: 'operatives guess words'
};

export const CLUE_OPTIONS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '∞'];

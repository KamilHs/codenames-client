/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { Box, Menu, MenuItem } from '@material-ui/core';

import Button from 'components/Buttons';

import { CLUE_OPTIONS } from './const';
import useStyles from './index.style';

type ClueFieldProps = {
    handleClue: (clue: string) => void;
};

const ClueField: React.FC<ClueFieldProps> = ({ handleClue }) => {
    const classes = useStyles();
    const [clue, setClue] = React.useState<string>('');
    const [selectedOption, setSelectedOption] = React.useState<string>(
        CLUE_OPTIONS[0]
    );
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    return (
        <Box display="flex" justifyContent="center" alignItems="center">
            <input
                value={clue}
                onChange={e => setClue(e.target.value)}
                className={classes.clueField}
                placeholder="Clue..."
            />
            <Menu
                className={classes.clueOptionMenu}
                getContentAnchorEl={null}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                transformOrigin={{ vertical: 'top', horizontal: 'center' }}
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
                keepMounted>
                {CLUE_OPTIONS.map(option => (
                    <MenuItem
                        key={option}
                        className={classes.clueOptionItem}
                        onClick={() => {
                            setSelectedOption(option);
                            setAnchorEl(null);
                        }}>
                        {option}
                    </MenuItem>
                ))}
            </Menu>
            <Button
                style={{ marginLeft: '16px', height: '100%' }}
                variant="outlined"
                color="primary"
                fontFamily='"Press Start 2P"'
                onClick={handleClick}>
                {selectedOption}
            </Button>
            <Button
                style={{ marginLeft: '16px' }}
                variant="outlined"
                color={clue.length === 0 ? 'secondary' : 'primary'}
                fontFamily='"Press Start 2P"'
                onClick={() => {
                    handleClue(`${clue} ${selectedOption}`);
                    setClue('');
                }}
                disabled={clue.length === 0}>
                Give clue
            </Button>
        </Box>
    );
};

export default ClueField;

import React from 'react';
import { Box } from '@material-ui/core';
import { useSelector } from 'react-redux';

import { RootState } from 'store';
import { TEAM_COLOR } from './const';

type AssumptionsProps = {
    assumptions: number[];
};

const Assumptions: React.FC<AssumptionsProps> = ({ assumptions }) => {
    const { teams } = useSelector((state: RootState) => state.room);
    const getAssumptionData = React.useCallback(
        (playerId: number) => {
            let name = '';
            const team = teams.find(team =>
                team.players.find(({ username, id }) =>
                    id === playerId ? (name = username) && true : false
                )
            );

            return { username: name, type: team?.type };
        },
        [teams]
    );

    return (
        <Box
            position="absolute"
            width="100%"
            maxHeight="100%"
            overflow="auto"
            top="0"
            left="0"
            zIndex="4"
            display="flex"
            flexWrap="wrap"
            padding={2}>
            {assumptions.map(assumption => {
                const { username, type } = getAssumptionData(assumption);

                if (!type) return null;

                return (
                    <Box
                        key={assumption}
                        style={{
                            fontFamily: 'Ticketing',
                            background: TEAM_COLOR[type]
                        }}
                        fontSize="10px"
                        color="white"
                        borderRadius={4}
                        padding={0.25}
                        marginRight={1}
                        marginBottom={1}>
                        {username}
                    </Box>
                );
            })}
        </Box>
    );
};

export default Assumptions;

import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { useSelector } from 'react-redux';

import {
    CardTypeEnum,
    ICard,
    InGameState,
    PlayerRoleEnum
} from 'modules/Rooms/redux/const';
import { RootState } from 'store';
import { SocketContext } from 'context/SocketContext';

import Assumptions from './Assumptions';
import { CLOSED_CARD_BACKGROUNDS, OPENED_CARD_BACKGROUNDS } from './const';
import useStyles from './index.style';
import { gameSocketActions } from 'sockets/game/actions';

type CardProps = {
    assumptions: number[];
    card: ICard;
    handleGuess: () => void;
};

const Card: React.FC<CardProps> = ({
    assumptions,
    card: { id, isOpen, type, word, frontImage },
    handleGuess
}) => {
    const classes = useStyles();
    const { socket } = React.useContext(SocketContext);
    const { game, player } = useSelector((state: RootState) => state.room);
    const [inclined, setInclined] = React.useState<boolean>(false);

    const canIncline = React.useMemo(() => isOpen, [isOpen]);
    const canOpen = React.useMemo(
        () =>
            player &&
            game?.currentTeamId === player.teamId &&
            game.state === InGameState.guessing &&
            player.role === PlayerRoleEnum.operative,
        [player, game]
    );
    const openedBackground = React.useMemo<string>(
        () => OPENED_CARD_BACKGROUNDS[type ?? CardTypeEnum.grey],
        [type]
    );
    const closedBackground = React.useMemo<string>(
        () => CLOSED_CARD_BACKGROUNDS[type ?? CardTypeEnum.grey],
        [type]
    );
    const isAssumed = React.useMemo<boolean>(
        () => !!player && assumptions.includes(player.id),
        [player, assumptions]
    );

    const handleClick = React.useCallback(() => {
        if (canIncline) {
            setInclined(!inclined);
        } else if (canOpen) handleGuess();
    }, [canIncline, canOpen, inclined, handleGuess]);

    const handleAssume = React.useCallback(
        (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
            e.preventDefault();
            if (!canOpen || !socket || !game) return;
            isAssumed
                ? gameSocketActions.removeAssumption(socket, game.slug, id)
                : gameSocketActions.makeAssumption(socket, game.slug, id);
        },
        [isAssumed, canOpen, socket, id, game]
    );

    return (
        <Box
            style={{ userSelect: 'none' }}
            mx={0.5}
            marginBottom={1}
            position="relative"
            onContextMenu={handleAssume}
            onClick={handleClick}
            className={clsx(
                canOpen || canIncline ? classes.cardHoverable : ''
            )}>
            {assumptions.length > 0 && (
                <Assumptions assumptions={assumptions} />
            )}
            {
                <Box position="relative" className={classes.inclineContainer}>
                    <Box overflow="hidden" borderRadius="9%">
                        <img
                            style={{ display: 'block' }}
                            src={closedBackground}
                            alt="background"
                            draggable="false"
                        />
                    </Box>
                    <Typography
                        className={clsx(
                            classes.cardWord,
                            type === CardTypeEnum.black
                                ? classes.blackCardWord
                                : ''
                        )}>
                        {word}
                    </Typography>
                    {isOpen && (
                        <Box
                            className={clsx(
                                classes.inlinableBox,
                                inclined ? classes.inclinedBox : ''
                            )}
                            position="absolute"
                            borderRadius="9%"
                            overflow="hidden"
                            top="0"
                            left="0">
                            {type !== CardTypeEnum.black && (
                                <img
                                    style={{
                                        display: 'block'
                                    }}
                                    src={openedBackground}
                                    alt="background"
                                    draggable="false"
                                />
                            )}
                            <Box
                                position={
                                    type !== CardTypeEnum.black
                                        ? 'absolute'
                                        : 'relative'
                                }
                                left="0"
                                top="0"
                                display="flex"
                                justifyContent="center"
                                width="100%"
                                height="100%">
                                <img
                                    style={{ display: 'block' }}
                                    src={frontImage}
                                    alt={type}
                                    draggable="false"
                                />
                            </Box>
                        </Box>
                    )}
                </Box>
            }
        </Box>
    );
};

export default Card;

import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { RootState } from 'store';
import { useSelector } from 'react-redux';

import useStyles from './index.style';
import { ITeam, TeamTypeEnum } from 'modules/Rooms/redux/const';
import {
    GAME_STATE_TITLES,
    TEAM_NAME
} from 'modules/Rooms/components/Board/components/const';

const Title: React.FC = () => {
    const classes = useStyles();
    const { game, teams } = useSelector((state: RootState) => state.room);

    const currentTeam = React.useMemo<ITeam | null>(
        () => teams.find(team => team.id === game?.currentTeamId) || null,
        [game, teams]
    );

    if (!currentTeam || !game) return null;

    const { type } = currentTeam;

    return (
        <Box
            display="flex"
            justifyContent="center"
            position="fixed"
            maxWidth="50%"
            left="50%"
            top="0"
            marginTop={2}
            style={{
                transform: 'translateX(-50%)'
            }}>
            <Typography
                className={clsx(
                    classes.boardTitle,
                    type === TeamTypeEnum.red
                        ? classes.redBoardTitle
                        : classes.blueBoardTitle
                )}>
                {`${TEAM_NAME[type]} ${GAME_STATE_TITLES[game.state]}`}
            </Typography>
        </Box>
    );
};

export default Title;

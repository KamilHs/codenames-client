import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';

import Button from 'components/Buttons';
import {
    TEAM_COLOR,
    TEAM_NAME
} from 'modules/Rooms/components/Board/components/const';

import useStyles from './index.style';
import { TeamTypeEnum } from 'modules/Rooms/redux/const';

type WinnerProps = {
    winner: TeamTypeEnum;
    handleLeave: () => void;
};

const Winner: React.FC<WinnerProps> = ({ winner, handleLeave }) => {
    const classes = useStyles();

    const [hidden, setHidden] = React.useState<boolean>(false);

    const containerRef = React.useRef<HTMLDivElement>(null);

    return (
        <div
            ref={containerRef}
            className={clsx(
                classes.winnerContainer,
                hidden ? classes.hiddenWinnerContainer : ''
            )}
            onClick={() => setHidden(false)}>
            <Box>
                <Typography
                    color="secondary"
                    align="center"
                    style={{ fontFamily: '"Press Start 2P"' }}
                    variant="h2">
                    Congratulations!!!
                </Typography>
                <Typography
                    style={{
                        color: TEAM_COLOR[winner]
                    }}
                    className={classes.winnerTitle}
                    align="center"
                    variant="h4">
                    {TEAM_NAME[winner]} is victorious
                </Typography>
                <Box marginTop={4} display="flex" justifyContent="center">
                    <Button
                        color="primary"
                        variant="contained"
                        fontFamily='"Press Start 2P"'
                        className={classes.leaveRoomButton}
                        onClick={handleLeave}>
                        Leave Game
                    </Button>
                    <Button
                        color="primary"
                        variant="outlined"
                        fontFamily='"Press Start 2P"'
                        onClick={e => {
                            e.stopPropagation();
                            setHidden(true);
                        }}>
                        See Board
                    </Button>
                </Box>
            </Box>
        </div>
    );
};

export default Winner;

import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    winnerContainer: {
        display: 'flex',
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 100,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100vw',
        height: '100vh',
        background: `${theme.palette.primary.main}aa`,
        transition: 'all 0.3s ease'
    },
    hiddenWinnerContainer: {
        opacity: 0
    },
    leaveRoomButton: {
        marginRight: theme.spacing(1)
    },
    winnerTitle: {
        fontFamily: '"Press Start 2P"',
        textShadow: `0 0 10px ${theme.palette.secondary.light}`
    }
}));

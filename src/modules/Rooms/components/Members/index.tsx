import React from 'react';
import { Box, Modal, Typography } from '@material-ui/core';
import { RootState } from 'store';
import { useSelector } from 'react-redux';

import Button from 'components/Buttons';
import { KickType } from 'sockets/room/const';
import { roomSocketActions } from 'sockets/room/actions';
import { SocketContext } from 'context/SocketContext';

import Member from './Member';
import useStyles from './index.styles';

type MembersPropsType = {
    handleLeave: () => void;
};

const Members: React.FC<MembersPropsType> = ({ handleLeave }) => {
    const classes = useStyles();

    const { game, spectators, teams } = useSelector(
        (state: RootState) => state.room
    );

    const [open, setOpen] = React.useState<boolean>(false);
    const showPlayers = React.useMemo<boolean>(
        () => teams.some(team => team.players.length > 0),
        [teams]
    );

    const { socket } = React.useContext(SocketContext);

    const handleKick = React.useCallback(
        (id: number, type: KickType) => {
            if (!socket || !game || !game.isCreator) return;
            roomSocketActions.kickMember(socket, type, id, game.slug);
        },
        [socket, game]
    );

    return game ? (
        <Box>
            <Button
                variant="outlined"
                color="primary"
                className={classes.trigger}
                onClick={() => setOpen(!open)}>
                Members
            </Button>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
                BackdropProps={{ invisible: true }}>
                <Box className={classes.modalContent}>
                    {spectators.length > 0 && (
                        <Box>
                            <Typography
                                variant="body1"
                                className={classes.modalSectionTitle}>
                                Spectators
                            </Typography>
                            {spectators.map(spectator => (
                                <Member
                                    key={spectator.id}
                                    {...spectator}
                                    isCreator={game.isCreator}
                                    handleKick={() =>
                                        handleKick(
                                            spectator.id,
                                            KickType.spectator
                                        )
                                    }
                                    handleLeave={handleLeave}
                                />
                            ))}
                        </Box>
                    )}
                    {showPlayers && (
                        <Box>
                            <Typography
                                variant="body1"
                                className={classes.modalSectionTitle}>
                                Players
                            </Typography>
                            {teams.map(team =>
                                team.players.map(player => (
                                    <Member
                                        key={player.id}
                                        {...player}
                                        isCreator={game.isCreator}
                                        handleKick={() =>
                                            handleKick(
                                                player.id,
                                                KickType.player
                                            )
                                        }
                                        handleLeave={handleLeave}
                                    />
                                ))
                            )}
                        </Box>
                    )}
                </Box>
            </Modal>
        </Box>
    ) : null;
};

export default Members;

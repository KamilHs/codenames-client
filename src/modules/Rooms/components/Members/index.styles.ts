/* eslint-disable @typescript-eslint/no-magic-numbers */
import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    trigger: {
        position: 'fixed',
        left: '16%',
        transform: 'translateX(-50%)',
        fontFamily: '"Press Start 2P"',
        marginTop: theme.spacing(2)
    },
    modalContent: {
        background: theme.palette.secondary.main,
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: theme.shape.borderRadius,
        padding: theme.spacing(2),
        position: 'fixed',
        maxWidth: '300px',
        maxHeight: `calc(100vh - ${theme.spacing(14)}px)`,
        overflowY: 'auto',
        height: 'fit-content',
        top: `${theme.spacing(9)}px !important`,
        left: '16% !important',
        transform: 'translateX(-50%)'
    },
    modalSectionTitle: {
        textAlign: 'center',
        fontFamily: '"Press Start 2P"'
    },
    member: {
        display: 'flex',
        alignItems: 'center'
    },
    memberCenter: {
        justifyContent: 'center'
    },
    memberBetween: {
        justifyContent: 'space-between'
    },
    memberUsername: {
        fontFamily: 'Ticketing'
    },
    kickButton: {
        cursor: 'pointer',
        fontFamily: '"Press Start 2P"'
    }
}));

import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { useSelector } from 'react-redux';

import { RootState } from 'store';

import useStyles from './index.styles';

type MemberProps = {
    username: string;
    userId: number;
    isCreator: boolean;
    handleKick: () => void;
    handleLeave: () => void;
};

const Member: React.FC<MemberProps> = ({
    username,
    userId,
    isCreator,
    handleKick,
    handleLeave
}) => {
    const classes = useStyles();
    const user = useSelector((state: RootState) => state.auth.user);

    const showKick = user?.id !== userId && isCreator;

    const isUser = user?.id === userId;

    return (
        <Box
            className={clsx(
                classes.member,
                showKick ? classes.memberBetween : classes.memberCenter
            )}>
            <Typography className={classes.memberUsername} variant="h6">
                {username}
            </Typography>
            {showKick && (
                <Typography
                    className={classes.kickButton}
                    variant="h6"
                    onClick={handleKick}>
                    X
                </Typography>
            )}
            {isUser && (
                <Typography
                    className={classes.kickButton}
                    variant="body1"
                    onClick={handleLeave}>
                    -&gt;
                </Typography>
            )}
        </Box>
    );
};

export default Member;

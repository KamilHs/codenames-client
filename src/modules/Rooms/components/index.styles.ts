import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    wrapper: {
        height: '100vh',
        maxHeight: '100vh'
    },
    contentWrapper: {
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    buttonWrapper: {
        position: 'fixed',
        right: '16%',
        transform: 'translateX(50%)',
        marginTop: theme.spacing(2)
    }
}));

import { FetchStatus } from 'store/const';
import {
    IRoomState,
    RoomActionsType,
    SET_ROOM_STATUS,
    SET_GAME,
    SET_JOINED,
    SET_ROOM_DATA,
    SET_DECKS,
    SET_CARDS,
    SET_GAME_DATA,
    SET_GAMELOGS,
    SET_PLAYER_DATA,
    SET_LEFT_CARDS,
    SET_TAB_DISABLED,
    SET_CLUE,
    SET_ASSUMPTIONS,
    SET_WINNER
} from './const';

const initialState: IRoomState = {
    languages: [],
    joined: false,
    game: null,
    status: FetchStatus.none,
    spectators: [],
    teams: [],
    cards: [],
    gamelogs: [],
    player: null,
    leftCards: null,
    tabDisabled: false,
    clue: '',
    assumptions: null,
    winner: null
};

const reducer = (
    state: IRoomState = initialState,
    action: RoomActionsType
): IRoomState => {
    switch (action.type) {
        case SET_ROOM_STATUS:
            return { ...state, status: action.payload };
        case SET_GAME:
            return { ...state, game: action.payload };
        case SET_JOINED:
            return { ...state, joined: action.payload };
        case SET_ROOM_DATA:
            return { ...state, ...action.payload };
        case SET_DECKS:
            return { ...state, languages: action.payload };
        case SET_CARDS:
            return { ...state, cards: action.payload };
        case SET_GAME_DATA:
            return {
                ...state,
                game: { ...state.game!, ...action.payload }
            };
        case SET_GAMELOGS:
            return {
                ...state,
                gamelogs: action.payload
            };
        case SET_PLAYER_DATA:
            return { ...state, player: action.payload };
        case SET_LEFT_CARDS:
            return { ...state, leftCards: action.payload };
        case SET_TAB_DISABLED:
            return { ...state, tabDisabled: action.payload };
        case SET_CLUE:
            return { ...state, clue: action.payload };
        case SET_ASSUMPTIONS:
            return { ...state, assumptions: action.payload };
        case SET_WINNER:
            return { ...state, winner: action.payload };
        default:
            return state;
    }
};

export default reducer;

import { Action } from 'redux';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { push } from 'connected-react-router';
import { ThunkAction } from 'redux-thunk';

import { FetchStatus } from 'store/const';
import { roomApi } from 'utils/api';

import {
    GameStatus,
    IGame,
    IRoomData,
    Language,
    RoomActionsType,
    SET_DECKS,
    SET_GAME,
    SET_ROOM_DATA,
    SET_ROOM_STATUS,
    SET_JOINED,
    ICard,
    SET_CARDS,
    SET_GAME_DATA,
    InGameState,
    IGameLog,
    SET_GAMELOGS,
    PlayerRoleEnum,
    SET_PLAYER_DATA,
    SET_LEFT_CARDS,
    TeamTypeEnum,
    SET_TAB_DISABLED,
    SET_CLUE,
    Assumptions,
    SET_ASSUMPTIONS,
    SET_WINNER
} from './const';
import { RootState } from 'store';
import { handleError } from 'utils/redux';
import { popupActions } from 'store/global/popup.actions';

const roomActions = {
    setWinner: (winner: TeamTypeEnum | null) => ({
        type: SET_WINNER,
        payload: winner
    }),
    setAssumptions: (assumptions: Assumptions | null) => ({
        type: SET_ASSUMPTIONS,
        payload: assumptions
    }),
    setClue: (clue: string): RoomActionsType => ({
        type: SET_CLUE,
        payload: clue
    }),
    setTabDisabled: (disabled: boolean): RoomActionsType => ({
        type: SET_TAB_DISABLED,
        payload: disabled
    }),
    setLeftCards: (
        leftCards: Record<TeamTypeEnum, number> | null
    ): RoomActionsType => ({
        type: SET_LEFT_CARDS,
        payload: leftCards
    }),
    setPlayerData: (
        data: { role: PlayerRoleEnum; teamId: number; id: number } | null
    ): RoomActionsType => ({
        type: SET_PLAYER_DATA,
        payload: data
    }),
    setGamelogs: (gamelogs: IGameLog[]): RoomActionsType => ({
        type: SET_GAMELOGS,
        payload: gamelogs
    }),
    setCards: (cards: ICard[][]): RoomActionsType => ({
        type: SET_CARDS,
        payload: cards
    }),
    setRoomData: (data: IRoomData): RoomActionsType => ({
        type: SET_ROOM_DATA,
        payload: data
    }),
    setRoomStatus: (status: FetchStatus): RoomActionsType => ({
        type: SET_ROOM_STATUS,
        payload: status
    }),
    setGame: (game: IGame | null): RoomActionsType => ({
        type: SET_GAME,
        payload: game
    }),
    setGameData: (game: {
        turn: number;
        status: GameStatus;
        state: InGameState;
        currentTeamId: number;
    }): RoomActionsType => ({
        type: SET_GAME_DATA,
        payload: game
    }),
    setLanguageData: (languages: Language[]): RoomActionsType => ({
        type: SET_DECKS,
        payload: languages
    }),
    setJoined: (joined: boolean): RoomActionsType => ({
        type: SET_JOINED,
        payload: joined
    }),
    clearRoomData: (): ThunkAction<
        void,
        RootState,
        unknown,
        Action<any>
    > => dispatch => {
        dispatch(roomActions.setAssumptions([]));
        dispatch(roomActions.setClue(''));
        dispatch(roomActions.setWinner(null));
        dispatch(roomActions.setCards([]));
        dispatch(roomActions.setGame(null));
        dispatch(roomActions.setGamelogs([]));
        dispatch(roomActions.setLanguageData([]));
        dispatch(roomActions.setJoined(false));
        dispatch(roomActions.setPlayerData(null));
        dispatch(roomActions.setLeftCards(null));
        dispatch(roomActions.setRoomStatus(FetchStatus.none));
        dispatch(roomActions.setRoomData({ teams: [], spectators: [] }));
    },
    getRoom: (
        token: string,
        slug: string
    ): ThunkAction<void, RootState, unknown, Action<any>> => async dispatch => {
        try {
            dispatch(roomActions.setRoomStatus(FetchStatus.loading));

            const {
                data: { data: result }
            } = await roomApi.getRoom(token, slug);

            if (result && result.game) {
                dispatch(roomActions.setRoomStatus(FetchStatus.success));
                dispatch(roomActions.setGame(result.game));
                dispatch(
                    popupActions.addSuccessPopup('Joined room successfully')
                );
            } else {
                dispatch(roomActions.setRoomStatus(FetchStatus.failure));
                dispatch(push(DASHBOARD_ROUTES.main));
            }
        } catch (error) {
            dispatch(roomActions.setRoomStatus(FetchStatus.failure));
            dispatch(push(DASHBOARD_ROUTES.main));
            handleError(error, dispatch);
        }
    }
};

export default roomActions;

import { FetchStatus } from 'store/const';

export const GET_ROOM = 'GET_ROOM';
export const SET_ROOM_STATUS = 'SET_ROOM_STATUS';
export const SET_ROOM_DATA = 'SET_ROOM_DATA';
export const SET_GAME = 'SET_GAME';
export const SET_GAME_DATA = 'SET_GAME_DATA';
export const SET_JOINED = 'SET_JOINED';
export const SET_DECKS = 'SET_DECKS';
export const SET_GAMELOGS = 'SET_GAMELOGS';
export const SET_CARDS = 'SET_CARDS';
export const SET_PLAYER_DATA = 'SET_PLAYER_DATA';
export const SET_LEFT_CARDS = 'SET_LEFT_CARDS';
export const SET_TAB_DISABLED = 'SET_TAB_DISABLED';
export const SET_CLUE = 'SET_CLUE';
export const SET_ASSUMPTIONS = 'SET_ASSUMPTIONS';
export const SET_WINNER = 'SET_WINNER';

export enum PlayerRoleEnum {
    spymaster = 'spymaster',
    operative = 'operative'
}

export enum GameStatus {
    finished = 'finished',
    goingOn = 'goingOn',
    notStarted = 'notStarted'
}

export enum InGameState {
    guessing = 'guessing',
    clue = 'clue'
}

export enum TeamTypeEnum {
    red = 'red',
    blue = 'blue'
}

export interface IPlayer {
    id: number;
    userId: number;
    username: string;
    role: PlayerRoleEnum;
}

export enum GameLogTypeEnum {
    assasinated = 'assasinated',
    endGuess = 'endGuess',
    guess = 'guess',
    clue = 'clue',
    win = 'win'
}

export enum CardTypeEnum {
    red = 'red',
    blue = 'blue',
    black = 'black',
    grey = 'grey'
}

export interface IGameLog {
    id: number;
    team: TeamTypeEnum;
    type: GameLogTypeEnum;
    playerId: number;
    cardId?: number;
    content?: string;
}

export type Assumptions = Record<number, number[]>;

export interface ISpectator {
    id: number;
    username: string;
    userId: number;
}

export interface ICard {
    id: number;
    word: string;
    isOpen: boolean;
    frontImage?: string;
    type?: CardTypeEnum;
}

export interface ITeam {
    id: number;
    type: TeamTypeEnum;
    players: IPlayer[];
}

export interface IGame {
    slug: string;
    isCreator: boolean;
    state: InGameState;
    status: GameStatus;
    turn: number;
    currentTeamId: number;
}

export interface IRoomData {
    spectators: ISpectator[];
    teams: ITeam[];
}

export type Deck = {
    id: number;
    name: string;
    length: number;
};

export type Language = {
    id: number;
    logo: string;
    decks: Deck[];
};

interface ISetRoomStatus {
    type: typeof SET_ROOM_STATUS;
    payload: FetchStatus;
}

interface ISetGame {
    type: typeof SET_GAME;
    payload: IGame | null;
}

interface ISetJoined {
    type: typeof SET_JOINED;
    payload: boolean;
}

interface ISetRoomData {
    type: typeof SET_ROOM_DATA;
    payload: IRoomData;
}

interface ISetLanguage {
    type: typeof SET_DECKS;
    payload: Language[];
}

interface ISetCards {
    type: typeof SET_CARDS;
    payload: ICard[][];
}

interface ISetGameData {
    type: typeof SET_GAME_DATA;
    payload: {
        turn: number;
        status: GameStatus;
        state: InGameState;
        currentTeamId: number;
    };
}

interface ISetGamelogs {
    type: typeof SET_GAMELOGS;
    payload: IGameLog[];
}

interface ISetPlayerData {
    type: typeof SET_PLAYER_DATA;
    payload: {
        id: number;
        role: PlayerRoleEnum;
        teamId: number;
    } | null;
}

interface ISetLeftCards {
    type: typeof SET_LEFT_CARDS;
    payload: Record<TeamTypeEnum, number> | null;
}

interface ISetTabDisabled {
    type: typeof SET_TAB_DISABLED;
    payload: boolean;
}

interface ISetClue {
    type: typeof SET_CLUE;
    payload: string;
}

interface ISetAssumptions {
    type: typeof SET_ASSUMPTIONS;
    payload: Assumptions | null;
}

interface ISetWinner {
    type: typeof SET_WINNER;
    payload: TeamTypeEnum | null;
}

export type RoomActionsType =
    | ISetRoomStatus
    | ISetGame
    | ISetJoined
    | ISetRoomData
    | ISetLanguage
    | ISetCards
    | ISetGameData
    | ISetGamelogs
    | ISetPlayerData
    | ISetLeftCards
    | ISetTabDisabled
    | ISetClue
    | ISetAssumptions
    | ISetWinner;

export interface IRoomState {
    clue: string;
    tabDisabled: boolean;
    languages: Language[];
    joined: boolean;
    status: FetchStatus;
    game: IGame | null;
    teams: ITeam[];
    leftCards: Record<TeamTypeEnum, number> | null;
    spectators: ISpectator[];
    cards: ICard[][];
    gamelogs: IGameLog[];
    player: {
        id: number;
        role: PlayerRoleEnum;
        teamId: number;
    } | null;
    assumptions: Assumptions | null;
    winner: TeamTypeEnum | null;
}

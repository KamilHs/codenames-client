import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    section: {
        marginBottom: `${theme.spacing(20)}px`,
        padding: theme.spacing(5),
        border: `1px solid ${theme.palette.secondary.main}`,
        borderRadius: `${theme.shape.borderRadius * 3}px`
    },
    primaryText: {
        fontFamily: 'Ticketing',
        color: theme.palette.secondary.light
    },
    secondaryText: {
        fontFamily: 'Ticketing',
        color: theme.palette.secondary.dark,
        fontSize: '24px'
    },
    sectionTitle: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(3),
        lineHeight: '48px',
        textTransform: 'uppercase'
    },
    teamContainer: {
        justifyContent: 'center'
    },
    jobTitle: {
        fontFamily: '"Press Start 2P"',
        textTransform: 'uppercase',
        marginBottom: theme.spacing(3)
    },
    frontend: {
        color: '#ab2323'
    },
    backend: {
        color: '#1d62c8'
    },
    teamLead: {
        position: 'absolute',
        bottom: '100%',
        color: theme.palette.primary.dark,
        fontFamily: '"Press Start 2P"',
        textTransform: 'uppercase',
        textShadow: `0px 0px 12px ${theme.palette.secondary.main}`
    },
    teamMember: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginBottom: theme.spacing(3)
    },
    listItem: {
        position: 'relative',
        '&::before': {
            content: '""',
            position: 'absolute',
            top: '50%',
            right: '100%',
            marginRight: theme.spacing(1),
            width: '5px',
            height: '5px',
            borderRadius: '50%',
            backgroundColor: theme.palette.secondary.light,
            transform: 'translateY(-50%)'
        }
    }
}));

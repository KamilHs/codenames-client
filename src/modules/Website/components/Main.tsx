/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { Box } from '@material-ui/core';

import Enterence from './components/Enterence';
import { Foooter, Header } from 'components/Layout';
import OurTeam from './components/OurTeam';
import Rules from './components/Rules';

const Main: React.FC = () => (
    <Box className="background_main">
        <Header />
        <Box className="container">
            <Box paddingTop={15}>
                <Enterence />
                <Rules />
                <OurTeam />
            </Box>
        </Box>
        <Foooter />
    </Box>
);

export default Main;

import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';

import { teamMembers } from '../const';
import useModuleStyles from '../index.style';

import TeamMember from './TeamMember';

const OurTeam: React.FC = () => {
    const classes = useModuleStyles();

    return (
        <Box className={classes.section}>
            <Typography
                color="secondary"
                align="center"
                variant="h4"
                className={classes.sectionTitle}
                paragraph>
                Our team
            </Typography>
            <Grid spacing={6} justify="center" container>
                {teamMembers.map(member => (
                    <Grid xs={12} sm={6} md={4} key={member.name} item>
                        <TeamMember member={member} />
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default OurTeam;

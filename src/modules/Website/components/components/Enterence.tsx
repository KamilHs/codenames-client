import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { AUTH_PREFIX } from 'modules/Auth/routes/const';
import Button from 'components/Buttons';
import useModuleStyles from '../index.style';

const Enterence: React.FC = () => {
    const classes = useModuleStyles();

    return (
        <Box className={classes.section}>
            <Box display="flex" justifyContent="space-between">
                <Box>
                    <Typography
                        className={classes.primaryText}
                        align="center"
                        variant="h3">
                        Codenames is a game of guessing
                    </Typography>
                    <Box marginTop={3}>
                        <Typography
                            className={classes.secondaryText}
                            variant="body1">
                            which codenames in a set are related to a hint-word
                            given by another player.
                        </Typography>
                    </Box>
                    <Box marginTop={3}>
                        <Link to={AUTH_PREFIX}>
                            <Button
                                fontFamily={'"Press Start 2P"'}
                                variant="contained"
                                color="primary">
                                Play the game
                            </Button>
                        </Link>
                    </Box>
                </Box>
                <Box>
                    <img
                        src="/website/Codenames.png"
                        style={{ userSelect: 'none' }}
                        alt="codenames"
                        draggable={false}
                    />
                </Box>
            </Box>
        </Box>
    );
};

export default Enterence;

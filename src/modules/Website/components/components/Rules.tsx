import React from 'react';
import clsx from 'clsx';
import { Grid, Box, Typography, List, ListItem } from '@material-ui/core';

import useModuleStyles from '../index.style';

const RULES = [
    'Players split into two teams: red and blue.',
    "One player of each team is selected as the team's spymaster and the others are operatives.",
    '25 Cards which represents red agents, blue agents and one represents an assassin, and the others represent innocent bystanders.',
    'Teams take turns. On each turn, the appropriate spymaster gives a verbal hint about the words on the respective cards',
    'Each hint may only consist of one single word and a number'
];

const Rules: React.FC = () => {
    const classes = useModuleStyles();

    return (
        <Box className={classes.section}>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center">
                <Typography
                    color="secondary"
                    align="center"
                    variant="h4"
                    className={classes.sectionTitle}
                    paragraph>
                    how to play the game
                </Typography>
                <List>
                    {RULES.map(rule => (
                        <ListItem key={rule}>
                            <Typography
                                variant="body1"
                                className={clsx(
                                    classes.primaryText,
                                    classes.listItem
                                )}>
                                {rule}
                            </Typography>
                        </ListItem>
                    ))}
                </List>
            </Grid>
        </Box>
    );
};

export default Rules;

import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';

import { ITeamMemberProps, TeamMemberJob } from '../const';
import useModuleStyles from '../index.style';

const TeamMember: React.FC<ITeamMemberProps> = ({
    member: { teamlead, picture, name, job }
}) => {
    const classes = useModuleStyles();

    return (
        <Box className={classes.teamMember}>
            {teamlead && (
                <Typography variant="h5" className={classes.teamLead}>
                    Teamlead
                </Typography>
            )}
            <img src={picture} />
            <Typography
                gutterBottom
                variant="h5"
                component="h2"
                className={classes.primaryText}>
                {name}
            </Typography>
            <Typography
                variant="body2"
                color="textSecondary"
                component="p"
                className={clsx(
                    classes.jobTitle,
                    job === TeamMemberJob.backend
                        ? classes.backend
                        : classes.frontend
                )}>
                {job}
            </Typography>
        </Box>
    );
};

export default TeamMember;

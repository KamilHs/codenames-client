export enum TeamMemberJob {
    backend = 'backend',
    frontend = 'frontend'
}

export type TeamMemberType = {
    name: string;
    job: string;
    teamlead: boolean;
    picture: string;
};

export interface ITeamMemberProps {
    member: TeamMemberType;
}

export const teamMembers: TeamMemberType[] = [
    {
        name: 'Rustam Aliyev',
        job: TeamMemberJob.backend,
        picture: `${process.env.PUBLIC_URL}/website/blue_rustem.png`,
        teamlead: false
    },
    {
        name: 'Emil Sadigov',
        job: TeamMemberJob.backend,
        picture: `${process.env.PUBLIC_URL}/website/blue_emil.png`,
        teamlead: false
    },
    {
        name: 'Elcin Qurbanli',
        job: TeamMemberJob.backend,
        picture: `${process.env.PUBLIC_URL}/website/blue_elcin.png`,
        teamlead: false
    },
    {
        name: 'Anar Shikhaliyev',
        job: TeamMemberJob.frontend,
        picture: `${process.env.PUBLIC_URL}/website/red_anar.png`,
        teamlead: false
    },
    {
        name: 'Kamil Salimli',
        job: TeamMemberJob.frontend,
        picture: `${process.env.PUBLIC_URL}/website/red_kamil.png`,
        teamlead: true
    }
];

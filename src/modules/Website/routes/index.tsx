import React from 'react';
import { Route, Switch } from 'react-router';

import Main from '../components/Main';

import { WEBSITE_ROUTES } from './const';

export const WebsiteRoutes: React.FC = () => (
    <Switch>
        <Route path={WEBSITE_ROUTES.main} component={Main} exact />
    </Switch>
);

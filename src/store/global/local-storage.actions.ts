import {
    LocalStorageActionsType,
    SET_LOCAL_STORAGE_ITEM,
    UNSET_LOCAL_STORAGE_ITEM
} from './local-storage.const';

export const localStorageActions = {
    setLocalStorageItem: (
        name: string,
        item: unknown
    ): LocalStorageActionsType => ({
        type: SET_LOCAL_STORAGE_ITEM,
        payload: {
            name,
            item: JSON.stringify(item)
        }
    }),
    unsetLocalStorageItem: (name: string): LocalStorageActionsType => ({
        type: UNSET_LOCAL_STORAGE_ITEM,
        payload: name
    })
};

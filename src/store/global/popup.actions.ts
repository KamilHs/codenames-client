import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { generateRandomString } from 'utils/random';

import {
    ADD_POPUP,
    IErrorPopup,
    IFriendRequestPopup,
    IInfoPopup,
    ISuccessPopup,
    Popup,
    PopupActionTypes,
    PopupType,
    REMOVE_POPUP
} from './popup.const';

export const popupActions = {
    addPopup: (popup: Popup): PopupActionTypes => ({
        type: ADD_POPUP,
        payload: popup
    }),
    removePopup: (id: string): PopupActionTypes => ({
        type: REMOVE_POPUP,
        payload: id
    }),
    addSuccessPopup: (
        message: string
    ): ThunkAction<void, unknown, unknown, Action<any>> => dispatch => {
        const popup: ISuccessPopup = {
            id: generateRandomString(5),
            type: PopupType.success,
            message
        };

        dispatch(popupActions.addPopup(popup));
    },
    addErrorPopup: (
        message: string
    ): ThunkAction<void, unknown, unknown, Action<any>> => dispatch => {
        const popup: IErrorPopup = {
            id: generateRandomString(5),
            type: PopupType.error,
            message
        };

        dispatch(popupActions.addPopup(popup));
    },
    addInfoPopup: (
        message: string
    ): ThunkAction<void, unknown, unknown, Action<any>> => dispatch => {
        const popup: IInfoPopup = {
            id: generateRandomString(5),
            type: PopupType.info,
            message
        };

        dispatch(popupActions.addPopup(popup));
    },
    addFriendRequestPopup: (
        friendRequestId: number,
        userId: number,
        username: string
    ): ThunkAction<void, unknown, unknown, Action<any>> => dispatch => {
        const popup: IFriendRequestPopup = {
            id: generateRandomString(5),
            type: PopupType.friendRequest,
            friendRequestId,
            username,
            userId,
            message: 'You have a new friend request from the user:'
        };

        dispatch(popupActions.addPopup(popup));
    }
};

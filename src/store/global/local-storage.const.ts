export const SET_LOCAL_STORAGE_ITEM = 'SET_LOCAL_STORAGE_ITEM';
export const UNSET_LOCAL_STORAGE_ITEM = 'UNSET_LOCAL_STORAGE_ITEM';

interface ISetLocalStorageItem {
    type: typeof SET_LOCAL_STORAGE_ITEM;
    payload: {
        name: string;
        item: string;
    };
}

interface IUnsetLocalStorageItem {
    type: typeof UNSET_LOCAL_STORAGE_ITEM;
    payload: string;
}

export type LocalStorageActionsType =
    | ISetLocalStorageItem
    | IUnsetLocalStorageItem;

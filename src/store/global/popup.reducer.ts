import {
    IPopupState,
    PopupActionTypes,
    ADD_POPUP,
    POPUP_PER_TIME,
    REMOVE_POPUP
} from './popup.const';

const initialState: IPopupState = {
    popups: []
};

const reducer = (
    state: IPopupState = initialState,
    action: PopupActionTypes
): IPopupState => {
    switch (action.type) {
        case ADD_POPUP:
            return {
                ...state,
                popups: [action.payload, ...state.popups].slice(
                    0,
                    POPUP_PER_TIME - 1
                )
            };
        case REMOVE_POPUP:
            return {
                ...state,
                popups: state.popups.filter(
                    popup => popup.id !== action.payload
                )
            };
        default:
            return state;
    }
};

export default reducer;

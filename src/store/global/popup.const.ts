export const ADD_POPUP = 'ADD_POPUP';
export const REMOVE_POPUP = 'REMOVE_POPUP';

export const POPUP_HEIGHT = 200;
export const POPUP_SPACING = 10;
export const HEIGHT_THRESHOLD = 83;
export const POPUP_PER_TIME =
    Math.floor(
        (document.documentElement.clientHeight -
            POPUP_SPACING -
            HEIGHT_THRESHOLD) /
            POPUP_HEIGHT
    ) + 1;

export enum PopupType {
    error,
    invitation,
    friendRequest,
    info,
    success
}

interface IBasePopup {
    id: string;
    message: string;
}

export interface IErrorPopup extends IBasePopup {
    type: PopupType.error;
}

export interface ISuccessPopup extends IBasePopup {
    type: PopupType.success;
}

export interface IInfoPopup extends IBasePopup {
    type: PopupType.info;
}

export interface IInvitationPopup extends IBasePopup {
    type: PopupType.invitation;
    link: string;
    username: string;
    userId: string;
}

export interface IFriendRequestPopup extends IBasePopup {
    type: PopupType.friendRequest;
    friendRequestId: number;
    username: string;
    userId: number;
}
export type Popup =
    | IErrorPopup
    | ISuccessPopup
    | IInfoPopup
    | IInvitationPopup
    | IFriendRequestPopup;

interface IAddPopup {
    type: typeof ADD_POPUP;
    payload: Popup;
}

interface IRemovePopup {
    type: typeof REMOVE_POPUP;
    payload: string;
}

export type PopupActionTypes = IAddPopup | IRemovePopup;

export interface IPopupState {
    popups: Popup[];
}

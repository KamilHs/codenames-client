import {
    LocalStorageActionsType,
    SET_LOCAL_STORAGE_ITEM,
    UNSET_LOCAL_STORAGE_ITEM
} from './local-storage.const';
import { localStorageActions } from './local-storage.actions';

describe('Testing localStorage redux', () => {
    describe('Testing action creators', () => {
        const data = { name: 'name', item: 'item' };

        test('Testing setLocalStorageItem', () => {
            const expectedAction: LocalStorageActionsType = {
                type: SET_LOCAL_STORAGE_ITEM,
                payload: {
                    name: data.name,
                    item: JSON.stringify(data.item)
                }
            };

            expect(
                localStorageActions.setLocalStorageItem(data.name, data.item)
            ).toEqual(expectedAction);
        });

        test('Testing unsetLocalStorageItem', () => {
            const expectedAction: LocalStorageActionsType = {
                type: UNSET_LOCAL_STORAGE_ITEM,
                payload: data.name
            };

            expect(
                localStorageActions.unsetLocalStorageItem(data.name)
            ).toEqual(expectedAction);
        });
    });
});

import { AuthActionsType } from 'modules/Auth/redux/const';

import { LocalStorageActionsType } from './global/local-storage.const';

export type AllActionsType = AuthActionsType | LocalStorageActionsType;

export enum FetchStatus {
    loading,
    success,
    failure,
    none
}

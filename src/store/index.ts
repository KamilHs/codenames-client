import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

import authReducer from '../modules/Auth/redux/reducer';
import dashboardReducer from 'modules/Dashboard/redux/reducer';
import {
    loadFromLocalStorage,
    localStorageMiddleware
} from 'middlewares/localStorageMiddleware';
import roomReducer from 'modules/Rooms/redux/reducer';
import profileReducer from 'modules/Profile/redux/reducer';

import popupReducer from './global/popup.reducer';

const createRootReducer = (history: any) =>
    combineReducers({
        router: connectRouter(history),
        auth: authReducer,
        dashboard: dashboardReducer,
        room: roomReducer,
        popup: popupReducer,
        profile: profileReducer
    });

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

const middlewares = [thunk, routerMiddleware(history), localStorageMiddleware];
const rootReducer = createRootReducer(history);

export type RootState = ReturnType<typeof rootReducer>;

export default createStore(
    rootReducer,
    loadFromLocalStorage(),
    composeEnhancers(applyMiddleware(...middlewares))
);

import React from 'react';
import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import {
    PopupType,
    POPUP_HEIGHT,
    POPUP_SPACING
} from 'store/global/popup.const';
import { popupActions } from 'store/global/popup.actions';
import { RootState } from 'store';

import FriendRequestPopup from './components/FriendRequestPopup';
import ErrorPopup from './components/ErrorPopup';
import InfoPopup from './components/InfoPopup';
import SuccessPopup from './components/SuccessPopup';

import useStyles from './index.style';

const Popup: React.FC = () => {
    const classes = useStyles();
    const { popups } = useSelector((state: RootState) => state.popup);

    const dispatch = useDispatch();

    const handleDelete = React.useCallback(
        (id: string) => {
            dispatch(popupActions.removePopup(id));
        },
        [dispatch]
    );

    return (
        <Box>
            {popups.map((popup, index) => (
                <Box
                    key={popup.id}
                    position="fixed"
                    right="0"
                    width="470px"
                    height={`${POPUP_HEIGHT}px`}
                    style={{
                        bottom: `calc(${index * POPUP_HEIGHT}px + ${
                            index * POPUP_SPACING
                        }px)`
                    }}
                    className={classes.popup}>
                    {popup.type === PopupType.success && (
                        <SuccessPopup
                            onClose={() => handleDelete(popup.id)}
                            {...popup}
                        />
                    )}
                    {popup.type === PopupType.error && (
                        <ErrorPopup
                            onClose={() => handleDelete(popup.id)}
                            {...popup}
                        />
                    )}
                    {popup.type === PopupType.info && (
                        <InfoPopup
                            onClose={() => handleDelete(popup.id)}
                            {...popup}
                        />
                    )}
                    {popup.type === PopupType.friendRequest && (
                        <FriendRequestPopup
                            onClose={() => handleDelete(popup.id)}
                            {...popup}
                        />
                    )}
                </Box>
            ))}
        </Box>
    );
};

export default Popup;

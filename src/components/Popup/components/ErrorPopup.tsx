import { Typography } from '@material-ui/core';
import React from 'react';

import { PopupType } from 'store/global/popup.const';

import BasePopup from './BasePopup';

import useStyles from '../index.style';

type ErrorPopupProps = {
    onClose: () => void;
    message: string;
};

const ErrorPopup: React.FC<ErrorPopupProps> = ({ message, onClose }) => {
    const classes = useStyles();

    return (
        <BasePopup onClose={onClose} type={PopupType.error} title="Error">
            <Typography variant="h5" className={classes.message}>
                {message}
            </Typography>
        </BasePopup>
    );
};

export default ErrorPopup;

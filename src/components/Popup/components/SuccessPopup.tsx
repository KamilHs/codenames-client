import React from 'react';
import { Typography } from '@material-ui/core';

import { PopupType } from 'store/global/popup.const';

import BasePopup from './BasePopup';

import useStyles from '../index.style';

type SuccessPopupProps = {
    onClose: () => void;
    message: string;
};

const SuccessPopup: React.FC<SuccessPopupProps> = ({ message, onClose }) => {
    const classes = useStyles();

    return (
        <BasePopup onClose={onClose} type={PopupType.success} title="Success">
            <Typography variant="h5" className={classes.message}>
                {message}
            </Typography>
        </BasePopup>
    );
};

export default SuccessPopup;

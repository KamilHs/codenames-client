import React from 'react';
import { Box, Typography } from '@material-ui/core';
import clsx from 'clsx';

import { PopupType } from 'store/global/popup.const';

import useStyles from '../index.style';

type BasePopupProps = {
    type: PopupType;
    title?: string;
    children: React.ReactNode;
    onClose: () => void;
};

const BasePopup: React.FC<BasePopupProps> = ({
    type,
    title,
    onClose,
    children
}) => {
    const classes = useStyles();

    let className = classes.successTitle;

    switch (type) {
        case PopupType.success:
            className = classes.successTitle;
            break;
        case PopupType.error:
            className = classes.errorTitle;
            break;
        case PopupType.info:
            className = classes.infoTitle;
            break;
        case PopupType.friendRequest:
            className = classes.friendRequestTitle;
            break;
        default:
            className = classes.infoTitle;
            break;
    }

    return (
        <Box
            className={classes.popupInner}
            padding={2}
            position="relative"
            height="100%">
            <Box
                className={classes.close}
                position="absolute"
                top="0"
                right="0"
                padding={2}
                onClick={onClose}>
                <span>X</span>
            </Box>
            <Box marginBottom={2}>
                <Typography
                    variant="h5"
                    className={clsx(classes.title, className)}>
                    {title}
                </Typography>
            </Box>
            <Box>{children}</Box>
        </Box>
    );
};

export default BasePopup;

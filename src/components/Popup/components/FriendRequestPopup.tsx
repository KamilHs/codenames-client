import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { PopupType } from 'store/global/popup.const';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import { SocketContext } from 'context/SocketContext';

import BasePopup from './BasePopup';

import useStyles from '../index.style';
import { dashboardSocketActions } from 'sockets/dashboard/actions';
import Button from 'components/Buttons';

type FriendRequestPopupProps = {
    onClose: () => void;
    message: string;
    friendRequestId: number;
    userId: number;
    username: string;
};

const FriendRequestPopup: React.FC<FriendRequestPopupProps> = ({
    message,
    friendRequestId,
    userId,
    username,
    onClose
}) => {
    const classes = useStyles();
    const { socket } = React.useContext(SocketContext);

    const handleAccept = React.useCallback(() => {
        if (!socket) return;
        dashboardSocketActions.acceptFriend(socket, friendRequestId);
        onClose();
    }, [socket, friendRequestId, onClose]);

    const handleDecline = React.useCallback(() => {
        if (!socket) return;
        dashboardSocketActions.declineFriend(socket, friendRequestId);
        onClose();
    }, [socket, friendRequestId, onClose]);

    return (
        <BasePopup
            onClose={onClose}
            type={PopupType.error}
            title="Friend request">
            <Box display="flex" alignItems="center">
                <Box marginRight={1}>
                    <Typography variant="h5" className={classes.message}>
                        {message}{' '}
                        <Link to={`${PROFILE_ROUTES.profile}/${userId}`}>
                            {username}
                        </Link>
                    </Typography>
                </Box>
            </Box>
            <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                marginTop={2}>
                <Button
                    style={{ marginRight: '10px' }}
                    color="primary"
                    fontFamily='"Press Start 2P"'
                    variant="contained"
                    onClick={handleAccept}>
                    Accept
                </Button>
                <Button
                    color="primary"
                    fontFamily='"Press Start 2P"'
                    variant="contained"
                    onClick={handleDecline}>
                    Decline
                </Button>
            </Box>
        </BasePopup>
    );
};

export default FriendRequestPopup;

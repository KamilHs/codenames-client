import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    popup: {
        zIndex: 2,
        transition: 'all 0.4s ease'
    },
    popupInner: {
        background: 'rgb(29 46 66)',
        border: `1px solid rgba(0, 255, 25, 0.68)`
    },
    title: {
        fontFamily: '"Press Start 2P"',
        textAlign: 'center'
    },
    successTitle: {
        color: 'rgba(0, 255, 25, 0.68)'
    },
    errorTitle: {
        color: '#ff0000'
    },
    infoTitle: {
        color: theme.palette.secondary.light
    },
    friendRequestTitle: {
        color: theme.palette.secondary.light
    },
    close: {
        cursor: 'pointer',
        color: theme.palette.secondary.main,
        fontFamily: '"Press Start 2P"'
    },
    message: {
        textAlign: 'center',
        color: theme.palette.secondary.main,
        fontFamily: 'Ticketing'
    }
}));

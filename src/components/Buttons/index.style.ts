import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    primary: {
        color: theme.palette.primary.main,
        background: theme.palette.secondary.main,
        '&:hover': {
            color: theme.palette.primary.light,
            background: theme.palette.secondary.dark
        }
    },
    secondary: {
        color: theme.palette.secondary.main,
        background: theme.palette.primary.main,
        '&:hover': {
            color: theme.palette.secondary.light,
            background: theme.palette.primary.dark
        }
    },
    inherit: {},
    default: {}
}));

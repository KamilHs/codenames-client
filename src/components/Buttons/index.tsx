import React from 'react';
import { Button as BaseButton, ButtonProps } from '@material-ui/core';
import clsx from 'clsx';

import useStyles from './index.style';

type ButtonExtraProps = {
    fontFamily?: '"Press Start 2P"' | 'Ticketing';
};

const Button: React.FC<ButtonProps & ButtonExtraProps> = ({
    className,
    color,
    children,
    fontFamily,
    style,
    ...props
}) => {
    const classes = useStyles();
    const typeClass = color ? classes[color] : null;

    return (
        <BaseButton
            style={{
                ...style,
                fontFamily
            }}
            className={clsx(typeClass, className)}
            {...props}>
            {children}
        </BaseButton>
    );
};

export default Button;

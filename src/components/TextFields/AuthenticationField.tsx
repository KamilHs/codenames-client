import React from 'react';
import clsx from 'clsx';
import { makeStyles, Theme, withStyles } from '@material-ui/core';
import { TextField, TextFieldProps } from 'formik-material-ui';

export const AuthenticationField = withStyles(theme => ({
    root: {
        '& label': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& label.Mui-focused:not(.Mui-error)': {
            color: theme.palette.secondary.main
        },
        '& .MuiInput-underline:not(.Mui-error):after': {
            borderBottomColor: theme.palette.secondary.main
        },
        '& .MuiFormLabel-root.Mui-disabled': {
            color: theme.palette.secondary.main
        },
        '& p.Mui-error': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& input': {
            color: theme.palette.secondary.main,
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        }
    }
}))(TextField);

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        '& label': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& label.Mui-focused:not(.Mui-error)': {
            color: theme.palette.secondary.main
        },
        '& .MuiInput-underline:not(.Mui-error):after': {
            borderBottomColor: theme.palette.secondary.main
        },
        '& .MuiFormLabel-root.Mui-disabled': {
            color: theme.palette.secondary.main
        },
        '& p.Mui-error': {
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        },
        '& textarea': {
            color: theme.palette.secondary.main,
            fontFamily: "'Ticketing'",
            letterSpacing: '2px'
        }
    }
}));

export const AuthenticationTextField: React.FC<TextFieldProps> = ({
    className,
    ...props
}) => {
    const classes = useStyles();

    return (
        <TextField
            {...props}
            className={clsx(classes.root, className)}
            rows={5}
            multiline
        />
    );
};

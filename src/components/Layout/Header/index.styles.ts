import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    headerContainer: {
        top: 0,
        zIndex: 2,
        width: '100%',
        position: 'fixed',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        transition: `background-color 0.2s ease, box-shadow 0.2s ease`,
        backgroundColor: 'transparent'
    },
    headerContainerScrolled: {
        backgroundColor: theme.palette.primary.dark,
        boxShadow: `0 0 12px 3px ${theme.palette.secondary.main}`
    },
    header: {
        padding: `${theme.spacing(2)}px 0`,
        display: 'flex',
        justifyContent: 'space-between'
    },
    nickname: {
        fontFamily: 'Ticketing',
        marginRight: theme.spacing(2)
    },
    notification: {
        color: theme.palette.secondary.main
    },
    menuPaper: {
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.secondary.main,
        borderRadius: `${theme.shape.borderRadius * 0.5}px`
    },
    menuItem: {
        fontFamily: 'Ticketing',
        color: theme.palette.primary.main
    },
    hoverCursor: {
        cursor: 'pointer'
    },
    badgeSpan: {
        fontFamily: 'Ticketing'
    },
    notificationsPaper: {
        marginTop: theme.spacing(2),
        backgroundColor: theme.palette.secondary.main,
        borderRadius: `${theme.shape.borderRadius * 0.5}px`,
        width: '400px',
        maxHeight: 'calc(100vh - 100px)',
        overflowY: 'auto'
    }
}));

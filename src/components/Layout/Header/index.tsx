import React from 'react';
import Badge from '@material-ui/core/Badge';
import {
    Avatar,
    Box,
    IconButton,
    Menu,
    MenuItem,
    Typography
} from '@material-ui/core';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import NotificationsIcon from '@material-ui/icons/Notifications';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import { useDispatch, useSelector } from 'react-redux';

import { AUTH_PREFIX, AUTH_ROUTES } from 'modules/Auth/routes/const';
import authActions from 'modules/Auth/redux/actions';
import Button from '../../Buttons';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import Notifications from 'components/Notifications';
import { RootState } from 'store';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import { WEBSITE_ROUTES } from 'modules/Website/routes/const';

import useStyles from './index.styles';

const SCROLL_THRESHOLD = 60;

const Header: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { isAuthenticated, user, notifications } = useSelector(
        (state: RootState) => ({
            ...state.auth,
            ...state.dashboard
        })
    );

    const [shadowed, setShadowed] = React.useState<boolean>(
        window.scrollY > SCROLL_THRESHOLD
    );
    const [menuAnchor, setMenuAnchor] = React.useState<null | HTMLElement>(
        null
    );
    const [
        notificationsAnchor,
        setNotificationsAnchor
    ] = React.useState<null | HTMLElement>(null);

    const menuOpen = Boolean(menuAnchor);
    const notificationsOpen = Boolean(notificationsAnchor);

    const handleScroll = React.useCallback(
        () => setShadowed(window.scrollY > SCROLL_THRESHOLD),
        []
    );

    const handleLogout = React.useCallback(
        () => dispatch(authActions.logout()),
        [dispatch]
    );

    const handleNotificationsClick = React.useCallback(
        (event: React.MouseEvent<HTMLElement>) =>
            setNotificationsAnchor(event.currentTarget),
        []
    );

    const handleMenuClick = React.useCallback(
        (event: React.MouseEvent<HTMLElement>) =>
            setMenuAnchor(event.currentTarget),
        []
    );

    React.useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => window.removeEventListener('scroll', handleScroll);
    }, [handleScroll]);

    return (
        <Box
            className={clsx(
                classes.headerContainer,
                shadowed ? classes.headerContainerScrolled : ''
            )}>
            <Box className="container">
                <Box className={classes.header}>
                    <Link to={WEBSITE_ROUTES.main}>
                        <Typography
                            color="secondary"
                            variant="h4"
                            style={{ fontFamily: '"Press Start 2P"' }}>
                            KEREA
                        </Typography>
                    </Link>
                    {isAuthenticated && user ? (
                        <Box display="flex" alignItems="center">
                            <Typography
                                color="secondary"
                                variant="body1"
                                className={classes.nickname}>
                                {user.username}
                            </Typography>
                            <Avatar
                                src={user.avatar}
                                className={classes.hoverCursor}
                                onClick={handleMenuClick}>
                                <PermIdentityIcon />
                            </Avatar>
                            <Menu
                                anchorEl={menuAnchor}
                                getContentAnchorEl={null}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'center'
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center'
                                }}
                                classes={{ paper: classes.menuPaper }}
                                open={menuOpen}
                                onClose={() => setMenuAnchor(null)}
                                keepMounted
                                disableScrollLock>
                                <MenuItem>
                                    <Link
                                        to={PROFILE_ROUTES.profile}
                                        className={classes.menuItem}>
                                        Profile
                                    </Link>
                                </MenuItem>
                                <MenuItem>
                                    <Link
                                        to={DASHBOARD_ROUTES.main}
                                        className={classes.menuItem}>
                                        Dashboard
                                    </Link>
                                </MenuItem>
                                <MenuItem
                                    className={classes.menuItem}
                                    onClick={handleLogout}>
                                    Logout
                                </MenuItem>
                            </Menu>
                            <IconButton
                                onClick={handleNotificationsClick}
                                className={classes.notification}>
                                <Badge
                                    badgeContent={notifications.length}
                                    classes={{ badge: classes.badgeSpan }}
                                    color="error">
                                    <NotificationsIcon />
                                </Badge>
                            </IconButton>
                            <Menu
                                style={{
                                    width: '400px'
                                }}
                                anchorEl={notificationsAnchor}
                                getContentAnchorEl={null}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'center'
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center'
                                }}
                                classes={{ paper: classes.notificationsPaper }}
                                open={notificationsOpen}
                                onClose={() => setNotificationsAnchor(null)}
                                keepMounted
                                disableScrollLock>
                                <MenuItem
                                    style={{ cursor: 'default' }}
                                    disableRipple>
                                    <Notifications />
                                </MenuItem>
                            </Menu>
                        </Box>
                    ) : (
                        <Box display="flex">
                            <Box marginRight={1}>
                                <Link to={AUTH_PREFIX}>
                                    <Button
                                        fontFamily="Ticketing"
                                        color="primary">
                                        Login
                                    </Button>
                                </Link>
                            </Box>
                            <Link to={AUTH_ROUTES.register}>
                                <Button
                                    fontFamily="Ticketing"
                                    color="secondary"
                                    variant="outlined">
                                    SIGN UP
                                </Button>
                            </Link>
                        </Box>
                    )}
                </Box>
            </Box>
        </Box>
    );
};

export default Header;

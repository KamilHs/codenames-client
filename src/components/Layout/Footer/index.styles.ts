/* eslint-disable @typescript-eslint/no-magic-numbers */
import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    footerContainer: {
        bottom: '0px',
        background: theme.palette.primary.dark,
        padding: theme.spacing(6)
    },
    slogan: {
        fontFamily: 'Ticketing',
        color: theme.palette.secondary.main,
        textAlign: 'center'
    },
    copyright: {
        marginTop: theme.spacing(3),
        fontFamily: 'Ticketing',
        width: '100%',
        color: theme.palette.secondary.main,
        background: 'rgba(0,0,0,0.1)',
        textAlign: 'center'
    },
    title: {
        fontFamily: '"Press Start 2P"',
        marginBottom: theme.spacing(3),
        lineHeight: '48px',
        color: theme.palette.secondary.light,
        textTransform: 'uppercase'
    }
}));

import React from 'react';

import { Box, Typography } from '@material-ui/core';

import useStyles from './index.styles';

const Footer: React.FC = () => {
    const classes = useStyles();

    return (
        <Box className={classes.footerContainer}>
            <Box className="container">
                <Typography
                    className={classes.title}
                    color="secondary"
                    align="center"
                    variant="h3">
                    Codenames
                </Typography>
                <Typography className={classes.slogan} variant="h6">
                    Top secret, word game <br />
                    Use your imagination to solve the puzzle
                </Typography>
                <Typography className={classes.copyright} variant="h6">
                    Copyright © KEREA
                </Typography>
            </Box>
        </Box>
    );
};

export default Footer;

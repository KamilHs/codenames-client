import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';

import { RootState } from 'store';

import useStyles from './index.style';
import Notification from 'components/Notifications/Notification';

const Notifications: React.FC = () => {
    const classes = useStyles();
    const { notifications } = useSelector(
        (state: RootState) => state.dashboard
    );

    return notifications.length > 0 ? (
        <Box display="flex" flexDirection="column" width="100%">
            {notifications.map(notification => (
                <Notification key={notification.id} {...notification} />
            ))}
        </Box>
    ) : (
        <Typography
            className={classes.noNotificationTitle}
            align="center"
            variant="body1">
            No Notifications
        </Typography>
    );
};

export default Notifications;

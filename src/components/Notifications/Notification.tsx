import React from 'react';
import { Box, IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Link } from 'react-router-dom';

import { dashboardSocketActions } from 'sockets/dashboard/actions';
import {
    INotification,
    NotificationTypeEnum
} from 'modules/Dashboard/redux/const';
import { PROFILE_ROUTES } from 'modules/Profile/routes/const';
import { SocketContext } from 'context/SocketContext';

import useStyles from './index.style';
import Button from 'components/Buttons';

type NotificationProps = INotification;

const Notification: React.FC<NotificationProps> = ({
    id,
    type,
    userId,
    username,
    friendId
}) => {
    const classes = useStyles();
    const { socket } = React.useContext(SocketContext);

    const canRemove = React.useMemo(
        () => type !== NotificationTypeEnum.friendRequestSent,
        [type]
    );

    const handleRemove = React.useCallback(() => {
        if (!socket || !canRemove) return;
        dashboardSocketActions.removeNotification(socket, id);
    }, [socket, id, canRemove]);

    const handleDecline = React.useCallback(() => {
        if (!socket || !friendId) return;
        dashboardSocketActions.declineFriend(socket, friendId);
    }, [socket, friendId]);

    const handleAccept = React.useCallback(() => {
        if (!socket || !friendId) return;
        dashboardSocketActions.acceptFriend(socket, friendId);
    }, [socket, friendId]);

    return (
        <Box
            width="100%"
            padding={3}
            borderRadius={4}
            marginBottom={2}
            className={classes.notificationContainer}
            position="relative">
            {canRemove && (
                <Box position="absolute" right="0" top="0">
                    <IconButton onClick={handleRemove}>
                        <CloseIcon color="secondary" />
                    </IconButton>
                </Box>
            )}
            <Typography
                style={{ fontFamily: 'Ticketing', whiteSpace: 'normal' }}
                align="center"
                variant="body1"
                color="secondary">
                User{' '}
                <Link to={`${PROFILE_ROUTES.profile}${userId}`}>
                    {username}{' '}
                </Link>
                {type === NotificationTypeEnum.friendRequestAccepted
                    ? 'accepted your friend request'
                    : 'wants to become your friend'}
            </Typography>
            {type === NotificationTypeEnum.friendRequestSent && (
                <Box display="flex" justifyContent="center" marginTop={4}>
                    <Button
                        size="small"
                        style={{ margin: '0 10px' }}
                        fontFamily='"Press Start 2P"'
                        color="primary"
                        variant="contained"
                        onClick={handleAccept}>
                        Accept
                    </Button>
                    <Button
                        size="small"
                        style={{ margin: '0 10px' }}
                        fontFamily='"Press Start 2P"'
                        color="primary"
                        variant="outlined"
                        onClick={handleDecline}>
                        Decline
                    </Button>
                </Box>
            )}
        </Box>
    );
};

export default Notification;

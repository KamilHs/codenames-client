import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    noNotificationTitle: {
        fontFamily: '"Press Start 2P"'
    },
    notificationContainer: {
        background: theme.palette.primary.main
    }
}));

import React from 'react';
import { Box } from '@material-ui/core';

import { OAUTH2_ROUTES, OAuthLinkType } from 'modules/Auth/routes/const';

import OAuthLink from './OAuthLink';
import useStyles from './index.styles';

const OAuthLinks: React.FC = () => {
    const classes = useStyles();

    const handleClick = React.useCallback(
        link => window.open(link, '_self'),
        []
    );

    return (
        <Box className={classes.list}>
            {Object.entries(OAUTH2_ROUTES).map(([type, link]) => (
                <OAuthLink
                    key={type}
                    type={type as OAuthLinkType}
                    onClick={() => handleClick(link)}
                />
            ))}
        </Box>
    );
};

export default OAuthLinks;

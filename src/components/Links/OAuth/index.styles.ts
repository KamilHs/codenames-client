import { makeStyles, Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
    list: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%'
    },
    wrapper: {
        display: 'inline-block',
        margin: `0 ${theme.spacing(1)}px`,
        cursor: 'pointer',
        color: theme.palette.secondary.main,
        backgroundColor: 'transparent',
        transitionTimingFunction: 'cubic-bezier(0.4, 0, 0.2, 1)',
        transitionDuration: '0.25s',
        transitionProperty: 'background-color, border',
        width: '50px',
        height: '50px',
        padding: '3px',
        border: `1px solid ${theme.palette.secondary.dark}`,
        '&:hover': {
            backgroundColor: theme.palette.primary.light,
            border: `1px solid ${theme.palette.secondary.light}`
        },
        '& svg, & svg *': {
            maxWidth: '100%',
            maxHeight: '100%',
            fill: theme.palette.secondary.main
        }
    }
}));

import React from 'react';
import { Avatar } from '@material-ui/core';

import FacebookIcon from 'icons/oauth2/Facebook';
import GithubIcon from 'icons/oauth2/Github';
import GoogleIcon from 'icons/oauth2/Google';
import { OAuthLinkType } from 'modules/Auth/routes/const';

import useStyles from './index.styles';

type OAuthLinkProps = {
    type: OAuthLinkType;
    onClick: () => void;
};

const OAuthLink: React.FC<OAuthLinkProps> = ({ onClick, type }) => {
    const classes = useStyles();

    return (
        <Avatar onClick={onClick} className={classes.wrapper}>
            {type === OAuthLinkType.github && <GithubIcon />}
            {type === OAuthLinkType.google && <GoogleIcon />}
            {type === OAuthLinkType.facebook && <FacebookIcon />}
        </Avatar>
    );
};

export default OAuthLink;

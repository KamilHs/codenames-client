import React from 'react';
import { useDispatch } from 'react-redux';

import authActions from 'modules/Auth/redux/actions';

type LocalStorageContextProps = {
    checkAuth: () => void;
};

type LocalStorageContextProviderProps = {
    children: React.ReactNode;
};

const initialProps: LocalStorageContextProps = {
    checkAuth: () => void 0
};

export const LocalStorageContext = React.createContext<LocalStorageContextProps>(
    initialProps
);

export const LocalStorageContextProvider: React.FC<LocalStorageContextProviderProps> = ({
    children
}) => {
    const dispatch = useDispatch();
    const listener = React.useCallback(
        (_e: StorageEvent | null, getUser = false) => {
            const token = localStorage.getItem('token');

            if (!token) return dispatch(authActions.logout());
            if (!localStorage.getItem('user') || getUser)
                dispatch(authActions.getUser(JSON.parse(token)));
        },
        [dispatch]
    );

    React.useEffect(() => {
        window.addEventListener('storage', listener);

        return () => {
            window.removeEventListener('storage', listener);
        };
    }, [listener]);

    const checkAuth = React.useCallback(() => listener(null, true), [listener]);

    const value = React.useMemo<LocalStorageContextProps>(
        () => ({
            checkAuth
        }),
        [checkAuth]
    );

    return (
        <LocalStorageContext.Provider value={value}>
            {children}
        </LocalStorageContext.Provider>
    );
};

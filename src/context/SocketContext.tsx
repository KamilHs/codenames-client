import React from 'react';
import SocketIOClient from 'socket.io-client';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from 'store';
import { socketListeners } from 'sockets';

type SocketContextProps = {
    socket: SocketIOClient.Socket | null;
};

type SocketContextProviderProps = {
    children: React.ReactNode;
};

const initialProps: SocketContextProps = {
    socket: null
};

export const SocketContext = React.createContext<SocketContextProps>(
    initialProps
);

export const SocketContextProvider: React.FC<SocketContextProviderProps> = ({
    children
}) => {
    const [socket, setSocket] = React.useState<SocketIOClient.Socket | null>(
        null
    );

    const { isAuthenticated, token } = useSelector(
        (state: RootState) => state.auth
    );

    const dispatch = useDispatch();

    React.useEffect(() => {
        if (!isAuthenticated || !token) {
            if (socket) {
                socket.removeAllListeners();
                socket.disconnect();
                setSocket(null);
            }

            return;
        }
        if (socket) return;

        setSocket(
            SocketIOClient(process.env.REACT_APP_API_URL!, {
                query: {
                    token
                }
            })
        );
    }, [isAuthenticated, token, socket]);

    React.useEffect(() => {
        if (!socket) return;

        socketListeners(dispatch, socket);
    }, [socket, dispatch]);

    const value = React.useMemo<SocketContextProps>(
        () => ({
            socket
        }),
        [socket]
    );

    return (
        <SocketContext.Provider value={value}>
            {children}
        </SocketContext.Provider>
    );
};

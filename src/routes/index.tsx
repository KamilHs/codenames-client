import React from 'react';
import { Route, Switch } from 'react-router';

import { AuthRoutes } from 'modules/Auth/routes';
import { DashboardRoutes } from 'modules/Dashboard/routes';
import { RoomRoutes } from 'modules/Rooms/routes';
import { WebsiteRoutes } from 'modules/Website/routes';
import { ProfileRoutes } from 'modules/Profile/routes';

const Routes: React.FC = () => (
    <Switch>
        <Route path="/">
            <AuthRoutes />
            <DashboardRoutes />
            <RoomRoutes />
            <WebsiteRoutes />
            <ProfileRoutes />
        </Route>
    </Switch>
);

export default Routes;

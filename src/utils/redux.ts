import { popupActions } from 'store/global/popup.actions';

export const handleError = (error: any, dispatch: any) => {
    try {
        if (error.response) {
            const {
                response: { status, data }
            } = error;

            if (status === 500)
                dispatch(popupActions.addErrorPopup('Internal Server Error'));
            else if (data.data?.message)
                dispatch(popupActions.addErrorPopup(data.data.message));
            else if (data.error?.message)
                dispatch(popupActions.addErrorPopup(data.error.message));
            else if (data.error.body?.length)
                dispatch(popupActions.addErrorPopup(data.error.body[0]));
            else dispatch(popupActions.addErrorPopup('Bad request'));
        } else if (error.message)
            dispatch(popupActions.addErrorPopup(error.message));
        else dispatch(popupActions.addErrorPopup('Something went wrong'));
    } catch {
        dispatch(popupActions.addErrorPopup('Something went wrong'));
    }
};

import { IGame } from 'modules/Rooms/redux/const';
import { IProfileData } from 'modules/Profile/redux/const';
import { IUser } from '../../modules/Auth/redux/const';

export interface IError {
    status: number;
    message: string;
    body?: Record<string, unknown>;
}

export interface IBaseResponse {
    data: Record<string, unknown> | false;
    error: IError | false;
}

export interface IRegisterResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface ILoginResponse extends IBaseResponse {
    data:
        | {
              isAuthenticated: boolean;
              token: string;
              user: IUser;
          }
        | false;
}

export interface IGetUser {
    token: string;
}
export interface IGetUserResponse extends IBaseResponse {
    data:
        | {
              user: IUser;
          }
        | false;
}

export interface IVerifyEmailResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface ICreatePasswordResetResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface IGetPasswordResetResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface IConfirmPasswordResetResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface ICreateRoomResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              slug: string;
          }
        | false;
}

export interface IGetRoomResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              game: IGame;
          }
        | false;
}

export interface IGetActiveRoomResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              slug: string;
          }
        | false;
}
export interface IGetAvatarsResponse extends IBaseResponse {
    data:
        | {
              avatars: string[];
          }
        | false;
}

export interface IUpdateProfileResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              user: IUser | null;
              token: string | null;
              message: string;
          }
        | false;
}

export interface IGetProfileResponse extends IBaseResponse {
    data:
        | {
              profile: IProfileData;
          }
        | false;
}

export interface IUpdatePasswordResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export interface IDeleteAccountResponse extends IBaseResponse {
    data:
        | {
              success: boolean;
              message: string;
          }
        | false;
}

export const getAuthHeader = (token: string) => ({
    Authorization: `Bearer ${token}`
});

import { validate } from '../index';
import { ValidationType } from '../const';

describe('Login validation', () => {
    test('No data', () => {
        const result = validate(ValidationType.login, {
            email: '',
            password: ''
        });

        expect(result).toHaveProperty('email');
        expect(result).toHaveProperty('password');
    });

    test('Short password', () => {
        const result = validate(ValidationType.login, {
            email: '',
            password: '123'
        });

        expect(result).toHaveProperty('password');
    });

    test('Invalid type password', () => {
        const result = validate(ValidationType.login, {
            email: '',
            password: 123
        });

        expect(result).toHaveProperty('password');
    });

    test('Valid password', () => {
        const result = validate(ValidationType.login, {
            email: '',
            password: 'securedpassword'
        });

        expect(result).not.toHaveProperty('password');
    });

    test('Invalid email', () => {
        const result = validate(ValidationType.login, {
            email: 'random',
            password: ''
        });

        expect(result).toHaveProperty('email');
    });

    test('Invalid type email', () => {
        const result = validate(ValidationType.login, {
            email: 123,
            password: ''
        });

        expect(result).toHaveProperty('email');
    });

    test('Valid email', () => {
        const result = validate(ValidationType.login, {
            email: 'example@gmail.com',
            password: ''
        });

        expect(result).not.toHaveProperty('email');
    });
});

describe('Register validation', () => {
    test('No data', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: '',
            username: '',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('email');
        expect(result).toHaveProperty('password');
        expect(result).toHaveProperty('username');
        expect(result).toHaveProperty('confirmPassword');
    });

    test('Short password', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: '123',
            username: '',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('password');
    });

    test('Invalid type password', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: 123,
            username: '',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('password');
    });

    test('Valid password', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: 'securedpassword',
            username: '',
            confirmPassword: ''
        });

        expect(result).not.toHaveProperty('password');
    });

    test('Invalid email', () => {
        const result = validate(ValidationType.register, {
            email: 'random',
            password: '',
            username: '',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('email');
    });

    test('Invalid type email', () => {
        const result = validate(ValidationType.register, {
            email: 123,
            password: '',
            username: '',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('email');
    });

    test('Valid email', () => {
        const result = validate(ValidationType.register, {
            email: 'example@gmail.com',
            password: '',
            username: '',
            confirmPassword: ''
        });

        expect(result).not.toHaveProperty('email');
    });

    test('Invalid username', () => {
        const result = validate(ValidationType.register, {
            email: 'random',
            password: '',
            username: '`&^%*$#($',
            confirmPassword: ''
        });

        expect(result).toHaveProperty('username');
    });

    test('Invalid type username', () => {
        const result = validate(ValidationType.register, {
            email: 'random',
            password: '',
            username: 123,
            confirmPassword: ''
        });

        expect(result).toHaveProperty('username');
    });

    test('Valid username', () => {
        const result = validate(ValidationType.register, {
            email: 'random',
            password: '',
            username: 'bestNick123',
            confirmPassword: ''
        });

        expect(result).not.toHaveProperty('username');
    });

    test('No match passwords', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: '123123123',
            username: '',
            confirmPassword: '321321321'
        });

        expect(result).toHaveProperty('confirmPassword');
    });

    test('Match passwords', () => {
        const result = validate(ValidationType.register, {
            email: '',
            password: '123123123',
            username: '',
            confirmPassword: '123123123'
        });

        expect(result).not.toHaveProperty('confirmPassword');
    });
});

describe('Password Reset validation', () => {
    test('No data', () => {
        const result = validate(ValidationType.createPasswordReset, {
            email: ''
        });

        expect(result).toHaveProperty('email');
    });

    test('Invalid email', () => {
        const result = validate(ValidationType.createPasswordReset, {
            email: 'random'
        });

        expect(result).toHaveProperty('email');
    });

    test('Invalid type email', () => {
        const result = validate(ValidationType.createPasswordReset, {
            email: 123
        });

        expect(result).toHaveProperty('email');
    });

    test('Valid email', () => {
        const result = validate(ValidationType.createPasswordReset, {
            email: 'example@gmail.com'
        });

        expect(result).not.toHaveProperty('email');
    });
});

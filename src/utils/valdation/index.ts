import {
    RULES_BY_TYPE,
    getRulesByType,
    _validate,
    getErrorMessage,
    RulesKeys
} from './const';

export const validate = <T extends RulesKeys>(
    type: T,
    values: Record<keyof typeof RULES_BY_TYPE[T], any>
) => {
    const allRules = getRulesByType(type);

    return Object.entries(allRules).reduce((acc, [key, rule]) => {
        const invalidRule = rule.find(
            r => !_validate(r, values, values[key as keyof typeof values])
        );

        return invalidRule
            ? {
                  ...acc,
                  [key]: getErrorMessage(values, key, invalidRule)
              }
            : acc;
    }, {});
};

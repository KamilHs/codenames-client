import { isEqual, isString, isArray } from 'lodash';

export enum Rules {
    required = 'required',
    string = 'string',
    email = 'email',
    min = 'min',
    alphanumeric = 'alphanumeric',
    equal = 'equal'
}

export enum ExtraArgumentType {
    key,
    numberValue,
    stringValue
}

export enum ValidationType {
    login,
    register,
    createPasswordReset,
    passwordReset,
    profileEdit,
    updatePassword
}

export interface IExtraArgument {
    type: ExtraArgumentType;
    value: any;
}

export type RuleElement = Rules | [Rules, IExtraArgument[]];

export interface IRuleData {
    ruleType: Rules;
    args: IExtraArgument[];
}

export const CONST_VALUES = {
    password: {
        min: 8
    }
};

export const RULES_FUNCTIONS = {
    [Rules.required]: (val?: any) => !!val,
    [Rules.string]: isString,
    [Rules.email]: (val = '') =>
        /^[\w%+.-]+@[\d.a-z-]+\.[a-z]{2,4}$/i.test(val),
    [Rules.min]: (val = '', n = 0) => val.length >= n,
    [Rules.alphanumeric]: (val = '') => /^\w*$/.test(val),
    [Rules.equal]: (val1?: any, val2?: any) => isEqual(val1, val2)
};

export const RULES: Record<string, RuleElement[]> = {
    email: [Rules.required, Rules.string, Rules.email],
    username: [Rules.required, Rules.string],
    password: [
        Rules.required,
        Rules.string,
        [
            Rules.min,
            [
                {
                    type: ExtraArgumentType.numberValue,
                    value: CONST_VALUES.password.min
                }
            ]
        ]
    ],
    confirmPassword: [
        Rules.required,
        [
            Rules.equal,
            [
                {
                    type: ExtraArgumentType.key,
                    value: 'password'
                },
                { type: ExtraArgumentType.stringValue, value: 'Password' }
            ]
        ]
    ]
};

export const RULES_BY_TYPE = {
    [ValidationType.login]: {
        email: RULES.email,
        password: RULES.password
    },
    [ValidationType.register]: {
        email: RULES.email,
        username: RULES.username,
        password: RULES.password,
        confirmPassword: RULES.confirmPassword
    },
    [ValidationType.createPasswordReset]: {
        email: RULES.email
    },
    [ValidationType.passwordReset]: {
        password: RULES.password,
        confirmPassword: RULES.confirmPassword
    },
    [ValidationType.profileEdit]: {
        username: RULES.username
    },
    [ValidationType.updatePassword]: {
        oldPassword: RULES.password,
        password: RULES.password,
        confirmPassword: RULES.confirmPassword
    }
};

export const ERRORS: Record<Rules, (...args: any[]) => string> = {
    [Rules.required]: () => 'Required',
    [Rules.string]: () => 'Must be a string',
    [Rules.email]: () => 'Invalid email',
    [Rules.min]: (key?: string, n?: number) => `Minimum ${n} long`,
    [Rules.alphanumeric]: () => 'Must be alphanumeric',
    [Rules.equal]: (key?: string, label?: string) => `${label} must match`
};

export const getRuleData = (rule: RuleElement): IRuleData =>
    isArray(rule)
        ? { ruleType: rule[0], args: rule[1] }
        : { ruleType: rule, args: [] };

export const getRulesByType = <T extends ValidationType>(
    type: T
): typeof RULES_BY_TYPE[T] => RULES_BY_TYPE[type];

export type RulesKeys = keyof typeof RULES_BY_TYPE;

export const getErrorMessage = (
    values: Record<string, any>,
    key: string,
    rule: RuleElement
): string => {
    const { ruleType, args } = getRuleData(rule);

    return ERRORS[ruleType](key, ...transformArguments(values, args));
};

export const _validate = (
    rule: RuleElement,
    values: Record<string, any>,
    value: any
): boolean => {
    const { ruleType, args } = getRuleData(rule);

    return RULES_FUNCTIONS[ruleType](
        value,
        ...transformArguments(values, args, true)
    );
};

export const transformArguments = (
    values: Record<string, any>,
    args: IExtraArgument[],
    withKeys = false
) =>
    args
        .map(({ type, value }) => {
            switch (type) {
                case ExtraArgumentType.key:
                    return withKeys ? values[value] : false;
                case ExtraArgumentType.numberValue:
                    return +value;
                case ExtraArgumentType.stringValue:
                    return `${value}`;
                default:
                    return false;
            }
        })
        .filter(Boolean);

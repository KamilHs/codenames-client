import axios, { AxiosResponse } from 'axios';

import { API_ROOM_ROUTES } from 'utils/api/routes/room';
import { getAuthHeader, IGetRoomResponse } from 'utils/axios/const';

const roomApi = {
    getRoom: (
        token: string,
        slug: string
    ): Promise<AxiosResponse<IGetRoomResponse>> =>
        axios.get<IGetRoomResponse>(`${API_ROOM_ROUTES.getRoom}/${slug}`, {
            headers: getAuthHeader(token)
        })
};

export default roomApi;

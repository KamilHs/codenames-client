import axios, { AxiosResponse } from 'axios';

import { API_DASHBOARD_ROUTES } from 'utils/api/routes/dashboard';
import {
    getAuthHeader,
    ICreateRoomResponse,
    IGetActiveRoomResponse
} from 'utils/axios/const';

const dashboardApi = {
    createRoom: (token: string): Promise<AxiosResponse<ICreateRoomResponse>> =>
        axios.post<ICreateRoomResponse>(API_DASHBOARD_ROUTES.createRoom, null, {
            headers: getAuthHeader(token)
        }),
    getActiveRoom: (
        token: string
    ): Promise<AxiosResponse<IGetActiveRoomResponse>> =>
        axios.get<IGetActiveRoomResponse>(API_DASHBOARD_ROUTES.getActiveRoom, {
            headers: getAuthHeader(token)
        })
};

export default dashboardApi;

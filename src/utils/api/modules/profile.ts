import { AxiosResponse } from 'axios';

import axios from '../../axios';
import { API_PROFILE_ROUTES } from 'utils/api/routes/profile';
import {
    getAuthHeader,
    IGetAvatarsResponse,
    IGetProfileResponse,
    IUpdateProfileResponse
} from 'utils/axios/const';
import { IEditableData } from 'modules/Profile/redux/const';

const profileApi = {
    getAvatars: (token: string): Promise<AxiosResponse<IGetAvatarsResponse>> =>
        axios.get<IGetAvatarsResponse>(API_PROFILE_ROUTES.getAvatars, {
            headers: getAuthHeader(token)
        }),
    setProfile: (
        token: string,
        profile: IEditableData
    ): Promise<AxiosResponse<IUpdateProfileResponse>> =>
        axios.put<IUpdateProfileResponse>(
            API_PROFILE_ROUTES.setProfile,
            profile,
            {
                headers: getAuthHeader(token)
            }
        ),
    getProfile: (
        token: string,
        userId: number
    ): Promise<AxiosResponse<IGetProfileResponse>> =>
        axios.get<IGetProfileResponse>(
            `${API_PROFILE_ROUTES.getProfile}/${userId}`,
            {
                headers: getAuthHeader(token)
            }
        )
};

export default profileApi;

import { AxiosResponse } from 'axios';

import { API_AUTH_ROUTES } from '../routes/auth';
import axios from '../../axios';
import { ILogin, IRegister } from 'modules/Auth/redux/const';
import {
    IConfirmPasswordResetResponse,
    ICreatePasswordResetResponse,
    IDeleteAccountResponse,
    getAuthHeader,
    IGetUserResponse,
    ILoginResponse,
    IRegisterResponse,
    IVerifyEmailResponse,
    IGetPasswordResetResponse,
    IGetUser,
    IUpdatePasswordResponse
} from '../../axios/const';

const authApi = {
    register: (data: IRegister): Promise<AxiosResponse<IRegisterResponse>> =>
        axios.post<IRegisterResponse>(API_AUTH_ROUTES.register, data),
    login: (data: ILogin): Promise<AxiosResponse<ILoginResponse>> =>
        axios.post<ILoginResponse>(API_AUTH_ROUTES.login, data),
    getUser: (data: IGetUser): Promise<AxiosResponse<IGetUserResponse>> =>
        axios.post<IGetUserResponse>(API_AUTH_ROUTES.getUser, data),
    verifyEmail: (
        verificationId: string,
        token: string
    ): Promise<AxiosResponse<IVerifyEmailResponse>> =>
        axios.delete<IVerifyEmailResponse>(API_AUTH_ROUTES.verifyEmail, {
            headers: getAuthHeader(token),
            data: { token: verificationId }
        }),
    createPasswordReset: (
        email: string
    ): Promise<AxiosResponse<ICreatePasswordResetResponse>> =>
        axios.post<ICreatePasswordResetResponse>(
            API_AUTH_ROUTES.passwordReset,
            { email }
        ),
    getPasswordReset: (
        resetId: string
    ): Promise<AxiosResponse<IGetPasswordResetResponse>> =>
        axios.get<IGetPasswordResetResponse>(
            `${API_AUTH_ROUTES.passwordReset}/${resetId}`
        ),
    confirmPasswordReset: (
        password: string,
        resetId: string
    ): Promise<AxiosResponse<IConfirmPasswordResetResponse>> =>
        axios.delete<IConfirmPasswordResetResponse>(
            API_AUTH_ROUTES.passwordReset,
            {
                data: { token: resetId, password }
            }
        ),
    updatePassword: (
        token: string,
        password: string,
        updatedPassword: string
    ): Promise<AxiosResponse<IUpdatePasswordResponse>> =>
        axios.put<IUpdatePasswordResponse>(
            API_AUTH_ROUTES.updatePassword,
            { updatedPassword, password },
            {
                headers: getAuthHeader(token)
            }
        ),
    deleteAccount: (
        token: string
    ): Promise<AxiosResponse<IDeleteAccountResponse>> =>
        axios.delete<IDeleteAccountResponse>(API_AUTH_ROUTES.deleteAccount, {
            headers: getAuthHeader(token)
        })
};

export default authApi;

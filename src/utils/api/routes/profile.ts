export const API_PROFILE_ROUTES = {
    getProfile: '/profile',
    getAvatars: '/profile/get-avatars',
    setProfile: '/profile/update-profile'
};

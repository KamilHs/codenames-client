export const API_DASHBOARD_ROUTES = {
    createRoom: '/rooms/',
    getActiveRoom: '/rooms/active-game-slug'
};

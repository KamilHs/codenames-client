export const API_AUTH_ROUTES = {
    login: 'auth/login',
    register: 'auth/register',
    getUser: 'auth/get-user',
    verifyEmail: 'auth/verify-email',
    passwordReset: 'auth/password-reset',
    updatePassword: 'auth/update-password',
    deleteAccount: 'profile/delete-profile'
};

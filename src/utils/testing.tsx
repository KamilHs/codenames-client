import React from 'react';
import { ShallowWrapper, HTMLAttributes } from 'enzyme';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import defaultStore, { history, RootState } from 'store';

export const findByDataTestIdAttribute = (
    wrapper: ShallowWrapper<
        any,
        Readonly<Record<string, unknown>>,
        React.Component<Record<string, unknown>, Record<string, unknown>, any>
    >,
    dataTestId: string
): ShallowWrapper<HTMLAttributes, any> =>
    wrapper.find(`[data-testid="${dataTestId}"]`);

interface wrapToGlobalOptions {
    store: Partial<RootState>;
}

const defaultOptions: wrapToGlobalOptions = {
    store: {}
};

export const wrapToGlobal = (
    content: React.ReactElement<any, any>,
    options: Partial<wrapToGlobalOptions> = defaultOptions
): React.ReactElement<any, any> => (
    <Provider store={{ ...defaultStore, ...options.store }}>
        <ConnectedRouter history={history}>{content}</ConnectedRouter>
    </Provider>
);

export const runAllPromises = () => new Promise(setImmediate);

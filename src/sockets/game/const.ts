import {
    GameStatus,
    Assumptions,
    ICard,
    IGameLog,
    InGameState,
    Language,
    PlayerRoleEnum,
    TeamTypeEnum
} from 'modules/Rooms/redux/const';

export const ANNOUNCE_WINNER = 'ANNOUNCE_WINNER';
export const USER_MAKE_ASSUMPTION = 'USER_MAKE_ASSUMPTION';
export const USER_REMOVE_ASSUMPTION = 'USER_REMOVE_ASSUMPTION';
export const MOVE_GUESS = 'MOVE_GUESS';
export const MOVE_END_GUESS = 'MOVE_END_GUESS';
export const MOVE_GIVE_CLUE = 'MOVE_GIVE_CLUE';
export const USER_GET_GAME_DATA = 'USER_GET_GAME_DATA';
export const ADMIN_GET_DECK_DATA = 'ADMIN_GET_DECK_DATA';
export const USER_START_GAME = 'USER_START_GAME';
export const GET_ASSUMPTIONS = 'GET_ASSUMPTIONS';

export type AnnounceWinnerResType = {
    winner: TeamTypeEnum;
};

export type GetLanguageResType = {
    decks: Language[];
};

export type GetAssumptionsResType = {
    assumptions: Assumptions;
};

export type UserGetGameDataRes = {
    assumptions: Assumptions;
    cards: ICard[][];
    gameLogs: IGameLog[];
    game: {
        turn: number;
        status: GameStatus;
        inGameState: InGameState;
        currentTeamId: number;
    };
    clue: string;
    leftCards: Record<TeamTypeEnum, number> | null;
    player: {
        id: number;
        role: PlayerRoleEnum;
        teamId: number;
    } | null;
};

import SocketIOClient from 'socket.io-client';

import {
    ADMIN_GET_DECK_DATA,
    MOVE_END_GUESS,
    MOVE_GIVE_CLUE,
    MOVE_GUESS,
    USER_GET_GAME_DATA,
    USER_MAKE_ASSUMPTION,
    USER_REMOVE_ASSUMPTION,
    USER_START_GAME
} from './const';

export const gameSocketActions = {
    getDecksData: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(ADMIN_GET_DECK_DATA, { slug }),
    startGame: (
        socket: SocketIOClient.Socket,
        slug: string,
        deckIds: number[]
    ) => socket.emit(USER_START_GAME, { slug, deckIds }),
    giveClue: (socket: SocketIOClient.Socket, slug: string, clue: string) =>
        socket.emit(MOVE_GIVE_CLUE, { slug, clue }),
    makeGuess: (socket: SocketIOClient.Socket, slug: string, cardId: number) =>
        socket.emit(MOVE_GUESS, { slug, cardId }),
    endGuess: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(MOVE_END_GUESS, { slug }),
    makeAssumption: (
        socket: SocketIOClient.Socket,
        slug: string,
        cardId: number
    ) => socket.emit(USER_MAKE_ASSUMPTION, { slug, cardId }),
    removeAssumption: (
        socket: SocketIOClient.Socket,
        slug: string,
        cardId: number
    ) => socket.emit(USER_REMOVE_ASSUMPTION, { slug, cardId }),
    getGameData: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(USER_GET_GAME_DATA, { slug })
};

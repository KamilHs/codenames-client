import { Dispatch } from 'redux';
import SocketIOClient from 'socket.io-client';

import { FetchStatus } from 'store/const';
import roomActions from 'modules/Rooms/redux/actions';

import {
    ADMIN_GET_DECK_DATA,
    GetLanguageResType,
    USER_GET_GAME_DATA,
    UserGetGameDataRes,
    ANNOUNCE_WINNER,
    GET_ASSUMPTIONS,
    GetAssumptionsResType,
    AnnounceWinnerResType
} from './const';

export const gameSocketListeners = (
    dispatch: Dispatch<any>,
    socket: SocketIOClient.Socket
) => {
    socket.on(ADMIN_GET_DECK_DATA, (res: GetLanguageResType) =>
        dispatch(roomActions.setLanguageData(res.decks))
    );
    socket.on(USER_GET_GAME_DATA, (res: UserGetGameDataRes) => {
        dispatch(
            roomActions.setGameData({
                turn: res.game.turn,
                state: res.game.inGameState,
                status: res.game.status,
                currentTeamId: res.game.currentTeamId
            })
        );
        dispatch(roomActions.setAssumptions(res.assumptions));
        dispatch(roomActions.setRoomStatus(FetchStatus.success));
        dispatch(roomActions.setLeftCards(res.leftCards));
        dispatch(roomActions.setGamelogs(res.gameLogs));
        dispatch(roomActions.setCards(res.cards));
        dispatch(roomActions.setPlayerData(res.player));
        dispatch(roomActions.setClue(res.clue));
    });
    socket.on(GET_ASSUMPTIONS, (res: GetAssumptionsResType) =>
        dispatch(roomActions.setAssumptions(res.assumptions))
    );
    socket.on(ANNOUNCE_WINNER, (res: AnnounceWinnerResType) =>
        dispatch(roomActions.setWinner(res.winner))
    );
};

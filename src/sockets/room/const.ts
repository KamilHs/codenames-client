import { IRoomData, PlayerRoleEnum } from 'modules/Rooms/redux/const';

export const USER_JOIN_ROOM = 'USER_JOIN_ROOM';
export const USER_JOIN_TEAM = 'USER_JOIN_TEAM';
export const USER_LEAVE_ROOM = 'USER_LEAVE_ROOM';
export const USER_LEAVE_TEAM = 'USER_LEAVE_TEAM';

export const ADMIN_KICK_PLAYER = 'ADMIN_KICK_PLAYER';
export const ADMIN_RESET_TEAM = 'ADMIN_RESET_TEAM';

export const KICKED_BY_ADMIN = 'KICKED_BY_ADMIN';
export const KICKED_AFTER_ADMIN_LEFT = 'KICKED_AFTER_ADMIN_LEFT';
export const KICKED_FROM_PREV_TAB = 'KICKED_FROM_PREV_TAB';
export const KICKED_FOR_INACTIVITY = 'KICKED_FOR_INACTIVITY';

export const ROOM_GET_DATA = 'ROOM_GET_DATA';

export enum KickType {
    player,
    spectator
}

export type RoomJoinReqType = {
    slug: string;
};

export type RoomLeaveReqType = {
    slug: string;
};

export type TeamLeaveReqType = {
    slug: string;
};

export type TeamJoinReqType = {
    slug: string;
    teamId: number;
    role: PlayerRoleEnum;
};

export type AdminResetTeamsReqType = {
    slug: string;
};

export type AdminKickPlayerReqType = {
    slug: string;
    id: number;
    type: KickType;
};

export type GetRoomDataResType = {
    roomData: IRoomData;
};

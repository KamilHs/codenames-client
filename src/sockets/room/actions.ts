import { PlayerRoleEnum } from 'modules/Rooms/redux/const';
import SocketIOClient from 'socket.io-client';

import {
    ADMIN_KICK_PLAYER,
    ADMIN_RESET_TEAM,
    KickType,
    USER_JOIN_ROOM,
    USER_JOIN_TEAM,
    USER_LEAVE_ROOM,
    USER_LEAVE_TEAM
} from './const';

export const roomSocketActions = {
    joinRoom: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(USER_JOIN_ROOM, { slug }),
    joinTeam: (
        socket: SocketIOClient.Socket,
        slug: string,
        teamId: number,
        role: PlayerRoleEnum
    ) => socket.emit(USER_JOIN_TEAM, { slug, role, teamId }),
    leaveTeam: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(USER_LEAVE_TEAM, { slug }),
    leaveRoom: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(USER_LEAVE_ROOM, { slug }),
    kickMember: (
        socket: SocketIOClient.Socket,
        type: KickType,
        id: number,
        slug: string
    ) => socket.emit(ADMIN_KICK_PLAYER, { type, id, slug }),
    resetTeams: (socket: SocketIOClient.Socket, slug: string) =>
        socket.emit(ADMIN_RESET_TEAM, { slug })
};

import { Dispatch } from 'redux';
import SocketIOClient from 'socket.io-client';

import roomActions from 'modules/Rooms/redux/actions';

import {
    KICKED_AFTER_ADMIN_LEFT,
    KICKED_BY_ADMIN,
    ROOM_GET_DATA,
    GetRoomDataResType,
    KICKED_FROM_PREV_TAB,
    KICKED_FOR_INACTIVITY
} from './const';
import { push } from 'connected-react-router';
import { DASHBOARD_ROUTES } from 'modules/Dashboard/routes/const';
import { popupActions } from 'store/global/popup.actions';

export const roomSocketListeners = (
    dispatch: Dispatch<any>,
    socket: SocketIOClient.Socket
) => {
    socket.on(ROOM_GET_DATA, (res: GetRoomDataResType) => {
        dispatch(roomActions.setRoomData(res.roomData));
    });
    socket.on(KICKED_BY_ADMIN, (res: { message: string }) => {
        dispatch(popupActions.addInfoPopup(res.message));
        dispatch(push(DASHBOARD_ROUTES.main));
    });
    socket.on(KICKED_AFTER_ADMIN_LEFT, (res: { message: string }) => {
        dispatch(popupActions.addInfoPopup(res.message));
        dispatch(push(DASHBOARD_ROUTES.main));
    });
    socket.on(KICKED_FOR_INACTIVITY, (res: { message: string }) => {
        dispatch(popupActions.addInfoPopup(res.message));
        dispatch(push(DASHBOARD_ROUTES.main));
    });
    socket.on(KICKED_FROM_PREV_TAB, (res: { message: string }) => {
        dispatch(popupActions.addInfoPopup(res.message));
        dispatch(roomActions.setTabDisabled(true));
    });
};

import { Dispatch } from 'redux';
import SocketIOClient from 'socket.io-client';

import dashboardActions from 'modules/Dashboard/redux/actions';
import { handleError } from 'utils/redux';

import { CUSTOM_CONNECT, DISCONNECT, ERROR } from './const';
import { dashboardSocketListeners } from './dashboard/listeners';
import { gameSocketListeners } from './game/listeners';
import { roomSocketListeners } from './room/listeners';

export const socketListeners = (
    dispatch: Dispatch<any>,
    socket: SocketIOClient.Socket
) => {
    socket.removeAllListeners();

    socket.on(CUSTOM_CONNECT, () => {
        dispatch(dashboardActions.setSocketConnected(true));
    });

    socket.on(ERROR, (error: any) => {
        handleError(error, dispatch);
    });

    socket.on(DISCONNECT, () => {
        dispatch(dashboardActions.setSocketConnected(false));
    });

    dashboardSocketListeners(dispatch, socket);
    gameSocketListeners(dispatch, socket);
    roomSocketListeners(dispatch, socket);
};

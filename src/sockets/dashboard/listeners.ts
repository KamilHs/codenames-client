import dashboardActions from 'modules/Dashboard/redux/actions';
import { Dispatch } from 'redux';
import SocketIOClient from 'socket.io-client';
import { popupActions } from 'store/global/popup.actions';

import {
    GetDashboardDataResType,
    GetMessagesResType,
    GET_DASHBOARD_DATA,
    SendFriendRequestAccepterResType,
    SendFriendRequestRequesterResType,
    UserSearchUserResType,
    USER_GET_MESSAGES,
    USER_SEARCH_USER,
    USER_SEND_FRIEND_REQUEST
} from './const';

const isRequestRequester = (
    res: SendFriendRequestRequesterResType | SendFriendRequestAccepterResType
): res is SendFriendRequestRequesterResType =>
    (res as SendFriendRequestRequesterResType).success !== undefined;

export const dashboardSocketListeners = (
    dispatch: Dispatch<any>,
    socket: SocketIOClient.Socket
) => {
    socket.on(
        USER_SEND_FRIEND_REQUEST,
        (
            res:
                | SendFriendRequestRequesterResType
                | SendFriendRequestAccepterResType
        ) => {
            if (isRequestRequester(res)) {
                if (res.success) {
                    dispatch(
                        popupActions.addSuccessPopup(
                            'Successfully made friend request'
                        )
                    );
                } else {
                    dispatch(
                        popupActions.addErrorPopup(
                            'Unable to make friend request'
                        )
                    );
                }
            } else if ((res as SendFriendRequestAccepterResType).userId) {
                dispatch(
                    popupActions.addFriendRequestPopup(
                        res.friendRequestId,
                        res.userId,
                        res.username
                    )
                );
            }
        }
    );
    socket.on(GET_DASHBOARD_DATA, (res: GetDashboardDataResType) => {
        dispatch(dashboardActions.setFriends(res.friends));
        dispatch(dashboardActions.setNotifications(res.notifications));
    });
    socket.on(USER_SEARCH_USER, (res: UserSearchUserResType) =>
        dispatch(dashboardActions.setSearchedUser(res ?? null))
    );
    socket.on(USER_GET_MESSAGES, (res: GetMessagesResType) =>
        dispatch(dashboardActions.setMessages(res))
    );
};

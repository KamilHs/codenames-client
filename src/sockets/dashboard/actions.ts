import SocketIOClient from 'socket.io-client';

import {
    GET_DASHBOARD_DATA,
    USER_ACCEPT_FRIEND_REQUEST,
    USER_BLOCK_USER,
    USER_DECLINE_FRIEND_REQUEST,
    USER_JOIN_CHAT,
    USER_LEAVE_CHAT,
    USER_REMOVE_FROM_FRIENDS,
    USER_REMOVE_NOTIFICATION,
    USER_SEARCH_USER,
    USER_SEND_FRIEND_REQUEST,
    USER_SEND_MESSAGE,
    USER_UNBLOCK_USER
} from './const';

export const dashboardSocketActions = {
    requestFriend: (socket: SocketIOClient.Socket, accepterId: number) =>
        socket.emit(USER_SEND_FRIEND_REQUEST, { accepterId }),
    acceptFriend: (socket: SocketIOClient.Socket, friendRequestId: number) =>
        socket.emit(USER_ACCEPT_FRIEND_REQUEST, { friendRequestId }),
    declineFriend: (socket: SocketIOClient.Socket, friendRequestId: number) =>
        socket.emit(USER_DECLINE_FRIEND_REQUEST, { friendRequestId }),
    getDashboardData: (socket: SocketIOClient.Socket) =>
        socket.emit(GET_DASHBOARD_DATA),
    searchUser: (socket: SocketIOClient.Socket, email: string) =>
        socket.emit(USER_SEARCH_USER, { email }),
    removeNotification: (socket: SocketIOClient.Socket, id: number) =>
        socket.emit(USER_REMOVE_NOTIFICATION, { id }),
    removeFriend: (socket: SocketIOClient.Socket, removedFriendId: number) =>
        socket.emit(USER_REMOVE_FROM_FRIENDS, { removedFriendId }),
    blockUser: (socket: SocketIOClient.Socket, blockedId: number) =>
        socket.emit(USER_BLOCK_USER, { blockedId }),
    unblockUser: (socket: SocketIOClient.Socket, blockedId: number) =>
        socket.emit(USER_UNBLOCK_USER, { blockedId }),
    joinChat: (socket: SocketIOClient.Socket, chatId: number) =>
        socket.emit(USER_JOIN_CHAT, { chatId }),
    leaveChat: (socket: SocketIOClient.Socket, chatId: number) =>
        socket.emit(USER_LEAVE_CHAT, { chatId }),
    sendMessage: (
        socket: SocketIOClient.Socket,
        content: string,
        chatId: number
    ) => socket.emit(USER_SEND_MESSAGE, { content, chatId })
};

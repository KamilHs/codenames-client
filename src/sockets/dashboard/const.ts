import {
    IFriend,
    IMessage,
    INotification,
    SearchedUserState
} from 'modules/Dashboard/redux/const';

export const USER_SEND_FRIEND_REQUEST = 'USER_SEND_FRIEND_REQUEST';
export const USER_ACCEPT_FRIEND_REQUEST = 'USER_ACCEPT_FRIEND_REQUEST';
export const USER_DECLINE_FRIEND_REQUEST = 'USER_DECLINE_FRIEND_REQUEST';
export const USER_REMOVE_FROM_FRIENDS = 'USER_REMOVE_FROM_FRIENDS';
export const USER_BLOCK_USER = 'USER_BLOCK_USER';
export const USER_UNBLOCK_USER = 'USER_UNBLOCK_USER';

export const USER_JOIN_CHAT = 'USER_JOIN_CHAT';
export const USER_LEAVE_CHAT = 'USER_LEAVE_CHAT';
export const USER_SEND_MESSAGE = 'USER_SEND_MESSAGE';
export const USER_GET_MESSAGES = 'USER_GET_MESSAGES';

export const USER_SEARCH_USER = 'USER_SEARCH_USER';

export const USER_REMOVE_NOTIFICATION = 'USER_REMOVE_NOTIFICATION';

export const GET_DASHBOARD_DATA = 'GET_DASHBOARD_DATA';

export type SendFriendRequestRequesterResType = {
    success: boolean;
};

export type SendFriendRequestAccepterResType = {
    friendRequestId: number;
    userId: number;
    username: string;
};

export type GetDashboardDataResType = {
    friends: IFriend[];
    notifications: INotification[];
};

export type UserSearchUserResType = {
    id: number;
    username: string;
    avatar: string;
    state: SearchedUserState;
};

export type GetMessagesResType = IMessage[];

import Enzyme from 'enzyme';
import EnzymeAdapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new EnzymeAdapter() });

class MockLocalStorage {
    private store: Record<string, string>;
    constructor() {
        this.store = {};
    }
    clear() {
        this.store = {};
    }
    getItem(key: string) {
        return this.store[key] || null;
    }
    setItem(key: string, value: string) {
        this.store[key] = value;
    }
    removeItem(key: string) {
        delete this.store[key];
    }
    key(n: number) {
        return this.length <= n ? null : Object.keys(this.store)[n];
    }
    get length() {
        return Object.keys(this.store).length;
    }
}
const mockLocalStorage = new MockLocalStorage();

global.localStorage = mockLocalStorage;
Object.defineProperty(window, 'localStorage', {
    value: mockLocalStorage
});

export default mockLocalStorage;

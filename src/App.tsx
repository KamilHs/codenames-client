import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { CssBaseline, StylesProvider, ThemeProvider } from '@material-ui/core';
import dotenv from 'dotenv';
import { Provider } from 'react-redux';

import { GlobalStyles, themeService } from './theming';
import { LocalStorageContextProvider } from './context/LocalStorageContext';
import Popup from './components/Popup';
import Routes from './routes';
import { SocketContextProvider } from 'context/SocketContext';
import store, { history } from './store';

const theme = themeService.getTheme();

dotenv.config();

const App: React.FC = () => (
    <Provider store={store}>
        <StylesProvider>
            <ConnectedRouter history={history}>
                <CssBaseline />
                <ThemeProvider theme={theme}>
                    <GlobalStyles />
                    <LocalStorageContextProvider>
                        <SocketContextProvider>
                            <Popup />
                            <Routes />
                        </SocketContextProvider>
                    </LocalStorageContextProvider>
                </ThemeProvider>
            </ConnectedRouter>
        </StylesProvider>
    </Provider>
);

export default App;
